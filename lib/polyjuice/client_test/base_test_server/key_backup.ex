# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.KeyBackup do
  @behaviour Polyjuice.Server.Protocols.Helper.KeyBackup

  alias Polyjuice.Util.ClientAPIErrors

  @type required_state :: %{
          key_backup: %{
            optional(String.t()) => %{
              latest: String.t(),
              versions: %{
                String.t() => %{
                  algorithm: String.t(),
                  auth_data: String.t(),
                  backup: %{
                    optional(String.t()) => %{
                      String.t() => Polyjuice.Server.KeyBackup.backup_data()
                    }
                  }
                }
              }
            }
          }
        }

  def backup_stats(backup) do
    count =
      Enum.reduce(backup, 0, fn {_, room_data}, acc ->
        acc + Enum.count(room_data)
      end)

    {:ok, canonical} = Polyjuice.Util.JSON.canonical_json(backup)
    etag = :crypto.hash(:sha256, canonical) |> Base.encode64(padding: false)

    {count, etag}
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def put_keys({user_id, version, data}, _from, state) do
    case Map.fetch(state.key_backup, user_id) do
      {:ok, %{latest: ^version, versions: versions} = user_backups} ->
        %{backup: backup} = version_info = Map.get(versions, version, %{})

        new_backup =
          Enum.reduce(data, backup, fn {room_id, room_data}, acc ->
            room_backup =
              Enum.reduce(room_data, Map.get(backup, room_id, %{}), fn {session_id, new_data},
                                                                       acc ->
                with {:ok, existing_data} <- Map.fetch(acc, session_id),
                     cmp when cmp != :gt <-
                       Polyjuice.Server.KeyBackup.compare(new_data, existing_data) do
                  # we already have data, and the new data is not better
                  acc
                else
                  _ -> Map.put(acc, session_id, new_data)
                end
              end)

            Map.put(acc, room_id, room_backup)
          end)

        new_version_info = %{version_info | backup: new_backup}

        new_user_backups = %{
          user_backups
          | versions: Map.put(user_backups.versions, version, new_version_info)
        }

        new_key_backup = Map.put(state.key_backup, user_id, new_user_backups)

        state =
          Polyjuice.ClientTest.BaseTestServer.emit(
            {:key_backup_put_keys, user_id, data},
            state
          )

        {count, etag} = backup_stats(new_backup)
        {:reply, {:ok, count, etag}, %{state | key_backup: new_key_backup}}

      {:ok, %{latest: latest}} ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Wrong backup version",
            plug_status: 403,
            matrix_error:
              ClientAPIErrors.MWrongRoomKeysVersion.new("Wrong backup version",
                current_version: latest
              )
          }}, state}

      _ ->
        # FIXME: what's the right error for this?
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "No backups created",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new()
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def get_keys({user_id, version, room_id, session_id}, _from, state) do
    with {:ok, backup_for_user} <- Map.fetch(state.key_backup, user_id),
         {:ok, version_info} <- Map.fetch(backup_for_user.versions, version) do
      cond do
        room_id == nil ->
          {:reply, {:ok, version_info.backup}, state}

        session_id == nil ->
          case Map.fetch(version_info.backup, room_id) do
            {:ok, room_backup} ->
              {:reply, {:ok, %{room_id => room_backup}}, state}

            _ ->
              {:reply,
               {:error,
                %Polyjuice.Server.Plug.MatrixError{
                  message: "Requested session not found in backup",
                  plug_status: 404,
                  matrix_error: ClientAPIErrors.MNotFound.new("Requested session not found")
                }}, state}
          end

        true ->
          with {:ok, room_backup} <- Map.fetch(version_info.backup, room_id),
               {:ok, session} <- Map.fetch(room_backup, session_id) do
            {:reply, {:ok, %{room_id => %{session_id => session}}}, state}
          else
            _ ->
              {:reply,
               {:error,
                %Polyjuice.Server.Plug.MatrixError{
                  message: "Requested session not found in backup",
                  plug_status: 404,
                  matrix_error: ClientAPIErrors.MNotFound.new("Requested session not found")
                }}, state}
          end
      end
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Backup not found",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new("Backup not found")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def delete_keys({user_id, version, room_id, session_id}, _from, state) do
    with {:ok, backup_for_user} <- Map.fetch(state.key_backup, user_id),
         {:ok, %{backup: backup} = version_info} <- Map.fetch(backup_for_user.versions, version) do
      cond do
        room_id == nil ->
          # delete everything in the version
          new_key_backup =
            Map.put(state.key_backup, user_id, %{
              backup_for_user
              | versions: %{version_info | backup: %{}}
            })

          {count, etag} = backup_stats(%{})
          {:reply, {:ok, count, etag}, %{state | key_backup: new_key_backup}}

        session_id == nil ->
          new_backup = Map.delete(backup, room_id)

          new_backup_for_user =
            Map.put(backup_for_user, version, %{version_info | backup: new_backup})

          new_key_backup = Map.put(state.key_backup, user_id, new_backup_for_user)
          {count, etag} = backup_stats(new_backup)
          {:reply, {:ok, count, etag}, %{state | key_backup: new_key_backup}}

        true ->
          case Map.fetch(backup, room_id) do
            {:ok, room_backup} ->
              new_backup = Map.put(backup, room_id, Map.delete(room_backup, session_id))

              new_backup_for_user =
                Map.put(backup_for_user, version, %{version_info | backup: new_backup})

              new_key_backup = Map.put(state.key_backup, user_id, new_backup_for_user)
              {count, etag} = backup_stats(new_backup)
              {:reply, {:ok, count, etag}, %{state | key_backup: new_key_backup}}

            _ ->
              # nothing to delete
              {count, etag} = backup_stats(backup)
              {:reply, {:ok, count, etag}, state}
          end
      end
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Backup not found",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new("Backup not found")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def create_version({user_id, algorithm, auth_data}, _from, state) do
    backup_for_user = Map.get(state.key_backup, user_id, %{versions: %{}})
    version = Polyjuice.ClientTest.Util.make_random_key(backup_for_user)

    new_backup_for_user = %{
      latest: version,
      versions:
        Map.put(backup_for_user.versions, version, %{
          algorithm: algorithm,
          auth_data: auth_data,
          backup: %{}
        })
    }

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:key_backup_create_version, user_id, version, algorithm, auth_data},
        state
      )

    {:reply, version,
     %{state | key_backup: Map.put(state.key_backup, user_id, new_backup_for_user)}}
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def update_version({user_id, version, algorithm, auth_data}, _from, state) do
    with {:ok, backup_for_user} <- Map.fetch(state.key_backup, user_id),
         {:ok, version_info} <- Map.fetch(backup_for_user.versions, version) do
      if version_info.algorithm != algorithm do
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Algorithm does not match",
            plug_status: 400,
            matrix_error: ClientAPIErrors.MInvalidParam.new("Algorithm does not match")
          }}, state}
      else
        new_backup_for_user = %{
          backup_for_user
          | versions:
              Map.put(backup_for_user.versions, version, %{version_info | auth_data: auth_data})
        }

        new_key_backup = Map.put(state.key_backup, user_id, new_backup_for_user)

        state =
          Polyjuice.ClientTest.BaseTestServer.emit(
            {:key_backup_update_version, user_id, version, algorithm, auth_data},
            state
          )

        {:reply, :ok, %{state | key_backup: new_key_backup}}
      end
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Backup not found",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new("Backup not found")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def get_version({user_id, version}, _from, state) do
    with {:ok, backup_for_user} <- Map.fetch(state.key_backup, user_id),
         version = version || backup_for_user.latest,
         {:ok, version_info} <- Map.fetch(backup_for_user.versions, version) do
      {count, etag} = backup_stats(version_info.backup)

      {:reply,
       {:ok,
        %{
          algorithm: version_info.algorithm,
          auth_data: version_info.auth_data,
          count: count,
          etag: etag,
          version: version
        }}, state}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Backup not found",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new("Backup not found")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.KeyBackup
  def delete_version({user_id, version}, _from, state) do
    with {:ok, backup_for_user} <- Map.fetch(state.key_backup, user_id) do
      new_backup_for_user = %{
        backup_for_user
        | versions: Map.delete(backup_for_user.versions, version)
      }

      new_backup_for_user =
        if new_backup_for_user.latest == version do
          # FIXME: should we roll back to the previous
          %{new_backup_for_user | latest: nil}
        else
          new_backup_for_user
        end

      new_key_backup = Map.put(state.key_backup, user_id, new_backup_for_user)
      {:reply, :ok, %{state | key_backup: new_key_backup}}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Backup not found",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new("Backup not found")
          }}, state}
    end
  end
end
