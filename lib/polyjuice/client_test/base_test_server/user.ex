# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.BaseTestServer.User do
  alias Polyjuice.Util.ClientAPIErrors

  @behaviour Polyjuice.Server.Protocols.Helper.User

  @type user_map() :: %{
          optional(String.t()) => %{
            password: String.t() | :no_login,
            devices: %{optional(String.t()) => %{access_token: String.t()}}
          }
        }

  @type access_token_map() :: %{
          optional(String.t()) => %{user_id: String.t(), device_id: String.t()}
        }

  @type required_state() :: %{
          users: user_map(),
          access_tokens: access_token_map()
        }

  @impl Polyjuice.Server.Protocols.Helper.User
  def login_flows(_, _from, state) do
    {:reply, [%{"type" => "m.login.password"}], state}
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def log_in({{:password, user_id, password} = params, opts}, _from, state) do
    # FIXME: check opts
    with {:ok, _parsed_user_id} <- Polyjuice.Util.Identifiers.V1.UserIdentifier.parse(user_id),
         {:ok, user_info = %{password: ^password}} <- Map.fetch(state.users, user_id) do
      access_token = Polyjuice.ClientTest.Util.make_random_key(state.access_tokens)

      # FIXME: if device_id is specified, make sure it isn't already used
      device_id =
        Keyword.get(opts, :device_id) ||
          Polyjuice.ClientTest.Util.make_random_key(user_info.devices, fn ->
            Polyjuice.Util.Randomizer.randomize(10, :upcase)
          end)

      state =
        Polyjuice.ClientTest.BaseTestServer.emit(
          {:login, user_id, device_id, access_token},
          state
        )

      new_user_info = %{
        user_info
        | devices: Map.put(user_info.devices, device_id, %{access_token: access_token})
      }

      new_users = Map.put(state.users, user_id, new_user_info)

      new_access_tokens =
        Map.put(state.access_tokens, access_token, %{user_id: user_id, device_id: device_id})

      {:reply,
       {:ok,
        %{
          access_token: access_token,
          device_id: device_id,
          user_id: user_id
        }}, %{state | users: new_users, access_tokens: new_access_tokens}}
    else
      _ ->
        state =
          Polyjuice.ClientTest.BaseTestServer.emit(
            {:failed_login, params},
            state
          )

        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Forbidden",
            plug_status: 403,
            matrix_error: ClientAPIErrors.MForbidden.new("Invalid password")
          }}, state}
    end
  end

  def log_in({params, _opts}, _from, state) do
    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:failed_login, params},
        state
      )

    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Forbidden",
        plug_status: 403,
        matrix_error: ClientAPIErrors.MForbidden.new()
      }}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def log_out({user_id, device_id}, _from, state) do
    with {:ok, user_info} <- Map.fetch(state.users, user_id),
         {:ok, device_info} <- Map.fetch(user_info.devices, device_id) do
      new_users =
        put_in(state.users, [user_id, :devices], Map.delete(user_info.devices, device_id))

      new_access_tokens =
        case device_info do
          %{access_token: access_token} -> Map.delete(state.access_tokens, access_token)
          _ -> state.access_tokens
        end

      state =
        Polyjuice.ClientTest.BaseTestServer.emit(
          {:logout, user_id, device_id},
          state
        )

      {:reply, :ok, %{state | users: new_users, access_tokens: new_access_tokens}}
    else
      _ ->
        {:reply, :ok, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def get_user_devices({user_id}, _from, state) do
    with {:ok, user_info} <- Map.fetch(state.users, user_id) do
      {:reply, Map.keys(user_info.devices), state}
    else
      _ -> {:reply, [], state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def register(_, _from, state) do
    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Registration disabled",
        plug_status: 403,
        matrix_error: ClientAPIErrors.MForbidden.new("Registration disabled")
      }}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def change_password({user_id, password}, _from, state) do
    with {:ok, user_info} <- Map.fetch(state.users, user_id) do
      new_users = Map.put(state.users, user_id, %{user_info | password: password})

      state =
        Polyjuice.ClientTest.BaseTestServer.emit(
          {:change_password, user_id, user_info.password, password},
          state
        )

      {:reply, :ok, %{state | users: new_users}}
    else
      # this should never happen because it should only be called on an existing user
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Tried to change password of nonexistent user",
            plug_status: 500,
            matrix_error: ClientAPIErrors.MUnknown.new()
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def deactivate({user_id}, _from, state) do
    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:deactivate, user_id},
        state
      )

    {:reply, :ok, %{state | users: Map.delete(state.users, user_id)}}
  end

  defp check_uiauth_password(access_token, user_id, password, state) do
    {:ok, %{user_id: expected_user_id}} = Map.get(state.access_tokens, access_token)

    match =
      if String.starts_with?(user_id, "@") do
        user_id == expected_user_id
      else
        [<<?@, expected_localpart>>, _] = String.split(expected_user_id, ":", parts: 2)
        user_id == expected_localpart
      end

    with true <- match,
         {:ok, %{password: ^password}} <- Map.fetch(state.users, expected_user_id) do
      {:reply, :ok, state}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "M_FORBIDDEN",
            plug_status: 401,
            matrix_error: ClientAPIErrors.MUnknown.new("Invalid password")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def user_interactive_auth(
        {access_token, :device_signing_upload, _endpoint, auth, _request},
        _from,
        state
      ) do
    # FIXME: allow the test to customize what stages to use
    case auth do
      nil ->
        {:reply,
         {:cont,
          %{
            "flows" => [
              %{
                "stages" => ["m.login.dummy"]
              },
              %{
                "stages" => ["m.login.password"]
              }
            ],
            "session" => access_token
          }}, state}

      %{"type" => "m.login.dummy"} ->
        {:reply, :ok, state}

      %{
        "type" => "m.login.password",
        "session" => ^access_token,
        "identifier" => %{"type" => "m.id.user", "user" => user_id},
        "password" => password
      } ->
        check_uiauth_password(access_token, user_id, password, state)

      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "M_FORBIDDEN",
            plug_status: 401,
            matrix_error: ClientAPIErrors.MUnknown.new("Unknown stage")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def user_interactive_auth({access_token, _type, _endpoint, auth, _request}, _from, state) do
    # FIXME: allow the test to customize what stages to use
    case auth do
      nil ->
        {:reply,
         {:cont,
          %{
            "flows" => [
              %{
                "stages" => ["m.login.password"]
              }
            ],
            "session" => access_token
          }}, state}

      %{
        "type" => "m.login.password",
        "session" => ^access_token,
        "identifier" => %{"type" => "m.id.user", "user" => user_id},
        "password" => password
      } ->
        check_uiauth_password(access_token, user_id, password, state)

      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "M_FORBIDDEN",
            plug_status: 401,
            matrix_error: ClientAPIErrors.MUnknown.new("Unknown stage")
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def get_device_info({_user_id, target_user_id, :all}, _from, state) do
    with {:ok, user_info} <- Map.fetch(state.users, target_user_id) do
      devices =
        Enum.map(user_info.devices, fn {device_id, _} -> {device_id, %{}} end)
        |> Map.new()

      {:reply, {:ok, devices}, state}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Unknown user",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new()
          }}, state}
    end
  end

  def get_device_info({_user_id, target_user_id, device_id}, _from, state)
      when is_binary(device_id) do
    with {:ok, user_info} <- Map.fetch(state.users, target_user_id),
         {:ok, _} <- Map.fetch(user_info.devices, device_id) do
      {:reply, {:ok, %{device_id => %{}}}, state}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Unknown user or device",
            plug_status: 404,
            matrix_error: ClientAPIErrors.MNotFound.new()
          }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.User
  def get_profile({_user_id, target_user_id, property}, _from, state) do
    if Map.has_key?(state.users, target_user_id) do
      {:reply, {:ok, %{}}, state}
    else
      {:reply,
       {:error,
        %Polyjuice.Server.Plug.MatrixError{
          message: "Unknown user",
          plug_status: 404,
          matrix_error: ClientAPIErrors.MNotFound.new()
        }}, state}
    end
  end
end
