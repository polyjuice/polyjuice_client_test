# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.Room do
  @behaviour Polyjuice.Server.Protocols.Helper.Room

  @type room_event_stream() :: list({pos_integer, String.t(), list(Polyjuice.Util.event())})
  @type room_prev_event_map() :: %{optional(String.t()) => {String.t(), String.t()}}
  @type room_state_map() :: %{
          optional(String.t()) => %{{String.t(), String.t()} => Polyjuice.Util.event()}
        }
  @type rooms_by_user_map() :: %{optional(String.t()) => MapSet.t(String.t())}
  @type transactions_map() :: %{optional({String.t(), String.t()}) => %{String.t() => String.t()}}

  @type required_state() :: %{
          room_event_stream: room_event_stream(),
          room_prev_event: room_prev_event_map(),
          room_state: room_state_map(),
          room_transactions: transactions_map(),
          rooms_by_user: rooms_by_user_map()
        }

  @doc """
  Determine if two users share a room.
  """
  @spec shared_room?(String.t(), String.t(), required_state()) :: boolean
  def shared_room?(user_id1, user_id2, state) do
    if user_id1 == user_id2 do
      # always consider a user to be in a room with themselves, even if they
      # aren't in any rooms
      true
    else
      user1_rooms = Map.get(state.rooms_by_user, user_id1, MapSet.new())
      user2_rooms = Map.get(state.rooms_by_user, user_id2, MapSet.new())
      not MapSet.disjoint?(user1_rooms, user2_rooms)
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def create({[{creator, "m.room.create", "", _} | _] = state_events}, from, state) do
    servername = state.server.servername

    room_id =
      Polyjuice.ClientTest.Util.make_random_key(state.room_state, fn ->
        "!#{Polyjuice.Util.Randomizer.randomize(10, :downcase)}:#{servername}"
      end)

    {events, {room_state, prev_event, prev_event_hash}} =
      Enum.map(state_events, fn {sender, event_type, state_key, event_content} ->
        %{
          "sender" => sender,
          "type" => event_type,
          "state_key" => state_key,
          "content" => event_content
        }
      end)
      # FIXME: for now, we assume the room version is >= 4
      |> Polyjuice.ClientTest.Util.make_room_events_v4(room_id, %{}, nil, nil)

    new_room_state = Map.put(state.room_state, room_id, room_state)
    new_room_prev_event = Map.put(state.room_prev_event, room_id, {prev_event, prev_event_hash})

    new_rooms_by_user =
      Map.get(state.rooms_by_user, creator, MapSet.new())
      |> MapSet.put(room_id)
      |> (&Map.put(state.rooms_by_user, creator, &1)).()

    # FIXME: we should probably record invites somewhere too

    [{stream_pos, _, _} | _] =
      new_room_event_stream =
      case state.room_event_stream do
        [{pos, _, _}, _] ->
          [{pos + 1, room_id, events} | state.room_event_stream]

        [] ->
          [{1, room_id, events}]
      end

    state =
      Polyjuice.ClientTest.BaseTestServer.emit(
        {:create_room, creator, room_id, events},
        state
      )

    if Map.has_key?(state, :subscribers) do
      Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
        {{:room_events, stream_pos, room_id, events}, [creator]},
        from,
        state
      )

      stripped_state = Polyjuice.Server.Room.strip_state(room_state)

      Enum.each(room_state, fn {{type, key}, event} ->
        if type == "m.room.member" and
             match?(%{"content" => %{"membership" => "invite"}}, event) do
          Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
            {{:invite, stream_pos, room_id, stripped_state}, [key]},
            from,
            state
          )
        end
      end)
    end

    {:reply, {:ok, room_id},
     %{
       state
       | room_event_stream: new_room_event_stream,
         room_state: new_room_state,
         room_prev_event: new_room_prev_event,
         rooms_by_user: new_rooms_by_user
     }}
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def join({user_id, room_id, opts}, from, state) do
    # FIXME: pretend they're not allowed to join for now, until we actually
    # implement this
    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Not implemented",
        plug_status: 403,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
      }}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def send_event(
        {user_id, device_id, room_id, event_type, state_key, txn_id, content},
        from,
        state
      ) do
    user_transactions = Map.get(state.room_transactions, {user_id, device_id}, %{})

    case Map.fetch(user_transactions, txn_id) do
      {:ok, event_id} ->
        {:reply, {:ok, event_id}, state}

      _ ->
        with {:ok, orig_room_state} <- Map.fetch(state.room_state, room_id),
             {:ok, {prev_event, prev_event_hash}} <- Map.fetch(state.room_prev_event, room_id) do
          {:ok, %{"content" => create_event_content}} =
            Map.fetch(orig_room_state, {"m.room.create", ""})

          room_version = Map.get(create_event_content, "room_version", "1")

          event =
            if state_key == nil do
              %{
                "type" => event_type,
                "sender" => user_id,
                "content" => content,
                "room_id" => room_id
              }
            else
              %{
                "type" => event_type,
                "state_key" => state_key,
                "sender" => user_id,
                "content" => content,
                "room_id" => room_id
              }
            end

          event =
            if state_key != nil,
              do: Map.put(event, "state_key", state_key),
              else: event

          event =
            if txn_id != nil,
              do:
                event
                |> Map.put("unsigned", %{"transaction_id" => txn_id})
                |> Map.put(:sender_device, device_id),
              else: event

          if Polyjuice.Util.RoomVersion.authorized?(room_version, event, orig_room_state) do
            # FIXME: we assume that the room is > v4 for now
            {[event], {room_state, prev_event, prev_event_hash}} =
              Polyjuice.ClientTest.Util.make_room_events_v4(
                [event],
                room_id,
                orig_room_state,
                prev_event,
                prev_event_hash
              )

            [{stream_pos, _, _} | _] = state.room_event_stream

            [{stream_pos, _, _} | _] =
              new_room_event_stream = [
                {stream_pos + 1, room_id, [event]} | state.room_event_stream
              ]

            new_room_state = Map.put(state.room_state, room_id, room_state)

            new_room_prev_event =
              Map.put(state.room_prev_event, room_id, {prev_event, prev_event_hash})

            state =
              Polyjuice.ClientTest.BaseTestServer.emit(
                {:send_event, user_id, room_id, event},
                state
              )

            # update membership if it got changed, and notify people who were affected
            new_rooms_by_user =
              if event_type == "m.room.member" && state_key != nil do
                case Map.get(content, "membership") do
                  "join" ->
                    rooms = Map.get(state.rooms_by_user, state_key, MapSet.new())

                    if MapSet.member?(rooms, room_id) do
                      state.rooms_by_user
                    else
                      if Map.has_key?(state, :subscribers) do
                        Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
                          {{:join, stream_pos, room_id, nil}, [state_key]},
                          from,
                          state
                        )
                      end

                      MapSet.put(rooms, room_id)
                      |> (&Map.put(state.rooms_by_user, state_key, &1)).()
                    end

                  "invite" ->
                    if Map.has_key?(state, :subscribers) do
                      stripped_state = Polyjuice.Server.Room.strip_state(room_state)

                      Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
                        {{:invite, stream_pos, room_id, stripped_state}, [state_key]},
                        from,
                        state
                      )
                    end

                    state.rooms_by_user

                  "ban" ->
                    rooms =
                      Map.get(state.rooms_by_user, state_key, MapSet.new())
                      |> MapSet.delete(room_id)

                    if MapSet.equal?(rooms, MapSet.new()) do
                      Map.delete(state.rooms_by_user, state_key)
                    else
                      Map.put(state.rooms_by_user, state_key, rooms)
                    end

                  "leave" ->
                    rooms =
                      Map.get(state.rooms_by_user, state_key, MapSet.new())
                      |> MapSet.delete(room_id)

                    if MapSet.equal?(rooms, MapSet.new()) do
                      Map.delete(state.rooms_by_user, state_key)
                    else
                      Map.put(state.rooms_by_user, state_key, rooms)
                    end

                  _ ->
                    state.rooms_by_user
                end
              else
                state.rooms_by_user
              end

            # notify everyone in the room about the new event
            if Map.has_key?(state, :subscribers) do
              # Note: we use the room state from before the event, because
              # those are the people who need to be notified.
              # e.g. banned/kicked/left people need to receive their
              # ban/leave event.  Invited/joined people are already notified above
              Enum.each(orig_room_state, fn {{type, key}, member_event} ->
                if type == "m.room.member" and
                     match?(%{"content" => %{"membership" => "join"}}, member_event) do
                  Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
                    {{:room_events, stream_pos, room_id, [event]}, [key]},
                    from,
                    state
                  )
                end
              end)
            end

            event_id = Map.fetch!(event, "event_id")

            new_room_transactions =
              if txn_id != nil do
                Map.put(user_transactions, txn_id, event_id)
                |> (&Map.put(state.room_transactions, {user_id, device_id}, &1)).()
              else
                state.room_transactions
              end

            {:reply, {:ok, event_id},
             %{
               state
               | room_event_stream: new_room_event_stream,
                 room_state: new_room_state,
                 room_prev_event: new_room_prev_event,
                 room_transactions: new_room_transactions,
                 rooms_by_user: new_rooms_by_user
             }}
          else
            {:reply,
             {:error,
              %Polyjuice.Server.Plug.MatrixError{
                message: "Forbidden",
                plug_status: 403,
                matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
              }}, state}
          end
        else
          _ ->
            {:reply,
             {:error,
              %Polyjuice.Server.Plug.MatrixError{
                message: "Forbidden",
                plug_status: 403,
                matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
              }}, state}
        end
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def get_state({user_id, room_id, event_type, state_key}, _from, state) do
    is_member? =
      Map.get(state.rooms_by_user, user_id, MapSet.new())
      |> MapSet.member?(room_id)

    if is_member? do
      room_state = Map.get(state.room_state, room_id, %{})

      events =
        cond do
          event_type == nil ->
            Map.values(room_state)

          state_key == nil ->
            Enum.flat_map(room_state, fn {{type, _}, value} ->
              if type == event_type, do: [value], else: []
            end)

          true ->
            case Map.fetch(room_state, {event_type, state_key}) do
              {:ok, state} -> [state]
              :error -> []
            end
        end

      {:reply, {:ok, events}, state}
    else
      # FIXME: if the user used to be a member, then return the latest state
      # that they could see
      {:reply,
       {:error,
        %Polyjuice.Server.Plug.MatrixError{
          message: "Forbidden",
          plug_status: 403,
          matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
        }}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def invite({user_id, room_id, target_user_id, reason}, from, state) do
    with {:ok, room_state} <- Map.fetch(state.room_state, room_id) do
      # NOTE: in a real implementation, you want to check that the user can
      # send the invite first, before checking whether the target user is in
      # the room.  Otherwise, an attacker can check if the target user is in a
      # room, even if the attacker is not a member of the room.  But since this
      # is just a test implementation, we don't need to worry about it.
      case Map.fetch(room_state, {"m.room.member", target_user_id}) do
        {:ok, %{"content" => %{"membership" => "join"}}} ->
          {:reply,
           {:error,
            %Polyjuice.Server.Plug.MatrixError{
              message: "User already in room",
              plug_status: 403,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new("User already in room")
            }}, state}

        {:ok, %{"content" => %{"membership" => "invite"}}} ->
          {:reply,
           {:error,
            %Polyjuice.Server.Plug.MatrixError{
              message: "User already invited",
              plug_status: 403,
              matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new("User already invited")
            }}, state}

        _ ->
          content =
            if reason == "",
              do: %{"membership" => "invite"},
              else: %{"membership" => "invite", "reason" => reason}

          case send_event(
                 {user_id, nil, room_id, "m.room.member", target_user_id, nil, content},
                 from,
                 state
               ) do
            {:reply, {:ok, _event_id}, state} -> {:reply, :ok, state}
            rv -> rv
          end
      end
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Forbidden",
            plug_status: 403,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
          }}, state}
    end
  end
end
