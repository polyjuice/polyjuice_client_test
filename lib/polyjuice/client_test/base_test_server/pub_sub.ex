# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.BaseTestServer.PubSub do
  @behaviour Polyjuice.Server.Protocols.Helper.PubSub

  @type required_state() :: %{
          subscribers: %{optional(any) => Polyjuice.Server.Protocols.Subscriber.t()},
          sync_helper: pid,
          filters: %{optional(String.t()) => %{String.t() => map()}}
        }

  @type optional_state_account_data() :: %{
          account_data: Polyjuice.ClientTest.AccountData.account_data_map(),
          account_data_stream: Polyjuice.ClientTest.AccountData.account_data_stream()
        }

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def publish({data, queues}, _, state) do
    Enum.each(queues, fn queue ->
      state.subscribers
      |> Map.get(queue, [])
      |> Enum.each(&Polyjuice.Server.Protocols.Subscriber.notify(&1, queue, data))
    end)

    {:reply, :ok, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def subscribe({subscriber, queues}, _, state) do
    new_subscribers =
      Enum.reduce(queues, state.subscribers, fn queue, subscribers ->
        Map.get(subscribers, queue, MapSet.new())
        |> MapSet.put(subscriber)
        |> (&Map.put(subscribers, queue, &1)).()
      end)

    {:reply, :ok, %{state | subscribers: new_subscribers}}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def unsubscribe({subscriber, queues}, _, state) do
    new_subscribers =
      Enum.reduce(queues, state.subscribers, fn queue, subscribers ->
        subscriber_set =
          Map.get(subscribers, queue, MapSet.new())
          |> MapSet.delete(subscriber)

        if MapSet.size(subscriber_set) == 0 do
          Map.delete(subscribers, queue)
        else
          Map.put(subscribers, queue, subscriber_set)
        end
      end)

    {:reply, :ok, %{state | subscribers: new_subscribers}}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def await_sync({user_id, device_id, sync_token, timeout, filter}, from, state) do
    Task.start(fn ->
      Polyjuice.Server.SyncHelper.sync(
        state.sync_helper,
        state.server,
        user_id,
        device_id,
        sync_token,
        timeout,
        filter
      )
      |> (&GenServer.reply(from, &1)).()
    end)

    {:noreply, state}
  end

  def is_member?(user_id, room_state, membership \\ "join") do
    match?(
      %{"content" => %{"membership" => ^membership}},
      Map.get(room_state, {"m.room.member", user_id})
    )
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def get_sync_data_since({user_id, device_id, sync_token, filter}, _, state) do
    resp = %{}

    # account data
    account_data_start_pos = Polyjuice.Server.SyncToken.get_component(sync_token, :a)

    account_data =
      Map.get(state, :account_data_stream, [])
      |> Enum.reduce_while(
        %{},
        fn {pos, account_data_user_id, room_id, type, data}, account_data ->
          cond do
            pos <= account_data_start_pos ->
              {:halt, account_data}

            account_data_user_id != user_id ->
              {:cont, account_data}

            true ->
              account_data_for_room =
                Map.get(account_data, room_id, %{})
                |> Map.put_new(type, data)

              account_data = Map.put(account_data, room_id, account_data_for_room)
              {:cont, account_data}
          end
        end
      )

    account_data_end_pos =
      Map.get(state, :account_data_stream, [])
      |> Enum.at(0, {0})
      |> elem(0)

    sync_token = Polyjuice.Server.SyncToken.update(sync_token, :a, account_data_end_pos)

    resp =
      if Map.has_key?(account_data, nil) do
        Map.get(account_data, nil)
        |> Enum.map(fn {type, data} -> %{"type" => type, "content" => data} end)
        |> (&Map.put(resp, "account_data", %{"events" => &1})).()
      else
        resp
      end

    # FIXME: add room account data too

    # device changes

    device_key_start_pos = Polyjuice.Server.SyncToken.get_component(sync_token, :k)

    devices_changed =
      Map.get(state, :changed_keys_stream, [])
      |> Enum.reduce_while(
        MapSet.new(),
        fn {pos, changed_user_id}, acc ->
          cond do
            pos <= device_key_start_pos ->
              {:halt, acc}

            not Polyjuice.ClientTest.BaseTestServer.Room.shared_room?(
              changed_user_id,
              user_id,
              state
            ) ->
              {:cont, acc}

            true ->
              {:cont, MapSet.put(acc, changed_user_id)}
          end
        end
      )

    device_key_end_pos =
      Map.get(state, :changed_keys_stream, [])
      |> Enum.at(0, {0})
      |> elem(0)

    sync_token = Polyjuice.Server.SyncToken.update(sync_token, :k, device_key_end_pos)

    # FIXME: do "left" too

    resp =
      if not MapSet.equal?(devices_changed, MapSet.new()) do
        Map.put(resp, "device_lists", %{"changed" => MapSet.to_list(devices_changed)})
      else
        resp
      end

    # room messages

    room_start_pos = Polyjuice.Server.SyncToken.get_component(sync_token, :r)

    room_stream = Map.get(state, :room_event_stream, [])
    latest_room_state = Map.get(state, :room_state, %{})

    {rooms, invites, _, _} =
      Enum.reduce_while(
        room_stream,
        {%{}, %{}, latest_room_state, %{}},
        fn {pos, room_id, events}, {rooms, invites, room_state, visible_rooms} = acc ->
          if pos <= room_start_pos do
            {:halt, acc}
          else
            curr_room_state = Map.get(room_state, room_id, %{})

            events_to_send = Map.get(rooms, room_id, [])

            visible_rooms =
              if Map.has_key?(visible_rooms, room_id) do
                visible_rooms
              else
                Map.put(visible_rooms, room_id, is_member?(user_id, curr_room_state))
              end

            {invites, new_room_state, visible_rooms} =
              Enum.reverse(events)
              |> Enum.reduce(
                {invites, curr_room_state, visible_rooms},
                fn event, {invites, curr_room_state, visible_rooms} ->
                  invites =
                    if match?(
                         %{
                           "type" => "m.room.member",
                           "state_key" => ^user_id,
                           "content" => %{"membership" => "invite"}
                         },
                         event
                       ) and
                         is_member?(user_id, Map.get(latest_room_state, room_id, %{}), "invite") and
                         not Map.has_key?(invites, room_id) do
                      Map.put(invites, room_id, %{
                        "events" => Polyjuice.Server.Room.strip_state(curr_room_state)
                      })
                    else
                      invites
                    end

                  visible_rooms =
                    if match?(
                         %{
                           "type" => "m.room.member",
                           "state_key" => ^user_id,
                           "content" => %{"membership" => membership},
                           "unsigned" => %{"prev_content" => %{"membership" => "join"}}
                         },
                         event
                       ) do
                      Map.put(visible_rooms, room_id, true)
                    else
                      visible_rooms
                    end

                  new_room_state =
                    case event do
                      %{
                        "type" => type,
                        "state_key" => key,
                        "unsigned" => %{"prev_content" => prev_content}
                      } ->
                        Map.put(curr_room_state, {type, key}, %{"content" => prev_content})

                      %{"type" => type, "state_key" => key} ->
                        Map.delete(curr_room_state, {type, key})

                      _ ->
                        curr_room_state
                    end

                  {invites, new_room_state, visible_rooms}
                end
              )

            events_to_send =
              cond do
                is_member?(user_id, curr_room_state) or
                    is_member?(user_id, new_room_state) ->
                  # users should see their join and ban/leave events, so check
                  # both the old and new state to see if they're a member
                  # FIXME: also should add the events if they're in the room and
                  # the room history setting says they can see the event
                  [{pos, events} | events_to_send]

                match?([%{"state_key" => _} | _], events) and Map.get(visible_rooms, room_id) ->
                  # state events are always visible to room members
                  [{pos, events} | events_to_send]

                match?(
                  %{"content" => %{"history_visibility" => vis}}
                  when vis == "shared" or vis == "world_readable",
                  Map.get(curr_room_state, {"m.room.history_visibility", ""})
                ) and Map.get(visible_rooms, room_id) ->
                  # events sent before the user joined, if the history
                  # visibility allows it, is visible
                  [{pos, events} | events_to_send]

                # FIXME: if history visibility is "invited", and user has been
                # invited at this point, and joined later, it should be visible

                true ->
                  events_to_send
              end

            new_rooms =
              if events_to_send == [], do: rooms, else: Map.put(rooms, room_id, events_to_send)

            {:cont,
             {new_rooms, invites, Map.put(room_state, room_id, new_room_state), visible_rooms}}
          end
        end
      )

    {joined_rooms, left_rooms} =
      Enum.reduce(rooms, {%{}, %{}}, fn {room_id, events_to_send}, {joined_rooms, left_rooms} ->
        {timeline_events, state, limited, prev_token, join_state} =
          Enum.reverse(events_to_send)
          |> Polyjuice.Server.SyncHelper.Worker.split_events(10, user_id)

        room_info = %{
          # FIXME: drop empty properties
          "timeline" => %{
            "events" => timeline_events,
            "limited" => limited,
            "prev_batch" => prev_token
          },
          "state" => %{"events" => state}
        }

        if is_member?(user_id, Map.get(latest_room_state, room_id, %{})) do
          {Map.put(joined_rooms, room_id, room_info), left_rooms}
        else
          {joined_rooms, Map.put(left_rooms, room_id, room_info)}
        end
      end)

    rooms_resp = %{}

    rooms_resp =
      if joined_rooms != %{} do
        Map.put(rooms_resp, "join", joined_rooms)
      else
        rooms_resp
      end

    rooms_resp =
      if left_rooms != %{} do
        Map.put(rooms_resp, "left", left_rooms)
      else
        rooms_resp
      end

    rooms_resp =
      if invites != %{} do
        Map.put(rooms_resp, "invite", invites)
      else
        rooms_resp
      end

    resp =
      if rooms_resp != %{} do
        Map.put(resp, "rooms", rooms_resp)
      else
        resp
      end

    room_end_pos =
      room_stream
      |> Enum.at(0, {0})
      |> elem(0)

    sync_token = Polyjuice.Server.SyncToken.update(sync_token, :r, room_end_pos)

    # send-to-device messages
    to_device_start_pos = Polyjuice.Server.SyncToken.get_component(sync_token, :d)

    user_to_device_stream =
      Map.get(state, :to_device_stream, %{})
      |> Map.get({user_id, device_id}, [])

    to_device_msgs =
      if match?([{_, :empty}], user_to_device_stream) do
        []
      else
        Enum.take_while(user_to_device_stream, fn {pos, _event} -> pos > to_device_start_pos end)
        |> Enum.map(fn {_, event} -> event end)
      end

    to_device_end_pos =
      user_to_device_stream
      |> Enum.at(0, {0})
      |> elem(0)

    resp =
      if to_device_msgs != [] do
        Map.put(resp, "to_device", %{"events" => Enum.reverse(to_device_msgs)})
      else
        resp
      end

    sync_token = Polyjuice.Server.SyncToken.update(sync_token, :d, to_device_end_pos)

    # FIXME: more info

    {:reply, {sync_token, resp}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def add_filter({user_id, filter}, _from, state) do
    user_filters = Map.get(state.filters, user_id, %{})
    filter_id = Polyjuice.ClientTest.Util.make_random_key(user_filters)

    new_filters =
      Map.put(user_filters, filter_id, filter)
      |> (&Map.put(state.filters, user_id, &1)).()

    {:reply, filter_id, %{state | filters: new_filters}}
  end

  @impl Polyjuice.Server.Protocols.Helper.PubSub
  def get_filter({user_id, filter_id}, _from, state) do
    with {:ok, user_filters} <- Map.fetch(state.filters, user_id),
         {:ok, filter} <- Map.fetch(user_filters, filter_id) do
      {:reply, {:ok, filter}, state}
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Not found",
            plug_status: 404,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
          }}, state}
    end
  end
end
