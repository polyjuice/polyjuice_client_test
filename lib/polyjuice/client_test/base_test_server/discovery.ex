# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.Discovery do
  @behaviour Polyjuice.Server.Protocols.Helper.Discovery

  @type required_state :: %{
          server: %{address: String.t() | URI.t()}
        }

  @impl Polyjuice.Server.Protocols.Helper.Discovery
  def address({}, _from, state), do: {:reply, state.server.address, state}
  @impl Polyjuice.Server.Protocols.Helper.Discovery
  def identity_server({}, _from, state), do: {:reply, nil, state}
  @impl Polyjuice.Server.Protocols.Helper.Discovery
  def extra_fields({}, _from, state), do: {:reply, nil, state}
  @impl Polyjuice.Server.Protocols.Helper.Discovery
  def delegated_hostname({}, _from, state), do: {:reply, nil, state}
end
