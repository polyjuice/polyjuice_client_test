# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defprotocol Polyjuice.ClientTest.TestServer do
  @moduledoc """
  Protocol that all test servers must implement.

  Most test servers should use `Polyjuice.ClientTest.BaseTestServer`.
  """

  @doc """
  Starts monitoring the test server from the calling process.

  This will usually be implemented by calling `Process.monitor/1` on the
  underlying process.
  """
  @spec monitor(Polyjuice.ClientTest.TestServer.t()) :: reference
  def monitor(server)

  @doc """
  Stop the test server with the given reason.
  """
  @spec exit(Polyjuice.ClientTest.TestServer.t(), atom) :: any
  def exit(server, reason)
end
