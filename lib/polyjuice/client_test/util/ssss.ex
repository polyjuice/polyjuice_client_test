# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.SSSS do
  @moduledoc """
  Utility functions for using SSSS.
  """

  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  @doc """
  Create a default SSSS key.
  """
  @spec create_default(Util.User.t()) :: Util.User.t()
  def create_default(%Util.User{} = user) do
    key_id = Util.make_random_key(user.ssss.keys)
    key = :crypto.strong_rand_bytes(32)
    {_, iv, mac} = encrypt_aes(<<0::integer-size(32)-unit(8)>>, "", key)

    account_data = %{
      "algorithm" => "m.secret_storage.v1.aes-hmac-sha2",
      "iv" => Base.encode64(iv, padding: false),
      "mac" => Base.encode64(mac, padding: false)
    }

    Protocols.AccountData.set(
      user.server,
      user.id,
      nil,
      "m.secret_storage.key.#{key_id}",
      account_data
    )

    Protocols.AccountData.set(user.server, user.id, nil, "m.secret_storage.default_key", %{
      "key" => key_id
    })

    %{user | ssss: %{user.ssss | keys: Map.put(user.ssss.keys, key_id, key), default: key_id}}
  end

  @doc """
  Store a secret in SSSS.

  `keys` is a list of SSSS keys to use to encrypt the private keys.  If the key
  `:default` is specified, then the default SSSS key will be used.  If not
  specified, only the default SSSS key will be used.
  """
  @spec store(Util.User.t(), String.t(), String.t(), list(String.t() | :default) | nil) ::
          Util.User.t()
  def store(%Util.User{} = user, name, value, keys \\ nil) do
    keys =
      Enum.map(keys || [:default], fn
        :default ->
          name = user.ssss.default
          {name, user.ssss.keys[name]}

        name ->
          {name, user.ssss.keys[name]}
      end)

    encrypted = encrypt(value, name, keys)
    Polyjuice.Server.Protocols.AccountData.set(user.server, user.id, nil, name, encrypted)

    user
  end

  @doc ~S"""
  Create an SSSS key.

  Returns a tuple of the form `{key_id, key, account_data}`, where `key_id`
  the ID of the key, `key` is the encryption key, and `account_data` is the
  JSON object to store in the user's `m.secret_storage_key.#{key_id}` account
  data.
  """
  @spec create() :: {String.t(), binary, map()}
  def create() do
    key_id = Polyjuice.Util.Randomizer.randomize(10)
    key = :crypto.strong_rand_bytes(32)

    {_, iv, mac} = encrypt_aes(<<0::integer-size(32)-unit(8)>>, "", key)

    account_data = %{
      "algorithm" => "m.secret_storage.v1.aes-hmac-sha2",
      "iv" => Base.encode64(iv, padding: false),
      "mac" => Base.encode64(mac, padding: false)
    }

    {key_id, key, account_data}
  end

  @doc """
  Perform an HKDF

  This function will usually not need to be called directly.
  """
  @spec hkdf(binary, pos_integer, binary, binary) :: binary
  def hkdf(key, len, info, salt \\ <<0::integer-size(32)-unit(8)>>) do
    prk = :crypto.mac(:hmac, :sha256, salt, key)

    hkdf_calculate_okm(prk, len, info, "", 1)
    |> Enum.join()
    |> binary_part(0, len)
  end

  defp hkdf_calculate_okm(prk, len, info, t, i) do
    if len > 0 do
      t = :crypto.mac(:hmac, :sha256, prk, t <> info <> <<i>>)
      [t | hkdf_calculate_okm(prk, len - 32, info, t, i + 1)]
    else
      []
    end
  end

  @doc """
  Encrypt a plaintext with AES using the method described for SSSS

  This function will usually not need to be called directly for SSSS, but may
  be used for other things that encrypt data similarly.  Usually, `encrypt/3`
  should be called instead.
  """
  @spec encrypt_aes(binary, String.t(), binary, Keyword.t()) :: {binary, binary, binary}
  def encrypt_aes(plaintext, name, key, opts \\ []) do
    # FIXME: check IV length, key length
    <<aes_key::binary-32, mac_key::binary-32>> = hkdf(key, 64, name)
    # IV must have bit 63 set to 0
    iv =
      Keyword.get_lazy(opts, :iv, fn ->
        <<:crypto.strong_rand_bytes(8)::binary,
          :crypto.strong_rand_bytes(1) |> :binary.at(0) |> Bitwise.band(0x7F),
          :crypto.strong_rand_bytes(7)::binary>>
      end)

    ciphertext =
      :crypto.crypto_one_time(:aes_256_ctr, aes_key, iv, plaintext, encrypt: true, padding: :none)

    mac = :crypto.mac(:hmac, :sha256, mac_key, ciphertext)
    {ciphertext, iv, mac}
  end

  @doc """
  Encrypt a plaintext using the given SSSS keys

  `name` is the account data event type to be used for the data.  `keys` is a
  list of `{key_id, key}` tuples, like the `key_id` and `key` returned by
  `create/0`.

  Returns a JSON object that can be stored in the account data under the given
  `name`.
  """
  @spec encrypt(binary, String.t(), list({String.t(), binary})) :: map()
  def encrypt(plaintext, name, keys) do
    encrypted =
      Enum.reduce(keys, %{}, fn {keyname, key}, acc ->
        {ciphertext, iv, mac} = encrypt_aes(plaintext, name, key)

        Map.put(acc, keyname, %{
          "ciphertext" => Base.encode64(ciphertext, padding: false),
          "iv" => Base.encode64(iv, padding: false),
          "mac" => Base.encode64(mac, padding: false)
        })
      end)

    %{"encrypted" => encrypted}
  end

  @doc """
  Decrypt a ciphertext with AES using the method described for SSSS

  This function will usually not need to be called directly for SSSS, but may
  be used for other things that encrypt data similarly.  Usually, `decrypt/4`
  should be called instead.
  """
  @spec decrypt_aes(binary, binary, binary, String.t(), binary) ::
          {:ok, binary} | {:error, :bad_mac}
  def decrypt_aes(ciphertext, iv, mac, name, key) do
    # FIXME: check IV length, key length
    <<aes_key::binary-32, mac_key::binary-32>> = hkdf(key, 64, name)

    if :crypto.mac(:hmac, :sha256, mac_key, ciphertext) != mac do
      {:error, :bad_mac}
    else
      {:ok,
       :crypto.crypto_one_time(:aes_256_ctr, aes_key, iv, ciphertext,
         encrypt: false,
         padding: :none
       )}
    end
  end

  @doc """
  Decrypt a SSSS data using the given SSSS key

  `data` should be the account data stored under the given `name`.
  """
  @spec decrypt(map(), String.t(), String.t(), binary) ::
          {:ok, binary} | {:error, :bad_mac | :base64 | :not_encrypted_for_key}
  def decrypt(data, name, key_id, key) do
    case data do
      %{"encrypted" => %{^key_id => %{"ciphertext" => ciphertext, "iv" => iv, "mac" => mac}}} ->
        with {:ok, ciphertext} <- Base.decode64(ciphertext, padding: false),
             {:ok, iv} <- Base.decode64(iv, padding: false),
             {:ok, mac} <- Base.decode64(mac, padding: false) do
          decrypt_aes(ciphertext, iv, mac, name, key)
        else
          _ -> {:error, :base64}
        end

      _ ->
        {:error, :not_encrypted_for_key}
    end
  end

  def make_key_repr(user, key_id) do
    key_id = if key_id == :default, do: user.ssss.default, else: key_id
    key = user.ssss.keys[key_id]

    parity = Enum.reduce(:binary.bin_to_list(key), Bitwise.bxor(0x8B, 0x01), &Bitwise.bxor/2)

    :base58.binary_to_base58(<<0x8B, 0x01, key::binary, parity>>)
    |> Enum.chunk_every(4)
    |> Enum.intersperse(" ")
    |> to_string()
  end

  @doc """
  Given a key, return the base58 representation of it as defined for SSSS
  """
  @spec make_key_repr(binary) :: String.t()
  def make_key_repr(key) do
    parity = Enum.reduce(:binary.bin_to_list(key), Bitwise.bxor(0x8B, 0x01), &Bitwise.bxor/2)

    :base58.binary_to_base58(<<0x8B, 0x01, key::binary, parity>>)
    |> Enum.chunk_every(4)
    |> Enum.intersperse(" ")
    |> to_string()
  end

  @doc """
  Store a given value in the user's account data, encrypted with the given SSSS keys

  `keys` is a list of `{key_id, key}` tuples.
  """
  @spec set(
          Polyjuice.Server.Protocols.AccountData.t(),
          String.t(),
          String.t(),
          String.t(),
          list({String.t(), binary})
        ) :: :ok | {:error, Polyjuice.Server.Plug.MatrixError.t()}
  def set(server, user_id, value, name, keys) do
    encrypted = encrypt(value, name, keys)
    Polyjuice.Server.Protocols.AccountData.set(server, user_id, nil, name, encrypted)
  end

  @doc """
  Share a secret with a device.

  Waits for a secret request from the target device, and then sends the secret.
  To use this function, you must use `secret_request_event_handler/3` as event
  handler, and the sending device must have its olm keys initialized using
  `Polyjuice.ClientTest.Util.Encryption.initialize_olm/1`.
  """
  def share_secret(%Util.Device{} = from, %Util.Device{} = to, name, data, timeout \\ 60_000) do
    from_user = from.user.id
    from_device = from.id
    from_pair = {from_user, from_device}
    to_user = to.user.id
    to_device = to.id
    to_pair = {to_user, to_device}
    server = from.user.server

    # wait until we get a request
    requests =
      Polyjuice.ClientTest.BaseTestServer.wait_until_state(
        server,
        fn
          %{
            polyjuice: %{
              encryption: %{ssss_requests: %{^from_pair => %{^to_pair => %{} = requests}}}
            }
          } ->
            requests =
              Enum.flat_map(
                requests,
                fn
                  {req_id, ^name} -> [req_id]
                  _ -> []
                end
              )

            if requests != [], do: requests, else: false

          state ->
            false
        end,
        timeout
      )

    # remove the request from the queue
    Polyjuice.ClientTest.BaseTestServer.update_test_state(
      server,
      &update_in(&1, [:polyjuice, :encryption, :ssss_requests, from_pair, to_pair], fn req ->
        Map.drop(req, requests)
      end)
    )

    # and send the secret
    txnid =
      Enum.reduce(requests, from.txnid, fn req_id, txnid ->
        {:ok, encrypted_secret} =
          Polyjuice.ClientTest.Util.Encryption.encrypt_olm(
            server,
            "m.secret.send",
            %{"request_id" => req_id, "secret" => data},
            from: from_pair,
            to: to_pair,
            olm_account: from.olm_account
          )

        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          from_user,
          from_device,
          "polyjuice_#{txnid}",
          "m.room.encrypted",
          %{to_user => %{to_device => encrypted_secret}}
        )

        txnid + 1
      end)

    %{from | txnid: txnid}
  end

  @doc """
  Event handler that keeps track of secret requests

  This function can be used as an `Polyjuice.ClientTest.BaseTestServer` event
  handler.

  Using this function requires that the test state is a `Map` that has a
  `:polyjuice` key that it can control.
  """
  def secret_request_event_handler(event, state, _server) do
    polyjuice_state = Map.get(state, :polyjuice, %{})
    encryption_state = Map.get(polyjuice_state, :encryption, %{})

    case event do
      {:send_to_device, to_user, to_device,
       %{
         "type" => "m.secret.request",
         "sender" => from_user,
         "content" => %{
           "action" => "request",
           "requesting_device_id" => from_device,
           "request_id" => req_id,
           "name" => name
         }
       }}
      when is_binary(from_user) and is_binary(from_device) and is_binary(req_id) and
             is_binary(name) ->
        request_state = Map.get(encryption_state, :ssss_requests, %{})
        requests_to_device = Map.get(request_state, {to_user, to_device}, %{})
        requests = Map.get(requests_to_device, {from_user, from_device}, %{})

        requests = Map.put(requests, req_id, name)
        requests_to_device = Map.put(requests_to_device, {from_user, from_device}, requests)
        request_state = Map.put(request_state, {to_user, to_device}, requests_to_device)
        encryption_state = Map.put(encryption_state, :ssss_requests, request_state)
        polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
        Map.put(state, :polyjuice, polyjuice_state)

      {:send_to_device, to_user, to_device,
       %{
         "type" => "m.secret.request",
         "sender" => from_user,
         "content" => %{
           "action" => "request_cancellation",
           "requesting_device_id" => from_device,
           "request_id" => req_id
         }
       }}
      when is_binary(from_user) and is_binary(from_device) and is_binary(req_id) ->
        request_state = Map.get(encryption_state, :ssss_requests, %{})
        requests_to_device = Map.get(request_state, {to_user, to_device}, %{})
        requests = Map.get(requests_to_device, {from_user, from_device}, %{})

        requests = Map.delete(requests, req_id)
        # FIXME: delete keys if the value is empty
        requests_to_device = Map.put(requests_to_device, {from_user, from_device}, requests)
        request_state = Map.put(request_state, {to_user, to_device}, requests_to_device)
        encryption_state = Map.put(encryption_state, :ssss_requests, request_state)
        polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
        Map.put(state, :polyjuice, polyjuice_state)

      _ ->
        state
    end
  end
end
