# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.Encryption.MegolmSession do
  @moduledoc """
  Utility functions for megolm sessions.

  This module defines a struct that represents a Megolm session that can be
  used for decrypting or (optionally) encrypting.
  """

  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use TypedStruct

  typedstruct do
    field :device, Util.Device.t(), enforce: true
    field :room_id, String.t(), enforce: true
    field :id, String.t(), enforce: true
    field :outbound, Polyjuice.Newt.GroupSession.t()
    field :inbound, Polyjuice.Newt.InboundGroupSession.t(), enforce: true
    field :first_message_index, non_neg_integer, default: 0
    field :forwarded_count, non_neg_integer, default: 0
    field :is_verified, boolean, default: false
    field :opts, list(), default: []
  end

  @doc """
  Create a new Megolm session.

  The session can be used for encrypting and decrypting.
  """
  @spec create(Util.Device.t(), String.t(), Keyword.t()) :: __MODULE__.t()
  def create(%Util.Device{} = device, room_id, opts \\ []) do
    session = Polyjuice.Newt.GroupSession.create()
    session_id = Polyjuice.Newt.GroupSession.get_id(session)
    key = Polyjuice.Newt.GroupSession.get_key(session)
    {:ok, inbound} = Polyjuice.Newt.InboundGroupSession.create(key)

    %__MODULE__{
      device: device,
      room_id: room_id,
      id: session_id,
      outbound: session,
      inbound: inbound,
      is_verified: true,
      opts: opts
    }
  end

  @doc """
  Store the session in the key backup.

  The user must have set up key backup.
  """
  @spec store_in_backup(__MODULE__.t()) :: __MODULE__.t()
  def store_in_backup(%__MODULE__{device: %{user: %{key_backup: %{}} = user} = device} = session) do
    payload = %{
      "algorithm" => Polyjuice.ClientTest.Util.Encryption.megolm_v1_algorithm(),
      "forwarding_curve25519_key_chain" => [],
      "sender_key" => Polyjuice.Newt.Account.curve25519_key(device.olm_account),
      "sender_claimed_keys" => %{
        "ed25519" => Polyjuice.Newt.Account.ed25519_key(device.olm_account)
      },
      # FIXME: select current index
      "session_key" =>
        Polyjuice.Newt.InboundGroupSession.export_at_first_known_index(session.inbound)
    }

    payload =
      case Keyword.fetch(session.opts, :untrusted) do
        {:ok, untrusted} -> Map.put(payload, "untrusted", untrusted)
        :error -> payload
      end

    session_data =
      case user.key_backup do
        %{algorithm: "org.matrix.msc3270.v1.aes-hmac-sha2", key: key} ->
          {key_ciphertext, key_iv, key_mac} =
            Util.SSSS.encrypt_aes(
              Jason.encode!(payload),
              session.id,
              key
            )

          %{
            "ciphertext" => Base.encode64(key_ciphertext, padding: false),
            "iv" => Base.encode64(key_iv, padding: false),
            "mac" => Base.encode64(key_mac, padding: false)
          }

        %{algorithm: "m.megolm_backup.v1.curve25519-aes-sha2", public_key: pub_key} ->
          {ephemeral_pub_key, ephemeral_priv_key} = :crypto.generate_key(:eddh, :x25519)
          shared_secret = :crypto.compute_key(:eddh, pub_key, ephemeral_priv_key, :x25519)

          <<aes_key::binary-32, mac_key::binary-32, iv::binary-16>> =
            Util.SSSS.hkdf(shared_secret, 80, "")

          ciphertext =
            payload
            |> Jason.encode!()
            |> (&:crypto.crypto_one_time(:aes_256_cbc, aes_key, iv, &1,
                  encrypt: true,
                  padding: :pkcs_padding
                )).()

          <<mac::binary-8, _rest::binary>> = :crypto.mac(:hmac, :sha256, mac_key, "")

          %{
            "ephemeral" => Base.encode64(ephemeral_pub_key, padding: false),
            "ciphertext" => Base.encode64(ciphertext, padding: false),
            "mac" => Base.encode64(mac, padding: false)
          }
      end

    Protocols.KeyBackup.put_keys(user.server, user.id, user.key_backup.version, %{
      session.room_id => %{
        session.id => %{
          first_message_index: session.first_message_index,
          forwarded_count: session.forwarded_count,
          is_verified: session.is_verified,
          session_data: session_data
        }
      }
    })

    session
  end

  @doc """
  Encrypt an event with the session.
  """
  @spec encrypt(__MODULE__.t(), String.t(), Polyjuice.Util.event_content()) ::
          Polyjuice.Util.event_content()
  def encrypt(
        %__MODULE__{outbound: session, id: session_id, room_id: room_id, device: device},
        type,
        content
      ) do
    ciphertext =
      Polyjuice.Newt.GroupSession.encrypt(
        session,
        Jason.encode!(%{
          "room_id" => room_id,
          "type" => type,
          "content" => content
        })
      )

    %{
      "algorithm" => Polyjuice.ClientTest.Util.Encryption.megolm_v1_algorithm(),
      "ciphertext" => ciphertext,
      "device_id" => device.id,
      "sender_key" => Polyjuice.Newt.Account.curve25519_key(device.olm_account),
      "session_id" => session_id
    }
  end

  @doc """
  Wait until the given device has received the given megolm session.

  This function depends on using the
  `Polyjuice.ClientTest.Util.Encryption.olm_event_handler` event handler.

  The returned `MegolmSession` struct can only be used for decrypting.
  """
  @spec wait_for_session(Util.Device.t(), String.t(), String.t()) :: __MODULE__.t()
  def wait_for_session(%Util.Device{} = device, room_id, session_id) do
    session =
      Util.Encryption.wait_for_megolm_session(
        device.user.server,
        device.user.id,
        device.id,
        room_id,
        session_id
      )

    session_id = Polyjuice.Newt.InboundGroupSession.get_id(session)

    %__MODULE__{
      device: device,
      room_id: room_id,
      id: session_id,
      inbound: session,
      # FIXME: set parameters correctly
      is_verified: true,
      opts: []
    }
  end

  @doc """
  Decrypt an event using the megolm session.
  """
  @spec decrypt_event(__MODULE__.t(), Polyjuce.Util.event()) ::
          {:ok, Polyjuice.Util.event()} | :error
  def decrypt_event(%__MODULE__{room_id: session_room_id, inbound: session}, %{
        "content" => %{} = content,
        "room_id" => event_room_id
      }) do
    use Towel

    with ^session_room_id <- event_room_id,
         {:ok, {plaintext, _index}} <-
           Polyjuice.Newt.InboundGroupSession.decrypt(
             session,
             content["ciphertext"]
           ),
         {:ok, %{"room_id" => ^event_room_id} = decrypted_event} <- Jason.decode(plaintext) do
      {:ok, decrypted_event}
    else
      _ -> :error
    end
  end
end
