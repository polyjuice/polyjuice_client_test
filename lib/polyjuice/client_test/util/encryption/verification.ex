# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.Encryption.Verification do
  @moduledoc """
  Utility functions for device verification.
  """

  alias Polyjuice.ClientTest.Util

  use TypedStruct

  typedstruct do
    field :from, Util.Device.t(), enforce: true
    field :to, Util.Device.t(), enforce: true
    field :channel, Util.Encryption.Verification.Channel.t(), enforce: true
    field :state, {atom, any}, enforce: true
  end

  defprotocol Channel do
    @moduledoc false
    def get_txnid(channel)
    def send_event(channel, type, event)
    def wait_until_event(channel, type, match \\ fn _ -> true end, timeout)

    def wait_until_event_or_start(
          channel,
          verification,
          method,
          type,
          match \\ fn _ -> true end,
          timeout
        )

    def update_state(channel, state)
  end

  defmodule ToDeviceChannel do
    @moduledoc false
    use TypedStruct

    alias Polyjuice.ClientTest.Util

    typedstruct do
      field :from, Util.Device.t(), enforce: true
      field :to, Util.Device.t(), enforce: true
      field :txnid, String.t(), enforce: true
    end

    defimpl Util.Encryption.Verification.Channel do
      def get_txnid(%{txnid: txnid}), do: txnid

      def send_event(%{from: from, to: to, txnid: txnid}, type, event) do
        event = Map.put(event, "transaction_id", txnid)

        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          from.user.server,
          from.user.id,
          from.id,
          "polyjuice_verification_#{type}_#{to.user.id}_#{to.id}_#{txnid}",
          type,
          %{
            to.user.id => %{
              to.id => event
            }
          }
        )

        event
      end

      def wait_until_event(%{from: from, to: to, txnid: txnid}, type, match, timeout) do
        from_user_id = from.user.id
        from_device_id = from.id
        to_user_id = to.user.id

        Polyjuice.ClientTest.BaseTestServer.wait_until_event(
          from.user.server,
          fn
            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => "m.key.verification.cancel",
               "sender" => ^to_user_id,
               "content" => %{"transaction_id" => ^txnid}
             }} ->
              {:error, :cancelled}

            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => ^type,
               "sender" => ^to_user_id,
               "content" => %{"transaction_id" => ^txnid} = content
             }} ->
              case match.(content) do
                true -> {:ok, content}
                false -> false
                {:error, _} = res -> res
              end

            _ ->
              false
          end,
          timeout
        )
      end

      def wait_until_event_or_start(
            %{from: from, to: to, txnid: txnid},
            _verification,
            method,
            type,
            match,
            timeout
          ) do
        from_user_id = from.user.id
        from_device_id = from.id
        to_user_id = to.user.id
        to_device_id = to.id

        use_other =
          cond do
            to_user_id < from_user_id -> true
            to_user_id == from_user_id -> to.id < from.id
            true -> false
          end

        Polyjuice.ClientTest.BaseTestServer.wait_until_event(
          from.user.server,
          fn
            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => "m.key.verification.cancel",
               "sender" => ^to_user_id,
               "content" => %{"transaction_id" => ^txnid}
             }} ->
              {:error, :cancelled}

            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => ^type,
               "sender" => ^to_user_id,
               "content" => %{"transaction_id" => ^txnid} = content
             }} ->
              case match.(content) do
                true -> {:ok, content}
                false -> false
                {:error, _} = res -> res
              end

            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => "m.key.verification.start",
               "sender" => ^to_user_id,
               "content" =>
                 %{
                   "from_device" => ^to_device_id,
                   "transaction_id" => ^txnid,
                   "method" => ^method
                 } = content
             }} ->
              if use_other, do: {:error, {:use_other_start, content}}, else: false

            {:send_to_device, ^from_user_id, ^from_device_id,
             %{
               "type" => "m.key.verification.start",
               "sender" => ^to_user_id,
               "content" => %{"from_device" => ^to_device_id, "transaction_id" => ^txnid}
             }} ->
              {:error, :start_method_conflict}

            _ ->
              false
          end,
          timeout
        )
      end

      def update_state(
            %{from: from, to: to, txnid: txnid},
            txn_state
          ) do
        server = from.user.server
        from_user = from.user.id
        from_device = from.id
        to_user = to.user.id

        Polyjuice.ClientTest.BaseTestServer.get_and_update_test_state(server, fn state ->
          with {:ok, polyjuice_state} <- Map.fetch(state, :polyjuice),
               {:ok, encryption_state} <- Map.fetch(polyjuice_state, :encryption),
               {:ok, verification_state} <- Map.fetch(encryption_state, :device_verification),
               {:ok, device_verifications} <-
                 Map.fetch(verification_state, {from_user, from_device}),
               {:ok, verification_transactions} <- Map.fetch(device_verifications, to_user),
               {:ok, txn_info} <- Map.fetch(verification_transactions, txnid) do
            txn_info = Map.put(txn_info, :state, txn_state)

            verification_transactions = Map.put(verification_transactions, txnid, txn_info)

            device_verifications =
              Map.put(device_verifications, to_user, verification_transactions)

            verification_state =
              Map.put(verification_state, {from_user, from_device}, device_verifications)

            encryption_state = Map.put(encryption_state, :device_verification, verification_state)
            polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
            state = Map.put(state, :polyjuice, polyjuice_state)

            {:ok, state}
          else
            _ -> {{:error, :unknown_transaction}, state}
          end
        end)
      end
    end
  end

  defmodule RoomChannel do
    @moduledoc false
    use TypedStruct

    alias Polyjuice.ClientTest.Util

    typedstruct do
      field :from, Util.Device.t(), enforce: true
      field :to, Util.User.t(), enforce: true
      field :room_id, String.t(), enforce: true
      field :event_id, String.t(), enforce: true
    end

    defimpl Util.Encryption.Verification.Channel do
      def get_txnid(%{event_id: event_id}), do: event_id

      def send_event(channel, type, event) do
        event =
          Map.put(event, "m.relates_to", %{
            "event_id" => channel.event_id,
            "rel_type" => "m.reference"
          })

        Polyjuice.Server.Protocols.Room.send_event(
          channel.from.user.server,
          channel.from.user.id,
          channel.from.id,
          channel.room_id,
          type,
          nil,
          nil,
          event
        )

        event
      end

      def wait_until_event(channel, type, match, timeout) do
        to_user_id = channel.to.id
        room_id = channel.room_id
        event_id = channel.event_id

        Polyjuice.ClientTest.BaseTestServer.wait_until_event(
          channel.from.user.server,
          fn
            {:send_event, ^to_user_id, ^room_id,
             %{
               "type" => "m.key.verification.cancel",
               "content" => %{
                 "m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"}
               }
             }} ->
              {:error, :cancelled}

            {:send_event, ^to_user_id, ^room_id,
             %{
               "type" => ^type,
               "content" =>
                 %{"m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"}} =
                     content
             }} ->
              case match.(content) do
                true -> {:ok, content}
                false -> false
                {:error, _} = res -> res
              end

            _ ->
              false
          end,
          timeout
        )
      end

      def wait_until_event_or_start(channel, verification, method, type, match, timeout) do
        to_user_id = channel.to.id
        room_id = channel.room_id
        event_id = channel.event_id

        use_other =
          cond do
            to_user_id < channel.from.user.id -> true
            to_user_id == channel.from.user.id -> verification.to.id < channel.from.id
            true -> false
          end

        Polyjuice.ClientTest.BaseTestServer.wait_until_event(
          channel.from.user.server,
          fn
            {:send_event, ^to_user_id, ^room_id,
             %{
               "type" => "m.key.verification.cancel",
               "content" => %{
                 "m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"}
               }
             }} ->
              {:error, :cancelled}

            {:send_event, ^to_user_id, ^room_id,
             %{
               "type" => ^type,
               "content" =>
                 %{"m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"}} =
                     content
             }} ->
              case match.(content) do
                true -> {:ok, content}
                false -> false
                {:error, _} = res -> res
              end

            {:send_to_device, ^to_user_id, ^room_id,
             %{
               "type" => "m.key.verification.start",
               "content" =>
                 %{
                   "m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"},
                   "method" => ^method
                 } = content
             }} ->
              if use_other, do: {:error, {:use_other_start, content}}, else: false

            {:send_event, ^to_user_id, ^room_id,
             %{
               "type" => "m.key.verification.start",
               "content" => %{
                 "m.relates_to" => %{"event_id" => ^event_id, "rel_type" => "m.reference"}
               }
             }} ->
              {:error, :start_method_conflict}

            _ ->
              false
          end,
          timeout
        )
      end

      def update_state(
            %{from: from, event_id: event_id},
            txn_state
          ) do
        server = from.user.server
        from_user = from.user.id

        Polyjuice.ClientTest.BaseTestServer.get_and_update_test_state(server, fn state ->
          with {:ok, polyjuice_state} <- Map.fetch(state, :polyjuice),
               {:ok, encryption_state} <- Map.fetch(polyjuice_state, :encryption),
               {:ok, verification_state} <- Map.fetch(encryption_state, :room_verification),
               {:ok, verification_transactions} <- Map.fetch(verification_state, from_user),
               {:ok, txn_info} <- Map.fetch(verification_transactions, event_id) do
            txn_info = Map.put(txn_info, :state, txn_state)

            verification_transactions = Map.put(verification_transactions, event_id, txn_info)

            verification_state = Map.put(verification_state, from_user, verification_transactions)

            encryption_state = Map.put(encryption_state, :room_verification, verification_state)
            polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
            state = Map.put(state, :polyjuice, polyjuice_state)

            {:ok, state}
          else
            _ -> {{:error, :unknown_transaction}, state}
          end
        end)
      end
    end
  end

  @spec request_to_device(
          Util.Device.t(),
          Util.Device.t(),
          list(String.t()),
          pos_integer | :infinity
        ) :: t()
  def request_to_device(
        %Util.Device{user: from_user} = from_device,
        %Util.Device{user: to_user} = to_device,
        methods,
        timeout \\ 120_000
      )
      when is_list(methods) do
    server = from_user.server

    txnid =
      Polyjuice.ClientTest.BaseTestServer.get_and_update_test_state(server, fn state ->
        polyjuice_state = Map.get(state, :polyjuice, %{})
        encryption_state = Map.get(polyjuice_state, :encryption, %{})
        verification_state = Map.get(encryption_state, :device_verification, %{})
        device_verifications = Map.get(verification_state, {from_user.id, from_device.id}, %{})
        verification_transactions = Map.get(device_verifications, to_user.id, %{})

        txnid = Polyjuice.ClientTest.Util.make_random_key(verification_transactions)

        verification_transactions =
          Map.put(verification_transactions, txnid, %{
            other_device: to_device.id,
            state: :requesting,
            requested_methods: methods,
            start_time: :erlang.monotonic_time(:millisecond)
          })

        device_verifications =
          Map.put(device_verifications, to_user.id, verification_transactions)

        verification_state =
          Map.put(verification_state, {from_user.id, from_device.id}, device_verifications)

        encryption_state = Map.put(encryption_state, :device_verification, verification_state)
        polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
        state = Map.put(state, :polyjuice, polyjuice_state)

        {txnid, state}
      end)

    channel = %Util.Encryption.Verification.ToDeviceChannel{
      from: from_device,
      to: to_device,
      txnid: txnid
    }

    Util.Encryption.Verification.Channel.send_event(
      channel,
      "m.key.verification.request",
      %{
        "from_device" => from_device.id,
        "methods" => methods,
        "timestamp" => :erlang.system_time(:millisecond)
      }
    )

    to_device_id = to_device.id

    {:ok, %{"methods" => methods}} =
      Util.Encryption.Verification.Channel.wait_until_event(
        channel,
        "m.key.verification.ready",
        fn
          %{"from_device" => ^to_device_id, "methods" => methods} when is_list(methods) -> true
          _ -> false
        end,
        timeout
      )

    Util.Encryption.Verification.Channel.update_state(channel, :ready)

    %__MODULE__{
      from: from_device,
      to: to_device,
      channel: channel,
      state: {:accept, methods}
    }
  end

  @spec request_in_room(
          Util.Device.t(),
          Util.Device.t(),
          String.t(),
          list(String.t()),
          pos_integer | :infinity
        ) :: t()
  def request_in_room(
        %Util.Device{user: from_user} = from_device,
        %Util.User{} = to_user,
        room_id,
        methods,
        timeout \\ 120_000
      )
      when is_list(methods) do
    server = from_user.server

    {:ok, event_id} =
      Polyjuice.Server.Protocols.Room.send_event(
        server,
        from_user.id,
        from_device.id,
        room_id,
        "m.room.message",
        nil,
        nil,
        %{
          "msgtype" => "m.key.verification.request",
          "body" =>
            "#{from_user.id} wants to verify your device, but your client does not support verification",
          "to" => to_user.id,
          "from_device" => from_device.id,
          "methods" => methods
        }
      )

    channel = %Util.Encryption.Verification.RoomChannel{
      from: from_device,
      to: to_user,
      room_id: room_id,
      event_id: event_id
    }

    Util.Encryption.Verification.Channel.send_event(
      channel,
      "m.key.verification.request",
      %{
        "from_device" => from_device.id,
        "methods" => methods,
        "timestamp" => :erlang.system_time(:millisecond)
      }
    )

    {:ok, %{"methods" => methods, "from_device" => to_device_id}} =
      Util.Encryption.Verification.Channel.wait_until_event(
        channel,
        "m.key.verification.ready",
        fn
          %{"from_device" => to_device_id, "methods" => methods}
          when is_binary(to_device_id) and is_list(methods) ->
            true

          _ ->
            false
        end,
        timeout
      )

    Util.Encryption.Verification.Channel.update_state(channel, :ready)

    %__MODULE__{
      from: from_device,
      to: %Util.Device{user: to_user, id: to_device_id},
      channel: channel,
      state: {:accept, methods}
    }
  end

  defmodule SAS do
    @moduledoc false

    use Towel

    alias Polyjuice.ClientTest.Util
    alias Polyjuice.ClientTest.BaseTestServer

    defp await_first(tasks) do
      awaiting =
        for task <- tasks, into: %{} do
          %Task{ref: ref, owner: owner} = task

          if owner != self() do
            raise ArgumentError,
                  "task #{inspect(task)} must be queried from the owner but was queried from #{
                    inspect(self())
                  }"
          end

          {ref, true}
        end

      do_await_first(tasks, awaiting)
    end

    defp do_await_first(tasks, awaiting) do
      receive do
        {:DOWN, ref, _, _proc, reason} when is_map_key(awaiting, ref) ->
          remaining = Map.delete(awaiting, ref)

          if remaining == %{} do
            exit({reason, {__MODULE__, :await_many, [tasks]}})
          else
            do_await_first(tasks, remaining)
          end

        {ref, reply} when is_map_key(awaiting, ref) ->
          Enum.each(awaiting, fn {ref, _} ->
            Process.demonitor(ref, [:flush])
          end)

          reply
      end
    end

    def verify(
          %Util.Encryption.Verification{
            channel: channel
          } = verification,
          keys,
          opts \\ []
        ) do
      server = verification.from.user.server

      # FIXME: timeout
      txnid = Util.Encryption.Verification.Channel.get_txnid(channel)

      case await_first([
             Task.async(fn ->
               # for some reason, it doesn't get the start event without this
               IO.puts("")

               {:ok, start_msg} =
                 Util.Encryption.Verification.Channel.wait_until_event(
                   verification.channel,
                   "m.key.verification.start",
                   fn
                     %{"method" => "m.sas.v1"} -> true
                     _ -> false
                   end,
                   # FIXME:
                   60_000
                 )

               {:their_start, start_msg}
             end),
             Task.async(fn ->
               BaseTestServer.prompt(
                 server,
                 Polyjuice.ClientTest.Fluent,
                 "start-verification-emoji",
                 [],
                 {:choice,
                  [{:our_start, {Polyjuice.ClientTest.Fluent, "send-start-verification"}}]}
               )
             end)
           ]) do
        {:their_start, start_msg} ->
          ok(%{
            verification: verification,
            start_msg: start_msg,
            keys: keys,
            opts: opts,
            txnid: txnid,
            started: :them
          })
          # FIXME:
          # |> fmap(fn state ->
          #   # check that the transaction hasn't already been started, and if not,
          #   # mark the transaction as in progress
          #   Polyjuice.ClientTest.BaseTestServer.get_and_update_test_state(server, fn test_state ->
          #     polyjuice_state = Map.get(test_state, :polyjuice, %{})
          #     encryption_state = Map.get(polyjuice_state, :encryption, %{})
          #     verification_state = Map.get(encryption_state, :device_verification, %{})
          #     device_verifications = Map.get(verification_state, {from_user.id, from_device.id}, %{})
          #     verification_transactions = Map.get(device_verifications, to_user.id, %{})

          #     case Map.fetch(verification_transactions, txnid) do
          #       {:ok, %{state: state}}
          #       when state != :ready and state != :cancelled and state != :done ->
          #         if state == :requested or state == :requesting do
          #           {error(:not_ready), test_state}
          #         else
          #           {error(:already_in_progress), test_state}
          #         end

          #       res ->
          #         # either no such transaction exists yet, or it's ready to start
          #         txn_info =
          #           case res do
          #             {:ok, txn_info} ->
          #               Map.merge(txn_info, %{state: :in_progress})

          #             _ ->
          #               %{
          #                 state: :in_progress,
          #                 other_device: to_device.id,
          #                 start_time: :erlang.monotonic_time(:millisecond)
          #               }
          #           end

          #         verification_transactions = Map.put(verification_transactions, txnid, txn_info)

          #         device_verifications =
          #           Map.put(device_verifications, to_user.id, verification_transactions)

          #         verification_state =
          #           Map.put(verification_state, {from_user.id, from_device.id}, device_verifications)

          #         encryption_state = Map.put(encryption_state, :device_verification, verification_state)
          #         polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
          #         test_state = Map.put(test_state, :polyjuice, polyjuice_state)

          #         {{:ok, state}, test_state}
          #     end
          #   end)
          # end)
          |> fmap(&check_start_event_validity/1)
          |> fmap(&send_accept_event/1)
          |> fmap(&wait_for_key_event/1)
          |> fmap(&send_key_event/1)
          |> fmap(&establish_sas/1)
          |> fmap(&display_emoji/1)
          |> fmap(&wait_for_mac_event/1)
          |> fmap(&check_key_ids_mac/1)
          |> fmap(&check_device_keys_mac/1)
          # FIXME: sending the MAC event should be done after the user has verified
          # that the emoji match, but we don't ask that on the dashboard (yet)
          |> fmap(&send_mac_event/1)
          |> fmap(&send_done_event/1)
          |> fmap(&wait_for_done_event/1)

        {:ok, :our_start} ->
          ok(%{
            verification: verification,
            keys: keys,
            opts: opts,
            txnid: txnid,
            started: :us
          })
          |> fmap(&send_start_event/1)
          |> fmap(&wait_for_accept_event/1)
          |> fmap(&send_key_event/1)
          |> fmap(&wait_for_key_event/1)
          |> fmap(&check_commitment/1)
          |> fmap(&establish_sas/1)
          |> fmap(&display_emoji/1)
          |> fmap(&wait_for_mac_event/1)
          |> fmap(&check_key_ids_mac/1)
          |> fmap(&check_device_keys_mac/1)
          # FIXME: sending the MAC event should be done after the user has verified
          # that the emoji match, but we don't ask that on the dashboard (yet)
          |> fmap(&send_mac_event/1)
          |> fmap(&send_done_event/1)
          |> fmap(&wait_for_done_event/1)
      end
      |> fmap(fn state ->
        # mark as done
        Util.Encryption.Verification.Channel.update_state(channel, :done)

        ok([state.their_ed25519_key_id])
      end)
      |> (&%{
            verification
            | state:
                case &1 do
                  {:ok, devices} -> {:verified, devices}
                  {:error, _} -> &1
                end
          }).()
    end

    def send_start_event(%{verification: %{channel: channel} = verification} = state) do
      start_msg = %{
        "from_device" => verification.from.id,
        "method" => "m.sas.v1",
        # FIXME: allow selecting these parameters?
        "hashes" => ["sha256"],
        "key_agreement_protocols" => ["curve25519-hkdf-sha256"],
        "message_authentication_codes" => [
          "hkdf-hmac-sha256",
          "org.matrix.msc3783.hkdf-hmac-sha256",
          "hkdf-hmac-sha256.v2"
        ],
        "short_authentication_string" => ["decimal", "emoji"]
      }

      start_msg =
        Util.Encryption.Verification.Channel.send_event(
          channel,
          "m.key.verification.start",
          start_msg
        )

      ok(Map.merge(state, %{start_msg: start_msg}))
    end

    def check_start_event_validity(%{start_msg: start_msg} = state) do
      if start_msg["method"] == "m.sas.v1" and
           Map.get(start_msg, "hashes", []) |> Enum.member?("sha256") and
           Map.get(start_msg, "key_agreement_protocols", [])
           |> Enum.member?("curve25519-hkdf-sha256") and
           Map.get(start_msg, "message_authentication_codes", [])
           |> Enum.member?("hkdf-hmac-sha256") and
           Map.get(start_msg, "short_authentication_string", []) |> Enum.member?("decimal") do
        ok(state)
      else
        error(:invalid_start)
      end
    end

    def wait_for_accept_event(%{verification: %{channel: channel}} = state) do
      res =
        Util.Encryption.Verification.Channel.wait_until_event(
          channel,
          "m.key.verification.accept",
          # FIXME:
          60_000
        )

      case res do
        {:ok,
         %{
           "commitment" => commitment,
           "hash" => "sha256",
           "key_agreement_protocol" => "curve25519-hkdf-sha256",
           "message_authentication_code" => mac_method,
           "short_authentication_string" => sas
         }} ->
          mac_method =
            case mac_method do
              "hkdf-hmac-sha256" -> :old
              "hkdf-hmac-sha256.v2" -> :fixed
              "org.matrix.msc3783.hkdf-hmac-sha256" -> :fixed
              _ -> nil
            end

          cond do
            mac_method == nil ->
              {:error, :invalid_accept_event}

            not Enum.member?(sas, "emoji") ->
              {:error, :not_implemented}

            true ->
              ok(Map.merge(state, %{their_commitment: commitment, mac_method: mac_method}))
          end

        {:ok, _} ->
          {:error, :invalid_accept_event}

        {:error, _} ->
          res
      end
    end

    def send_accept_event(
          %{
            start_msg: start_msg,
            verification: %{from: %{user: %{server: server}}, channel: channel}
          } = state
        ) do
      # send the accept message and wait for key message
      {sas, pubkey} = Polyjuice.Newt.Sas.create()

      {:ok, commitment} =
        start_msg
        |> Polyjuice.Util.JSON.canonical_json()
        |> fmap(&(pubkey <> &1))
        |> fmap(&:crypto.hash(:sha256, &1))
        |> fmap(&Base.encode64(&1, padding: false))

      {mac_method, mac_method_name} =
        cond do
          Map.get(start_msg, "message_authentication_codes", [])
          |> Enum.member?("hkdf-hmac-sha256.v2") ->
            Polyjuice.ClientTest.BaseTestServer.send_info(
              server,
              Polyjuice.ClientTest.Fluent,
              "sas_verification_fixed_base64"
            )

            {:fixed, "hkdf-hmac-sha256.v2"}

          Map.get(start_msg, "message_authentication_codes", [])
          |> Enum.member?("org.matrix.msc3783.hkdf-hmac-sha256") ->
            Polyjuice.ClientTest.BaseTestServer.send_info(
              server,
              Polyjuice.ClientTest.Fluent,
              "sas_verification_fixed_base64_unstable"
            )

            {:fixed, "org.matrix.msc3783.hkdf-hmac-sha256"}

          true ->
            Polyjuice.ClientTest.BaseTestServer.send_warning(
              server,
              Polyjuice.ClientTest.Fluent,
              "sas_verification_invalid_base64"
            )

            {:old, "hkdf-hmac-sha256"}
        end

      Util.Encryption.Verification.Channel.send_event(
        channel,
        "m.key.verification.accept",
        %{
          "commitment" => commitment,
          # FIXME: allow selecting these parameters
          "hash" => "sha256",
          "key_agreement_protocol" => "curve25519-hkdf-sha256",
          "message_authentication_code" => mac_method_name,
          "short_authentication_string" => ["emoji"]
        }
      )

      ok(
        Map.merge(state, %{
          our_pubkey: pubkey,
          sas: sas,
          mac_method: mac_method
        })
      )
    end

    def wait_for_key_event(%{verification: %{channel: channel}} = state) do
      res =
        Util.Encryption.Verification.Channel.wait_until_event(
          channel,
          "m.key.verification.key",
          # FIXME:
          60_000
        )

      case res do
        {:ok, %{"key" => their_pubkey}} ->
          ok(
            Map.merge(state, %{
              their_pubkey: their_pubkey
            })
          )

        {:error, _} ->
          res
      end
    end

    def check_commitment(
          %{their_pubkey: their_pubkey, start_msg: start_msg, their_commitment: their_commitment} =
            state
        ) do
      {:ok, commitment} =
        start_msg
        |> Polyjuice.Util.JSON.canonical_json()
        |> fmap(&(their_pubkey <> &1))
        |> fmap(&:crypto.hash(:sha256, &1))
        |> fmap(&Base.encode64(&1, padding: false))

      if commitment == their_commitment do
        ok(state)
      else
        error(:incorrect_hash_commitment)
      end
    end

    def send_key_event(%{verification: %{channel: channel}, our_pubkey: our_pubkey} = state) do
      Util.Encryption.Verification.Channel.send_event(
        channel,
        "m.key.verification.key",
        %{
          "key" => our_pubkey
        }
      )

      ok(state)
    end

    def send_key_event(%{verification: %{channel: channel}} = state) do
      {sas, our_pubkey} = Polyjuice.Newt.Sas.create()

      Util.Encryption.Verification.Channel.send_event(
        channel,
        "m.key.verification.key",
        %{
          "key" => our_pubkey
        }
      )

      ok(Map.merge(state, %{sas: sas, our_pubkey: our_pubkey}))
    end

    def establish_sas(%{sas: sas, their_pubkey: their_pubkey} = state) do
      {:ok, established_sas} = Polyjuice.Newt.Sas.establish(sas, their_pubkey)

      {calculate_mac, verify_mac} =
        case state.mac_method do
          :old ->
            {&Polyjuice.Newt.Sas.calculate_mac_invalid_base64(established_sas, &1, &2),
             fn input, info, tag ->
               expected =
                 Polyjuice.Newt.Sas.calculate_mac_invalid_base64(established_sas, input, info)

               if String.trim_trailing(expected, "=") == String.trim_trailing(tag, "="),
                 do: {:ok, {}},
                 else: {:error, :invalid_mac}
             end}

          :fixed ->
            {&Polyjuice.Newt.Sas.calculate_mac(established_sas, &1, &2),
             &Polyjuice.Newt.Sas.verify_mac(established_sas, &1, &2, &3)}
        end

      ok(
        Map.merge(state, %{
          established_sas: established_sas,
          calculate_mac: calculate_mac,
          verify_mac: verify_mac
        })
      )
    end

    def display_emoji(
          %{
            verification: %{
              from: %{user: from_user} = from_device,
              to: %{user: to_user} = to_device
            },
            txnid: txnid,
            established_sas: established_sas,
            started: started
          } = state
        ) do
      emoji_info =
        case started do
          :them ->
            "MATRIX_KEY_VERIFICATION_SAS|#{to_user.id}|#{to_device.id}|#{state.their_pubkey}|#{
              from_user.id
            }|#{from_device.id}|#{state.our_pubkey}|#{txnid}"

          :us ->
            "MATRIX_KEY_VERIFICATION_SAS|#{from_user.id}|#{from_device.id}|#{state.our_pubkey}|#{
              to_user.id
            }|#{to_device.id}|#{state.their_pubkey}|#{txnid}"
        end

      %{emoji: emoji_indices} =
        Polyjuice.Newt.Sas.generate_auth_string(established_sas, emoji_info)

      [
        {emoji1, emoji1_name},
        {emoji2, emoji2_name},
        {emoji3, emoji3_name},
        {emoji4, emoji4_name},
        {emoji5, emoji5_name},
        {emoji6, emoji6_name},
        {emoji7, emoji7_name}
      ] = Tuple.to_list(emoji_indices) |> Enum.map(&Polyjuice.Newt.Sas.emoji_for_index/1)

      {:ok, prompt_event_id} =
        Polyjuice.ClientTest.BaseTestServer.send_message(
          from_user.server,
          Polyjuice.ClientTest.Fluent,
          "verify-sas-emoji",
          emoji1: emoji1,
          emoji1_name: emoji1_name,
          emoji2: emoji2,
          emoji2_name: emoji2_name,
          emoji3: emoji3,
          emoji3_name: emoji3_name,
          emoji4: emoji4,
          emoji4_name: emoji4_name,
          emoji5: emoji5,
          emoji5_name: emoji5_name,
          emoji6: emoji6,
          emoji6_name: emoji6_name,
          emoji7: emoji7,
          emoji7_name: emoji7_name
        )

      # FIXME: prompt to verify emoji on our side too

      ok(Map.merge(state, %{sas_prompt_event_id: prompt_event_id}))
    end

    def wait_for_mac_event(
          %{verification: %{channel: channel, from: %{user: %{server: server}}}} = state
        ) do
      res =
        Util.Encryption.Verification.Channel.wait_until_event(
          channel,
          "m.key.verification.mac",
          # FIXME:
          60_000
        )

      Polyjuice.ClientTest.BaseTestServer.send_done(server, state.sas_prompt_event_id)

      case res do
        {:ok, %{"mac" => _, "keys" => _} = mac} ->
          ok(
            Map.merge(state, %{
              their_mac_event: mac
            })
          )

        {:error, _} ->
          res
      end
    end

    def send_mac_event(
          %{
            verification: %{
              from: %{user: from_user} = from_device,
              to: %{user: to_user} = to_device,
              channel: channel
            },
            txnid: txnid,
            keys: keys
          } = state
        ) do
      our_keys_mac_info =
        "MATRIX_KEY_VERIFICATION_MAC#{from_user.id}#{from_device.id}#{to_user.id}#{to_device.id}#{
          txnid
        }KEY_IDS"

      key_list_mac =
        Map.keys(keys)
        |> Enum.sort()
        |> Enum.join(",")
        |> (&state.calculate_mac.(&1, our_keys_mac_info)).()

      mac =
        Enum.map(keys, fn {name, key} ->
          info =
            "MATRIX_KEY_VERIFICATION_MAC#{from_user.id}#{from_device.id}#{to_user.id}#{
              to_device.id
            }#{txnid}#{name}"

          {name, state.calculate_mac.(key, info)}
        end)
        |> Map.new()

      Util.Encryption.Verification.Channel.send_event(
        channel,
        "m.key.verification.mac",
        %{
          "keys" => key_list_mac,
          "mac" => mac,
          "transaction_id" => txnid
        }
      )

      ok(state)
    end

    def check_key_ids_mac(
          %{
            verification: %{
              from: %{user: from_user} = from_device,
              to: %{user: to_user} = to_device
            },
            txnid: txnid
          } = state
        ) do
      # check the mac of the keys IDs
      their_keys_mac_info =
        "MATRIX_KEY_VERIFICATION_MAC#{to_user.id}#{to_device.id}#{from_user.id}#{from_device.id}#{
          txnid
        }KEY_IDS"

      key_list_data =
        Map.keys(state.their_mac_event["mac"])
        |> Enum.sort()
        |> Enum.join(",")

      if state.verify_mac.(key_list_data, their_keys_mac_info, state.their_mac_event["keys"]) ==
           {:ok, {}} do
        ok(state)
      else
        error(:incorrect_mac)
      end
    end

    def check_device_keys_mac(
          %{
            verification: %{
              from: %{user: from_user} = from_device,
              to: %{user: to_user} = to_device
            },
            txnid: txnid
          } = state
        ) do
      # check the mac of their device keys
      to_user_id = to_user.id
      to_device_id = to_device.id

      %{"device_keys" => %{^to_user_id => %{^to_device_id => their_device_keys}}} =
        Polyjuice.Server.Protocols.DeviceKey.query_keys(
          from_user.server,
          to_user.id,
          %{to_user.id => [to_device.id]},
          nil
        )

      # FIXME: check other keys too
      their_ed25519_key_id = "ed25519:" <> to_device.id

      key = their_device_keys["keys"][their_ed25519_key_id]
      key_mac = state.their_mac_event["mac"][their_ed25519_key_id]

      info =
        "MATRIX_KEY_VERIFICATION_MAC#{to_user.id}#{to_device.id}#{from_user.id}#{from_device.id}#{
          txnid
        }#{their_ed25519_key_id}"

      if state.verify_mac.(key, info, key_mac) == {:ok, {}} do
        ok(Map.merge(state, %{their_ed25519_key_id: their_ed25519_key_id}))
      else
        error(:incorrect_mac)
      end

      # FIXME: also check their cross-signing key, if available
    end

    def send_done_event(%{verification: %{channel: channel}} = state) do
      Util.Encryption.Verification.Channel.send_event(
        channel,
        "m.key.verification.done",
        %{}
      )

      ok(state)
    end

    def wait_for_done_event(%{verification: %{channel: channel}} = state) do
      res =
        Util.Encryption.Verification.Channel.wait_until_event(
          channel,
          "m.key.verification.done",
          # FIXME:
          60_000
        )

      case res do
        {:ok, _} -> ok(state)
        {:error, _} -> res
      end
    end
  end

  @spec accept_emoji_verification(
          t(),
          Polyjuice.Util.event_content(),
          %{required(String.t()) => String.t()},
          Keyword.t()
        ) :: t()
  def accept_emoji_verification(
        %__MODULE__{
          channel: channel
        } = verification,
        start_msg,
        keys,
        opts \\ []
      )
      when is_map(start_msg) and is_map(keys) and is_list(opts) do
    # FIXME: timeout
    txnid = Util.Encryption.Verification.Channel.get_txnid(channel)

    alias Util.Encryption.Verification.SAS

    use Towel

    ok(%{
      verification: verification,
      start_msg: start_msg,
      keys: keys,
      opts: opts,
      txnid: txnid
    })
    # FIXME:
    # |> fmap(fn state ->
    #   # check that the transaction hasn't already been started, and if not,
    #   # mark the transaction as in progress
    #   Polyjuice.ClientTest.BaseTestServer.get_and_update_test_state(server, fn test_state ->
    #     polyjuice_state = Map.get(test_state, :polyjuice, %{})
    #     encryption_state = Map.get(polyjuice_state, :encryption, %{})
    #     verification_state = Map.get(encryption_state, :device_verification, %{})
    #     device_verifications = Map.get(verification_state, {from_user.id, from_device.id}, %{})
    #     verification_transactions = Map.get(device_verifications, to_user.id, %{})

    #     case Map.fetch(verification_transactions, txnid) do
    #       {:ok, %{state: state}}
    #       when state != :ready and state != :cancelled and state != :done ->
    #         if state == :requested or state == :requesting do
    #           {error(:not_ready), test_state}
    #         else
    #           {error(:already_in_progress), test_state}
    #         end

    #       res ->
    #         # either no such transaction exists yet, or it's ready to start
    #         txn_info =
    #           case res do
    #             {:ok, txn_info} ->
    #               Map.merge(txn_info, %{state: :in_progress})

    #             _ ->
    #               %{
    #                 state: :in_progress,
    #                 other_device: to_device.id,
    #                 start_time: :erlang.monotonic_time(:millisecond)
    #               }
    #           end

    #         verification_transactions = Map.put(verification_transactions, txnid, txn_info)

    #         device_verifications =
    #           Map.put(device_verifications, to_user.id, verification_transactions)

    #         verification_state =
    #           Map.put(verification_state, {from_user.id, from_device.id}, device_verifications)

    #         encryption_state = Map.put(encryption_state, :device_verification, verification_state)
    #         polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
    #         test_state = Map.put(test_state, :polyjuice, polyjuice_state)

    #         {{:ok, state}, test_state}
    #     end
    #   end)
    # end)
    |> fmap(&SAS.check_start_event_validity/1)
    |> fmap(&SAS.send_accept_event/1)
    |> fmap(&SAS.wait_for_key_event/1)
    |> fmap(&SAS.send_key_event/1)
    |> fmap(&SAS.establish_sas/1)
    |> fmap(&SAS.display_emoji/1)
    |> fmap(&SAS.wait_for_mac_event/1)
    |> fmap(&SAS.check_key_ids_mac/1)
    |> fmap(&SAS.check_device_keys_mac/1)
    # FIXME: sending the MAC event should be done after the user has verified
    # that the emoji match, but we don't ask that on the dashboard (yet)
    |> fmap(&SAS.send_mac_event/1)
    |> fmap(&SAS.send_done_event/1)
    |> fmap(&SAS.wait_for_done_event/1)
    |> fmap(fn state ->
      # mark as done
      Util.Encryption.Verification.Channel.update_state(channel, :done)

      ok([state.their_ed25519_key_id])
    end)
    |> (&%{
          verification
          | state:
              case &1 do
                {:ok, devices} -> {:verified, devices}
                {:error, _} -> &1
              end
        }).()
  end

  @doc """
  Event handler that handles to-device verification events.

  This handler handles verification request and cancel events.

  This function can be used as an `Polyjuice.ClientTest.BaseTestServer` event
  handler.  Any devices whose to-events events should be decrypted must first
  have their olm account set by calling `set_account/4`.

  Using this function requires that the test state is a `Map` that has a
  `:polyjuice` key that it can control.
  """
  def to_device_event_handler(event, state, server) do
    polyjuice_state = Map.get(state, :polyjuice, %{})
    encryption_state = Map.get(polyjuice_state, :encryption, %{})

    case event do
      {:send_to_device, user_id, device_id,
       %{
         "type" => "m.key.verification.request",
         "sender" => from_user,
         "content" => %{
           "transaction_id" => txnid,
           "from_device" => from_device,
           "methods" => methods
         }
       }}
      when is_binary(txnid) and is_binary(from_device) and is_list(methods) ->
        verification_state = Map.get(encryption_state, :device_verification, %{})

        case Map.fetch(verification_state, {user_id, device_id}) do
          {:ok, device_verifications} ->
            verification_transactions = Map.get(device_verifications, from_user, %{})

            case Map.fetch(verification_transactions, txnid) do
              {:ok, _} ->
                # transaction already exists
                # FIXME: send a cancel event and set state to cancelled
                state

              :error ->
                Polyjuice.ClientTest.BaseTestServer.emit_event(
                  server,
                  {:verification_request, {from_user, from_device}, {user_id, device_id}, txnid}
                )

                verification_transactions =
                  Map.put(verification_transactions, txnid, %{
                    other_device: from_device,
                    state: :requested,
                    requested_methods: methods,
                    start_time: :erlang.monotonic_time(:millisecond)
                  })

                device_verifications =
                  Map.put(
                    device_verifications,
                    from_user,
                    verification_transactions
                  )

                verification_state =
                  Map.put(verification_state, {user_id, device_id}, device_verifications)

                encryption_state =
                  Map.put(encryption_state, :device_verification, verification_state)

                polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
                Map.put(state, :polyjuice, polyjuice_state)
            end

          :error ->
            state
        end

        state

      {:send_to_device, user_id, device_id,
       %{
         "type" => "m.key.verification.cancel",
         "sender" => from_user,
         "content" => %{
           "transaction_id" => txnid,
           "code" => code,
           "reason" => reason
         }
       }}
      when is_binary(from_user) and is_binary(txnid) and
             is_binary(code) and is_binary(reason) ->
        with {:ok, verification_state} <- Map.fetch(encryption_state, :device_verification),
             {:ok, device_verifications} <- Map.fetch(verification_state, {user_id, device_id}),
             {:ok, verification_transactions} <-
               Map.fetch(device_verifications, from_user),
             {:ok, %{other_device: from_device} = txn_info} <-
               Map.fetch(verification_transactions, txnid) do
          Polyjuice.ClientTest.BaseTestServer.emit_event(
            server,
            {:verification_cancelled, {from_user, from_device}, {user_id, device_id}, txnid}
          )

          txn_info =
            Map.merge(txn_info, %{state: :cancelled, cancel_code: code, cancel_reason: reason})

          verification_transactions = Map.put(verification_transactions, txnid, txn_info)

          device_verifications =
            Map.put(device_verifications, from_user, verification_transactions)

          verification_state =
            Map.put(verification_state, {user_id, device_id}, device_verifications)

          encryption_state = Map.put(encryption_state, :device_verification, verification_state)
          polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
          Map.put(state, :polyjuice, polyjuice_state)
        else
          _ -> state
        end

      _ ->
        state
    end
  end
end
