# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.Device do
  @moduledoc """
  Utility functions for devices.

  This module defines a struct that represents a user's logged-in device.
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols

  use TypedStruct

  typedstruct do
    field :user, Util.User.t(), enforce: true
    field :id, String.t(), enforce: true
    field :access_token, String.t() | nil
    field :olm_account, Polyjuice.Newt.Account.t() | nil
    field :txnid, non_neg_integer, default: 0
  end

  @doc """
  Log in a new device for the given user.
  """
  @spec log_in(Polyjuice.ClientTest.Util.User.t(), Keyword.t()) :: __MODULE__.t()
  def log_in(user, opts \\ []) do
    {:ok, %{device_id: device_id, access_token: access_token}} =
      Protocols.User.log_in(user.server, {:password, user.id, user.password}, opts)

    %__MODULE__{
      user: user,
      id: device_id,
      access_token: access_token
    }
  end

  @doc """
  Prompt the tester to log in as the user, and return the resulting device.

  Known options are:

  - `wait_for:` list of additional events to wait for.  Possible values are:
    - `:olm_otks` wait for the device to upload some olm one-time keys
    - `:device_keys` wait for the device to upload its device keys
  """
  @spec wait_for_login(Polyjuice.ClientTest.Util.User.t(), Keyword.t()) :: __MODULE__.t()
  def wait_for_login(user, opts \\ []) do
    {:ok, event_id} =
      BaseTestServer.send_message(
        user.server,
        Polyjuice.ClientTest.Fluent,
        "log-in-with-password",
        user: user.id,
        password: user.password
      )

    user_id = user.id

    device_id =
      BaseTestServer.wait_until_event(
        user.server,
        fn
          {:login, ^user_id, device_id, _} -> device_id
          _ -> false
        end
      )

    Keyword.get(opts, :wait_for, [])
    |> Enum.map(fn
      :olm_otks ->
        Task.async(fn ->
          BaseTestServer.wait_until_event(
            user.server,
            fn
              {:add_one_time_keys, ^user_id, ^device_id, _keys} -> true
              _ -> false
            end
          )
        end)

      :device_keys ->
        Task.async(fn ->
          BaseTestServer.wait_until_event(
            user.server,
            fn
              {:set_device_keys, ^user_id, %{"device_id" => ^device_id}} -> true
              _ -> false
            end
          )
        end)
    end)
    |> Enum.map(&Task.await(&1, 120_000))

    BaseTestServer.send_done(user.server, event_id)

    %__MODULE__{
      user: user,
      id: device_id
    }
  end

  @doc """
  Log out a device
  """
  @spec log_out(__MODULE__.t()) :: __MODULE__.t()
  def log_out(%__MODULE__{user: user} = device) do
    Protocols.User.log_out(user.server, user.id, device.id)
    %{device | access_token: nil}
  end
end
