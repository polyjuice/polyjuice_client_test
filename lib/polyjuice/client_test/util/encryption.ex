# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.Encryption do
  @moduledoc """
  Utility functions for end-to-end encryption.
  """

  alias Polyjuice.ClientTest.Util
  alias Polyjuice.Server.Protocols

  # some constants
  @olm_v1_algorithm "m.olm.v1.curve25519-aes-sha2"
  def olm_v1_algorithm(), do: @olm_v1_algorithm
  @megolm_v1_algorithm "m.megolm.v1.aes-sha2"
  def megolm_v1_algorithm(), do: @megolm_v1_algorithm

  @doc """
  Initialize a device's olm session.
  """
  @spec initialize_olm(Util.Device.t(), Keyword.t()) :: Util.Device.t()
  def initialize_olm(%Util.Device{user: user} = device, opts \\ []) do
    use Towel

    olm_account = Polyjuice.Newt.Account.create()

    set_account(user.server, user.id, device.id, olm_account)

    {:ok, device_keys} =
      %{
        "algorithms" => [
          olm_v1_algorithm(),
          megolm_v1_algorithm()
        ],
        "device_id" => device.id,
        "keys" => %{
          ("ed25519:" <> device.id) => Polyjuice.Newt.Account.ed25519_key(olm_account),
          ("curve25519:" <> device.id) => Polyjuice.Newt.Account.curve25519_key(olm_account)
        },
        "user_id" => user.id
      }
      |> Polyjuice.Util.JSON.sign(
        user.id,
        Polyjuice.Newt.Account.SigningKey.from_account(olm_account, device.id)
      )
      |> fmap(fn device_keys ->
        if user.cross_signing != nil and not Keyword.get(opts, :no_cross_signing, false) do
          Polyjuice.Util.JSON.sign(
            device_keys,
            user.id,
            Util.CrossSigning.make_signing_key(user.cross_signing, :ssk)
          )
        else
          {:ok, device_keys}
        end
      end)

    Protocols.DeviceKey.set_device_keys(
      user.server,
      user.id,
      device.id,
      device_keys
    )

    %{device | olm_account: olm_account}
  end

  @doc """
  Generate and upload olm one-time keys.
  """
  @spec upload_olm_otks(Util.Device.t(), pos_integer) :: Util.Device.t()
  def upload_olm_otks(%Util.Device{user: user, olm_account: olm_account} = device, count) do
    Polyjuice.Newt.Account.generate_one_time_keys(olm_account, count)

    olm_signing_key = Polyjuice.Newt.Account.SigningKey.from_account(olm_account, device.id)

    signed_otks =
      Polyjuice.Newt.Account.get_one_time_keys(olm_account)
      |> Enum.map(fn {id, key} ->
        {:ok, signed} =
          Polyjuice.Util.JSON.sign(
            %{
              "key" => key
            },
            user.id,
            olm_signing_key
          )

        {"signed_curve25519:" <> id, signed}
      end)
      |> Map.new()

    Protocols.DeviceKey.add_one_time_keys(user.server, user.id, device.id, signed_otks)

    Polyjuice.Newt.Account.mark_keys_as_published(olm_account)

    device
  end

  @doc """
  Send a room key from one device to another over olm.
  """
  @spec send_room_key_to(Util.Device.t(), Util.Encryption.MegolmSession.t(), Util.Device.t()) ::
          Util.Device.t()
  def send_room_key_to(
        %Util.Device{} = from,
        %Util.Encryption.MegolmSession{outbound: outbound} = session,
        %Util.Device{} = to,
        opts \\ []
      )
      when outbound != nil do
    key = Polyjuice.Newt.GroupSession.get_key(outbound)

    {:ok, encrypted_room_key} =
      Util.Encryption.encrypt_room_key_with_olm(
        from.user.server,
        session.room_id,
        {:megolm, session.id, key},
        from: {from.user.id, from.id},
        to: {to.user.id, to.id},
        olm_account: from.olm_account,
        extra_fields: Keyword.get(opts, :extra_fields, [])
      )

    Polyjuice.Server.Protocols.SendToDevice.send_to_device(
      from.user.server,
      from.user.id,
      from.id,
      "polyjuice_#{to_string(from.txnid)}",
      "m.room.encrypted",
      %{to.user.id => %{to.id => encrypted_room_key}}
    )

    %{from | txnid: from.txnid + 1}
  end

  @doc """
  Forward a room key from one device to another over olm.

  Recognized `opts` are:
  - `sender_key:` (required) the Curve25519 key of the device which initiated
    the session originally
  - `sender_claimed_ed25519_key:` (required) the (claimed) Ed25519 key of the
    device which initiated the session originally
  - `forwarding_curve25519_key_chain:` chain of Curve25519 keys that the room
    key was sent through.  Defaults to `[]`
  - `immediate:` don't wait for a key forward request.  Forward the key immediately.
    Defaults to `false`.
  - `extra_fields:` extra fields to include in the forwarded room key event.
    For example, this can be used to add a `withheld` code
  """
  @spec forward_room_key_to(
          Util.Device.t(),
          Util.Encryption.MegolmSession.t(),
          Util.Device.t(),
          Keyword.t()
        ) ::
          Util.Device.t()
  def forward_room_key_to(
        %Util.Device{} = from,
        %Util.Encryption.MegolmSession{inbound: inbound} = session,
        %Util.Device{} = to,
        opts \\ []
      ) do
    if not Keyword.get(opts, :immediate, false) do
      room_id = session.room_id
      session_id = session.id
      from_user = from.user.id
      from_device = from.id
      from_pair = {from_user, from_device}
      to_user = to.user.id
      to_device = to.id
      to_pair = {to_user, to_device}
      server = from.user.server

      requests =
        Polyjuice.ClientTest.BaseTestServer.wait_until_state(
          server,
          fn
            %{
              polyjuice: %{
                encryption: %{
                  key_forward_requests: %{^from_pair => %{^to_pair => %{} = requests}}
                }
              }
            } ->
              requests =
                Enum.flat_map(
                  requests,
                  fn
                    {req_id,
                     %{
                       "algorithm" => "m.megolm.v1.aes-sha2",
                       "room_id" => ^room_id,
                       "session_id" => ^session_id
                     }} ->
                      [req_id]

                    _ ->
                      []
                  end
                )

              if requests != [], do: requests, else: false

            _state ->
              false
          end,
          Keyword.get(opts, :timeout, 60_000)
        )

      # remove the request from the queue
      Polyjuice.ClientTest.BaseTestServer.update_test_state(
        server,
        &update_in(
          &1,
          [:polyjuice, :encryption, :key_forward_requests, from_pair, to_pair],
          fn req ->
            Map.drop(req, requests)
          end
        )
      )
    end

    extra_fields = Keyword.get(opts, :extra_fields, [])

    extra_fields = [
      {:forwarding_curve25519_key_chain, Keyword.get(opts, :forwarding_curve25519_key_chain, [])},
      {:sender_claimed_ed25519_key, Keyword.fetch!(opts, :sender_claimed_ed25519_key)},
      {:sender_key, Keyword.fetch!(opts, :sender_key)}
      | extra_fields
    ]

    index =
      Keyword.get_lazy(opts, :index, fn ->
        Polyjuice.Newt.InboundGroupSession.first_known_index(inbound)
      end)

    key = Polyjuice.Newt.InboundGroupSession.export_at(inbound, index)

    {:ok, encrypted_room_key} =
      Util.Encryption.encrypt_room_key_with_olm(
        from.user.server,
        session.room_id,
        {:megolm, session.id, key},
        from: {from.user.id, from.id},
        to: {to.user.id, to.id},
        olm_account: from.olm_account,
        extra_fields: extra_fields,
        type: "m.forwarded_room_key"
      )

    Polyjuice.Server.Protocols.SendToDevice.send_to_device(
      from.user.server,
      from.user.id,
      from.id,
      "polyjuice_#{to_string(from.txnid)}",
      "m.room.encrypted",
      %{to.user.id => %{to.id => encrypted_room_key}}
    )

    %{from | txnid: from.txnid + 1}
  end

  @doc """
  Encrypt an event using Megolm.

  `event` should be a map with `"room_id"`, `"type"`, and `"content"`
  properties.  `device_id` is the ID of the device sending the event.

  Returns a tuple of the form `{key, content}`, where `key` is the megolm
  session key that can be shared with other users and/or stored in key backup,
  and `content` is the `contents` of an event that can be sent to a room with
  type `m.room.encrypted`.
  """
  @spec encrypt_megolm(
          map(),
          Polyjuice.Newt.GroupSession.t(),
          String.t(),
          Polyjuice.Newt.Account.t()
        ) :: {String.t(), Polyjuice.Util.event_content()}
  def encrypt_megolm(event, group_session, device_id, olm_account) do
    key = Polyjuice.Newt.GroupSession.get_key(group_session)
    ciphertext = Polyjuice.Newt.GroupSession.encrypt(group_session, Jason.encode!(event))

    msg = %{
      "algorithm" => megolm_v1_algorithm(),
      "ciphertext" => ciphertext,
      "device_id" => device_id,
      "sender_key" => Polyjuice.Newt.Account.curve25519_key(olm_account),
      "session_id" => Polyjuice.Newt.GroupSession.get_id(group_session)
    }

    {key, msg}
  end

  @doc """
  Set the device's Olm account, so that it can be used by `olm_event_handler/3`.

  Using this function requires that the test state is a `Map` that has a
  `:polyjuice` key that it can control.
  """
  @spec set_account(
          Polyjuice.ClientTest.BaseTestServer.t(),
          String.t(),
          String.t(),
          Polyjuice.Newt.Account.t()
        ) :: :ok
  def set_account(server, user_id, device_id, olm_account) do
    Polyjuice.ClientTest.BaseTestServer.update_test_state(server, fn state ->
      polyjuice_state = Map.get(state, :polyjuice, %{})
      encryption_state = Map.get(polyjuice_state, :encryption, %{})

      olm_accounts =
        Map.get(encryption_state, :olm_accounts, %{})
        |> Map.put({user_id, device_id}, {olm_account, %{}, %{}})

      Map.put(encryption_state, :olm_accounts, olm_accounts)
      |> (&Map.put(polyjuice_state, :encryption, &1)).()
      |> (&Map.put(state, :polyjuice, &1)).()
    end)
  end

  @doc """
  Wait until we have received a Megolm session.

  `user_id` and `device_id` identify the device waiting for the Megolm session.
  """
  @spec wait_for_megolm_session(
          Polyjuice.ClientTest.BaseTestServer.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t(),
          non_neg_integer | :infinity
        ) :: Polyjuice.Newt.InboundGroupSession.t()
  def wait_for_megolm_session(server, user_id, device_id, room_id, session_id, timeout \\ 120_000) do
    Polyjuice.ClientTest.BaseTestServer.wait_until_state(
      server,
      fn state ->
        with {:ok, polyjuice_state} <- Map.fetch(state, :polyjuice),
             {:ok, encryption_state} <- Map.fetch(polyjuice_state, :encryption),
             {:ok, olm_accounts} <- Map.fetch(encryption_state, :olm_accounts),
             {:ok, {_, _, inbound_megolm}} <- Map.fetch(olm_accounts, {user_id, device_id}),
             {:ok, room_sessions} <- Map.fetch(inbound_megolm, room_id),
             {:ok, {session, _}} <- Map.fetch(room_sessions, session_id) do
          session
        else
          _ -> nil
        end
      end,
      timeout
    )
  end

  @spec get_megolm_room_key_event(
          Polyjuice.ClientTest.BaseTestServer.t(),
          String.t(),
          String.t(),
          String.t(),
          String.t()
        ) :: Polyjuice.Util.event()
  def get_megolm_room_key_event(server, user_id, device_id, room_id, session_id) do
    Polyjuice.ClientTest.BaseTestServer.get_test_state(
      server,
      fn state ->
        with {:ok, polyjuice_state} <- Map.fetch(state, :polyjuice),
             {:ok, encryption_state} <- Map.fetch(polyjuice_state, :encryption),
             {:ok, olm_accounts} <- Map.fetch(encryption_state, :olm_accounts),
             {:ok, {_, _, inbound_megolm}} <- Map.fetch(olm_accounts, {user_id, device_id}),
             {:ok, room_sessions} <- Map.fetch(inbound_megolm, room_id),
             {:ok, {_, event}} <- Map.fetch(room_sessions, session_id) do
          event
        else
          _ -> nil
        end
      end
    )
  end

  @doc """
  Encrypt an event using olm.

  Usually, a utility function such as `encrypt_room_key_with_olm/4` will be used
  rather than using this function directly.

  Note: Before using this function, `set_account/4` must have been called,
  setting the Olm account for the sending user.

  See `encrypt_room_key_with_olm/4` for documentation on `opts`.
  """
  @spec encrypt_olm(
          Polyjuice.ClientTest.BaseTestServer.t(),
          String.t(),
          Polyjuice.Util.event_content(),
          Keyword.t()
        ) :: {:ok, Polyjuice.Util.event_content()} | {:error, atom}
  def encrypt_olm(server, type, content, opts) do
    {from_user_id, from_device_id} = Keyword.fetch!(opts, :from)
    {to_user_id, to_device_id} = Keyword.fetch!(opts, :to)
    olm_account = Keyword.fetch!(opts, :olm_account)

    curve25519_key_id = "curve25519:" <> to_device_id
    ed25519_key_id = "ed25519:" <> to_device_id

    with %{
           "device_keys" => %{
             ^to_user_id => %{
               ^to_device_id => %{
                 "keys" => %{
                   ^ed25519_key_id => to_ed25519_key,
                   ^curve25519_key_id => to_curve25519_key
                 }
               }
             }
           }
         } <-
           Polyjuice.Server.Protocols.DeviceKey.query_keys(
             server,
             from_user_id,
             %{to_user_id => [to_device_id]},
             nil
           ) do
      event = %{
        "type" => type,
        "content" => content,
        "sender" => from_user_id,
        "recipient" => to_user_id,
        "recipient_keys" => %{
          "ed25519" => to_ed25519_key
        },
        "keys" => %{
          "ed25519" => Polyjuice.Newt.Account.ed25519_key(olm_account)
        }
      }

      olm_session =
        Polyjuice.ClientTest.BaseTestServer.get_test_state(server, fn state ->
          {_, olm_sessions_map, _} =
            state.polyjuice.encryption.olm_accounts[{from_user_id, from_device_id}]

          case Map.fetch(olm_sessions_map, to_curve25519_key) do
            {:ok, [olm_session | _]} ->
              olm_session

            _ ->
              nil
          end
        end)

      olm_session =
        if olm_session == nil do
          with %{^to_user_id => %{^to_device_id => %{} = otks}} <-
                 Polyjuice.Server.Protocols.DeviceKey.claim_key(server, %{
                   to_user_id => %{to_device_id => "signed_curve25519"}
                 }),
               [{"signed_curve25519:" <> _key_id, %{"key" => otk}}] <- Map.to_list(otks),
               {:ok, olm_session} <-
                 Polyjuice.Newt.OlmSession.create_outbound(
                   olm_account,
                   to_curve25519_key,
                   otk
                 ) do
            # FIXME: check otk signature
            Polyjuice.ClientTest.BaseTestServer.update_test_state(server, fn state ->
              update_in(
                state.polyjuice.encryption.olm_accounts[{from_user_id, from_device_id}],
                fn {olm_account, olm_sessions_map, inbound_megolm} ->
                  olm_sessions = Map.get(olm_sessions_map, to_curve25519_key, [])

                  olm_sessions_map =
                    Map.put(olm_sessions_map, to_curve25519_key, [olm_session | olm_sessions])

                  {olm_account, olm_sessions_map, inbound_megolm}
                end
              )
            end)

            olm_session
          else
            _ -> nil
          end
        else
          olm_session
        end

      if olm_session != nil do
        {message_type, ciphertext} =
          Jason.encode!(event)
          |> (&Polyjuice.Newt.OlmSession.encrypt(olm_session, &1)).()

        {:ok,
         %{
           "algorithm" => olm_v1_algorithm(),
           "sender_key" => Polyjuice.Newt.Account.curve25519_key(olm_account),
           "ciphertext" => %{
             to_curve25519_key => %{
               "type" => message_type,
               "body" => ciphertext
             }
           }
         }}
      else
        {:error, :no_olm_session}
      end
    else
      _ -> {:error, :no_device_keys}
    end
  end

  @doc """
  Encrypt a room key with Olm, for sending to another device

  Currently, the `key` must be of the form `{:megolm, session_id, session_key}`.

  The following options are supported:

  - `from:` A tuple of the form `{user_id, device_id}` indicating the sender
  - `to:` A tuple of the form `{user_id, device_id}` indicating the recipient
  - `olm_account:` the sender's Olm account (as created by
    `Polyjuice.Newt.Account`)
  - `extra_fields:` extra fields to set in the encrypted `m.room_key` event
    content

  Note: Before using this function, `set_account/4` must have been called,
  setting the Olm account for the sending user.

  """
  @spec encrypt_room_key_with_olm(
          Polyjuice.ClientTest.BaseTestServer.t(),
          String.t(),
          key :: {:megolm, String.t(), String.t()},
          Keyword.t()
        ) :: {:ok, Polyjuice.Util.event_content()} | {:error, atom}
  def encrypt_room_key_with_olm(server, room_id, {:megolm, session_id, session_key}, opts \\ []) do
    {extra_fields, opts} = Keyword.pop(opts, :extra_fields, [])

    content =
      Enum.reduce(
        extra_fields,
        %{
          "algorithm" => megolm_v1_algorithm(),
          "room_id" => room_id,
          "session_id" => session_id,
          "session_key" => session_key
        },
        fn {key, value}, acc -> Map.put(acc, to_string(key), value) end
      )

    encrypt_olm(server, Keyword.get(opts, :type, "m.room_key"), content, opts)
  end

  @doc """
  Event handler that decrypts Olm-encrypted to-events and stores inbound Megolm sessions.

  This function can be used as an `Polyjuice.ClientTest.BaseTestServer` event
  handler.  Any devices whose to-events events should be decrypted must first
  have their olm account set by calling `set_account/4`.

  Using this function requires that the test state is a `Map` that has a
  `:polyjuice` key that it can control.
  """
  def olm_event_handler(event, state, _server) do
    require Logger

    case event do
      {:send_to_device, user_id, device_id,
       %{
         "sender" => sender,
         "type" => "m.room.encrypted",
         "content" => %{
           "algorithm" => @olm_v1_algorithm,
           "sender_key" => sender_key,
           "ciphertext" => %{} = ciphertext
         }
       }}
      when is_binary(sender_key) ->
        polyjuice_state = Map.get(state, :polyjuice, %{})
        encryption_state = Map.get(polyjuice_state, :encryption, %{})
        olm_accounts = Map.get(encryption_state, :olm_accounts, %{})

        case Map.fetch(olm_accounts, {user_id, device_id}) do
          :error ->
            state

          {:ok, {olm_account, olm_sessions_map, inbound_megolm}} ->
            olm_sessions = Map.get(olm_sessions_map, sender_key, [])
            curve_25519_key = Polyjuice.Newt.Account.curve25519_key(olm_account)

            case Map.get(ciphertext, curve_25519_key) do
              %{"type" => type, "body" => body}
              when (type == 0 or type == 1) and is_binary(body) ->
                res =
                  case Enum.find_value(olm_sessions, fn session ->
                         case Polyjuice.Newt.OlmSession.decrypt(session, type, body) do
                           {:ok, _} = ret -> ret
                           {:error, _} -> nil
                         end
                       end) do
                    {:ok, text} ->
                      {:ok, text, olm_sessions}

                    nil ->
                      if type == 0 do
                        case Polyjuice.Newt.OlmSession.create_inbound(
                               olm_account,
                               sender_key,
                               body
                             ) do
                          {:ok, {session, text}} -> {:ok, text, [session | olm_sessions]}
                          _ -> :error
                        end
                      end
                  end

                # FIXME: olm_sessions should be sorted by
                # most-recently-succesfully-used
                case res do
                  {:ok, text, olm_sessions} ->
                    ed25519_key = Polyjuice.Newt.Account.ed25519_key(olm_account)

                    inbound_megolm =
                      with {:ok,
                            %{
                              "sender" => ^sender,
                              "keys" => %{"ed25519" => _sender_ed25519_key},
                              "recipient" => ^user_id,
                              "recipient_keys" => %{"ed25519" => ^ed25519_key},
                              "type" => type,
                              "content" => %{
                                "algorithm" => @megolm_v1_algorithm(),
                                "room_id" => room_id,
                                "session_id" => session_id,
                                "session_key" => session_key
                              }
                            } = key_event}
                           when type == "m.room_key" or type == "m.forwarded_room_key" <-
                             Jason.decode(text),
                           {:ok, inbound_session} <-
                             (case type do
                                "m.room_key" ->
                                  Polyjuice.Newt.InboundGroupSession.create(session_key)

                                "m.forwarded_room_key" ->
                                  Polyjuice.Newt.InboundGroupSession.import_key(session_key)
                              end) do
                        # FIXME: check that the sender's ed25519 key signed
                        # their curve25519 key
                        room_sessions = Map.get(inbound_megolm, room_id, %{})

                        room_sessions =
                          case Map.fetch(room_sessions, session_id) do
                            :error ->
                              Map.put(room_sessions, session_id, {inbound_session, key_event})

                            {:ok, {old_session, _}} ->
                              # if we already have the key, determine if we
                              # should keep the existing one or the new one
                              # based on the first known index
                              if Polyjuice.Newt.InboundGroupSession.first_known_index(old_session) <=
                                   Polyjuice.Newt.InboundGroupSession.first_known_index(
                                     inbound_session
                                   ) do
                                room_sessions
                              else
                                Map.put(room_sessions, session_id, {inbound_session, key_event})
                              end
                          end

                        Map.put(inbound_megolm, room_id, room_sessions)
                      else
                        _ ->
                          Logger.warn("unable to process room key")
                          inbound_megolm
                      end

                    olm_sessions_map = Map.put(olm_sessions_map, sender_key, olm_sessions)

                    olm_accounts =
                      Map.put(
                        olm_accounts,
                        {user_id, device_id},
                        {olm_account, olm_sessions_map, inbound_megolm}
                      )

                    Map.put(encryption_state, :olm_accounts, olm_accounts)
                    |> (&Map.put(polyjuice_state, :encryption, &1)).()
                    |> (&Map.put(state, :polyjuice, &1)).()

                  _ ->
                    Logger.warn("Could not decrypt olm message")
                    state
                end

              _ ->
                state
            end
        end

      _ ->
        state
    end
  end

  @doc """
  Event handler that keeps track of key forward requests

  This function can be used as an `Polyjuice.ClientTest.BaseTestServer` event
  handler.

  Using this function requires that the test state is a `Map` that has a
  `:polyjuice` key that it can control.
  """
  def key_forward_request_event_handler(event, state, _server) do
    polyjuice_state = Map.get(state, :polyjuice, %{})
    encryption_state = Map.get(polyjuice_state, :encryption, %{})

    case event do
      {:send_to_device, to_user, to_device,
       %{
         "type" => "m.room_key_request",
         "sender" => from_user,
         "content" => %{
           "action" => "request",
           "requesting_device_id" => from_device,
           "request_id" => req_id,
           "body" => %{} = body
         }
       }}
      when is_binary(from_user) and is_binary(from_device) and is_binary(req_id) ->
        request_state = Map.get(encryption_state, :key_forward_requests, %{})
        requests_to_device = Map.get(request_state, {to_user, to_device}, %{})
        requests = Map.get(requests_to_device, {from_user, from_device}, %{})

        requests = Map.put(requests, req_id, body)
        requests_to_device = Map.put(requests_to_device, {from_user, from_device}, requests)
        request_state = Map.put(request_state, {to_user, to_device}, requests_to_device)
        encryption_state = Map.put(encryption_state, :key_forward_requests, request_state)
        polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
        Map.put(state, :polyjuice, polyjuice_state)

      {:send_to_device, to_user, to_device,
       %{
         "type" => "m.room_key_request",
         "sender" => from_user,
         "content" => %{
           "action" => "request_cancellation",
           "requesting_device_id" => from_device,
           "request_id" => req_id
         }
       }}
      when is_binary(from_user) and is_binary(from_device) and is_binary(req_id) ->
        request_state = Map.get(encryption_state, :key_forward_requests, %{})
        requests_to_device = Map.get(request_state, {to_user, to_device}, %{})
        requests = Map.get(requests_to_device, {from_user, from_device}, %{})

        requests = Map.delete(requests, req_id)
        # FIXME: delete keys if the value is empty
        requests_to_device = Map.put(requests_to_device, {from_user, from_device}, requests)
        request_state = Map.put(request_state, {to_user, to_device}, requests_to_device)
        encryption_state = Map.put(encryption_state, :key_forward_requests, request_state)
        polyjuice_state = Map.put(polyjuice_state, :encryption, encryption_state)
        Map.put(state, :polyjuice, polyjuice_state)

      _ ->
        state
    end
  end
end
