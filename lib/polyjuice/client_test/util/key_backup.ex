# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.KeyBackup do
  @moduledoc """
  Utility functions for key backup.
  """

  defstruct [
    :version,
    :algorithm,
    :key,
    :public_key,
    :auth_data,
    :internal
  ]

  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  @doc """
  Create a key backup
  """
  def create(user, algorithm, opts \\ [])

  def create(user, algorithm = "m.megolm_backup.v1.curve25519-aes-sha2", opts) do
    {pub_key, priv_key} = :crypto.generate_key(:eddh, :x25519)

    auth_data = %{
      "public_key" => Base.encode64(pub_key, padding: false),
      "signatures" => %{}
    }

    {:ok, auth_data} =
      if user.cross_signing != nil and not Keyword.get(opts, :no_cross_signing, false) do
        Polyjuice.Util.JSON.sign(
          auth_data,
          user.id,
          Util.CrossSigning.make_signing_key(user.cross_signing, :msk)
        )
      else
        {:ok, auth_data}
      end

    version =
      Protocols.KeyBackup.create_version(
        user.server,
        user.id,
        algorithm,
        auth_data
      )

    %{
      user
      | key_backup: %__MODULE__{
          version: version,
          algorithm: algorithm,
          key: priv_key,
          public_key: pub_key,
          auth_data: auth_data
        }
    }
  end

  def create(user, algorithm = "org.matrix.msc3270.v1.aes-hmac-sha2", opts) do
    backup_key = :crypto.strong_rand_bytes(32)
    {_, iv, mac} = Util.SSSS.encrypt_aes(<<0::integer-size(32)-unit(8)>>, "", backup_key)

    auth_data = %{
      "iv" => Base.encode64(iv, padding: false),
      "mac" => Base.encode64(mac, padding: false),
      "signatures" => %{}
    }

    {:ok, auth_data} =
      if user.cross_signing != nil and not Keyword.get(opts, :no_cross_signing, false) do
        Polyjuice.Util.JSON.sign(
          auth_data,
          user.id,
          Util.CrossSigning.make_signing_key(user.cross_signing, :msk)
        )
      else
        {:ok, auth_data}
      end

    version =
      Protocols.KeyBackup.create_version(
        user.server,
        user.id,
        algorithm,
        auth_data
      )

    %{
      user
      | key_backup: %__MODULE__{
          version: version,
          algorithm: algorithm,
          key: backup_key,
          auth_data: auth_data
        }
    }
  end

  def store_in_ssss(user, ssss_keys \\ nil)

  def store_in_ssss(
        %Util.User{
          key_backup: %__MODULE__{algorithm: "m.megolm_backup.v1.curve25519-aes-sha2"} = backup
        } = user,
        ssss_keys
      ) do
    backup_key_b64 = Base.encode64(backup.key, padding: false)
    Util.SSSS.store(user, "m.megolm_backup.v1", backup_key_b64, ssss_keys)

    user
  end

  def store_in_ssss(
        %Util.User{
          key_backup: %__MODULE__{algorithm: "org.matrix.msc3270.v1.aes-hmac-sha2"} = backup
        } = user,
        ssss_keys
      ) do
    backup_key_b64 = Base.encode64(backup.key, padding: false)
    Util.SSSS.store(user, "m.megolm_backup.v1", backup_key_b64, ssss_keys)

    user
  end
end
