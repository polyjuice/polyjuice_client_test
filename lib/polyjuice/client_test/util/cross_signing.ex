# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Util.CrossSigning do
  @moduledoc """
  Utility functions for cross-signing.
  """

  @type cross_signing_key :: %{
          pubkey: binary,
          privkey: binary,
          b64: String.t()
        }

  @type t() :: %{
          msk: cross_signing_key,
          usk: cross_signing_key,
          ssk: cross_signing_key
        }

  defstruct [
    :msk,
    :usk,
    :ssk
  ]

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.ClientTest.Util
  alias Polyjuice.Server.Protocols

  @doc """
  Generate and set the cross-signing keys for a user.
  """
  @spec generate_keys(Util.User.t()) :: Util.User.t()
  def generate_keys(%Util.User{} = user) do
    keys = generate_keys()

    Protocols.DeviceKey.set_cross_signing_keys(
      user.server,
      user.id,
      make_keys_json(keys, user.id)
    )

    %{user | cross_signing: keys}
  end

  @doc """
  Store the cross-signing private keys in SSSS.

  `keys` is a list of SSSS keys to use to encrypt the private keys.  If the key
  `:default` is specified, then the default SSSS key will be used.
  """
  @spec store_in_ssss(Util.User.t(), list(String.t() | :default) | nil) :: Util.User.t()
  def store_in_ssss(%Util.User{} = user, keys \\ nil) do
    msk_b64 = Base.encode64(user.cross_signing.msk.privkey, padding: false)
    Util.SSSS.store(user, "m.cross_signing.master", msk_b64, keys)

    usk_b64 = Base.encode64(user.cross_signing.usk.privkey, padding: false)
    Util.SSSS.store(user, "m.cross_signing.user_signing", usk_b64, keys)

    ssk_b64 = Base.encode64(user.cross_signing.ssk.privkey, padding: false)
    Util.SSSS.store(user, "m.cross_signing.self_signing", ssk_b64, keys)

    user
  end

  @doc """
  Cross-sign another user.
  """
  @spec cross_sign(Util.User.t(), Util.User.t()) :: Util.User.t()
  def cross_sign(%Util.User{} = user, %Util.User{} = other) do
    usk_signing_key = make_signing_key(user.cross_signing, :usk)

    other_pubkey = other.cross_signing.msk.b64

    {:ok, signature} =
      %{
        "keys" => %{
          "ed25519:#{other_pubkey}" => other_pubkey
        },
        "usage" => ["master"],
        "user_id" => other.id
      }
      |> Polyjuice.Util.JSON.sign(user.id, usk_signing_key)

    Polyjuice.Server.Protocols.DeviceKey.add_signatures(
      user.server,
      user.id,
      %{
        other.id => %{
          other_pubkey => signature
        }
      }
    )

    user
  end

  @doc """
  Wait until a device is cross-signed.
  """
  @spec wait_until_cross_signed(Util.Device.t()) :: Util.Device.t()
  def wait_until_cross_signed(%Util.Device{user: user} = device) do
    ssk_key_id = "ed25519:" <> user.cross_signing.ssk.b64
    user_id = user.id
    device_id = device.id

    # wait until the client uploads a device key signed with the self-signing
    # key, which can either be done by uploading the device key with the
    # signature included, or uploading the signature separately
    BaseTestServer.wait_until_event(
      user.server,
      fn
        {:set_device_keys, ^user_id, %{"signatures" => %{^user_id => %{^ssk_key_id => _}}}} ->
          true

        {:add_signatures, ^user_id,
         %{^user_id => %{^device_id => %{"signatures" => %{^user_id => %{^ssk_key_id => _}}}}}} ->
          true

        _ ->
          false
      end
    )

    # check that the signature is valid
    Polyjuice.Server.Protocols.DeviceKey.query_keys(
      user.server,
      user.id,
      %{user.id => [device.id]},
      nil
    )
    |> Map.get("device_keys", %{})
    |> Map.get(user.id, %{})
    |> Map.get(device.id, %{})
    |> Polyjuice.Util.JSON.signed?(
      user.id,
      Util.CrossSigning.make_verify_key(user.cross_signing, :ssk)
    )
    |> Polyjuice.ClientTest.Assertions.assert(
      module: Polyjuice.ClientTest.Fluent,
      identifier: "incorrect-ssk-signature"
    )

    device
  end

  @doc """
  Generate cross-signing keys.
  """
  @spec generate_keys() :: t()
  def generate_keys() do
    {msk_pubkey, msk_privkey} =
      :crypto.generate_key(:eddsa, :ed25519, :crypto.strong_rand_bytes(32))

    msk_b64 = Base.encode64(msk_pubkey, padding: false)

    {usk_pubkey, usk_privkey} =
      :crypto.generate_key(:eddsa, :ed25519, :crypto.strong_rand_bytes(32))

    usk_b64 = Base.encode64(usk_pubkey, padding: false)

    {ssk_pubkey, ssk_privkey} =
      :crypto.generate_key(:eddsa, :ed25519, :crypto.strong_rand_bytes(32))

    ssk_b64 = Base.encode64(ssk_pubkey, padding: false)

    %__MODULE__{
      msk: %{
        pubkey: msk_pubkey,
        privkey: msk_privkey,
        b64: msk_b64
      },
      usk: %{
        pubkey: usk_pubkey,
        privkey: usk_privkey,
        b64: usk_b64
      },
      ssk: %{
        pubkey: ssk_pubkey,
        privkey: ssk_privkey,
        b64: ssk_b64
      }
    }
  end

  @doc """
  Make the JSON structure needed to upload cross-signing keys.
  """
  @spec make_keys_json(keys :: t(), String.t()) :: map()
  def make_keys_json(%__MODULE__{msk: msk, usk: usk, ssk: ssk} = _keys, user_id) do
    msk_signing_key = %Polyjuice.Util.Ed25519.SigningKey{
      key: msk.privkey,
      id: msk.b64
    }

    msk_json = %{
      "keys" => %{
        ("ed25519:" <> msk.b64) => msk.b64
      },
      "usage" => ["master"],
      "user_id" => user_id
    }

    {:ok, usk_json} =
      %{
        "keys" => %{
          ("ed25519:" <> usk.b64) => usk.b64
        },
        "usage" => ["user_signing"],
        "user_id" => user_id
      }
      |> Polyjuice.Util.JSON.sign(user_id, msk_signing_key)

    {:ok, ssk_json} =
      %{
        "keys" => %{
          ("ed25519:" <> ssk.b64) => ssk.b64
        },
        "usage" => ["self_signing"],
        "user_id" => user_id
      }
      |> Polyjuice.Util.JSON.sign(user_id, msk_signing_key)

    %{
      "master_key" => msk_json,
      "user_signing_key" => usk_json,
      "self_signing_key" => ssk_json
    }
  end

  @doc """
  Make a struct that can be used for verifying a signature.

  The struct can be used by the functions in `Polyjuice.Util`, such as
  `Polyjuice.Util.JSON.signed?/3`
  """
  @spec make_verify_key(t(), :msk | :usk | :ssk) :: Polyjuice.Util.VerifyKey.t()
  def make_verify_key(%__MODULE__{} = keys, type)
      when type == :msk or type == :usk or type == :ssk do
    key = Map.fetch!(keys, type)

    %Polyjuice.Util.Ed25519.VerifyKey{
      key: key.pubkey,
      id: key.b64
    }
  end

  @doc """
  Make a struct that can be used for signing.

  The struct can be used by the functions in `Polyjuice.Util`, such as
  `Polyjuice.Util.JSON.sign/3`
  """
  @spec make_signing_key(t(), :msk | :usk | :ssk) :: Polyjuice.Util.SsigningKey.t()
  def make_signing_key(%__MODULE__{} = keys, type)
      when type == :msk or type == :usk or type == :ssk do
    key = Map.fetch!(keys, type)

    %Polyjuice.Util.Ed25519.SigningKey{
      key: key.privkey,
      id: key.b64
    }
  end

  @doc """
  Upload the cross-signing private keys to SSSS.

  The private keys will be encrypted using the given SSSS keys.
  """
  @spec upload_to_ssss(
          Polyjuice.Server.Protocols.AccountData.t(),
          String.t(),
          t(),
          list({String.t(), binary})
        ) :: :ok
  def upload_to_ssss(server, user_id, %__MODULE__{} = xsigning_keys, keys)
      when is_binary(user_id) and is_list(keys) do
    alias Polyjuice.ClientTest.Util.SSSS

    msk_b64 = Base.encode64(xsigning_keys.msk.privkey, padding: false)
    SSSS.set(server, user_id, msk_b64, "m.cross_signing.master", keys)

    usk_b64 = Base.encode64(xsigning_keys.usk.privkey, padding: false)
    SSSS.set(server, user_id, usk_b64, "m.cross_signing.user_signing", keys)

    ssk_b64 = Base.encode64(xsigning_keys.ssk.privkey, padding: false)
    SSSS.set(server, user_id, ssk_b64, "m.cross_signing.self_signing", keys)
    :ok
  end
end
