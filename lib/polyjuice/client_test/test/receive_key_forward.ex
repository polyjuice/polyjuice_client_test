# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.ReceiveKeyForward do
  @moduledoc """
  Request and receive key forward

  Tests that a client can request and receive a forwarded key.

  Alice sends a message in a room.  Bob logs in and requests the key from
  another device.  The other device forwards the key to the new device, which
  should now be able to read Alice's message.

  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &Util.Encryption.key_forward_request_event_handler/3
    ]

  def test_script(server) do
    alice = Util.User.create(server, :aliceandbob, nil)

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()

    forwarder_device =
      Util.Device.log_in(bob)
      |> Util.Encryption.initialize_olm()

    # prepare the room
    room_name = "Key forward test"

    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: room_name,
        invite: [bob.id],
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    session = Util.Encryption.MegolmSession.create(alice_device, room_id)

    Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
      "body" => "Hello Bob",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    # start the test
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
        key: Util.SSSS.make_key_repr(bob, :default)
      )

    Util.CrossSigning.wait_until_cross_signed(bob_device)

    BaseTestServer.send_done(server, prompt_event_id)

    forwarder_device =
      Util.Encryption.forward_room_key_to(
        forwarder_device,
        session,
        bob_device,
        forwarding_curve25519_key_chain: [],
        sender_claimed_ed25519_key: Polyjuice.Newt.Account.ed25519_key(alice_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(alice_device.olm_account)
      )

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: room_name],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})
  end
end
