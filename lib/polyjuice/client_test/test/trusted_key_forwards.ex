# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.TrustedKeyForwards do
  @moduledoc """
  Trusted key forwards.

  Tests that a client maintains trust information over key forwards.

  #e2ee #key-forward #msc3879
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &Util.Encryption.olm_event_handler/3,
      &Util.Encryption.key_forward_request_event_handler/3
    ],
    fluent: true

  def test_script(server) do
    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()

    sending_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    trusted_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    untrusted_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm(no_cross_signing: true)

    requesting_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()
      |> Util.Encryption.upload_olm_otks(1)

    # prepare the room and send some messages
    room_name = "Trusted key forwards test"

    {:ok, room_id} =
      Util.Room.create(server, user.id,
        name: room_name,
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    tt_session = Util.Encryption.MegolmSession.create(sending_device, room_id)

    Util.Encryption.MegolmSession.encrypt(tt_session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "This message should be the only trusted message"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    tu_session = Util.Encryption.MegolmSession.create(sending_device, room_id)

    Util.Encryption.MegolmSession.encrypt(tu_session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "This message should not be trusted"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    ut_session = Util.Encryption.MegolmSession.create(sending_device, room_id)

    Util.Encryption.MegolmSession.encrypt(ut_session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Nor should this one"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    uu_session = Util.Encryption.MegolmSession.create(sending_device, room_id)

    Util.Encryption.MegolmSession.encrypt(uu_session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Nor this"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    # start the test
    test_device = Util.Device.wait_for_login(user)

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
        key: Util.SSSS.make_key_repr(user, :default)
      )

    Util.CrossSigning.wait_until_cross_signed(test_device)

    BaseTestServer.send_done(server, prompt_event_id)

    trusted_device =
      trusted_device
      |> Util.Encryption.forward_room_key_to(
        tt_session,
        test_device,
        forwarding_curve25519_key_chain: [
          Polyjuice.Newt.Account.curve25519_key(trusted_device.olm_account)
        ],
        sender_claimed_ed25519_key:
          Polyjuice.Newt.Account.ed25519_key(sending_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(sending_device.olm_account),
        extra_fields: [{"org.matrix.msc3879.trusted", true}]
      )
      |> Util.Encryption.forward_room_key_to(
        tu_session,
        test_device,
        forwarding_curve25519_key_chain: [
          Polyjuice.Newt.Account.curve25519_key(trusted_device.olm_account)
        ],
        sender_claimed_ed25519_key:
          Polyjuice.Newt.Account.ed25519_key(sending_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(sending_device.olm_account)
      )

    untrusted_device =
      untrusted_device
      |> Util.Encryption.forward_room_key_to(
        ut_session,
        test_device,
        forwarding_curve25519_key_chain: [
          Polyjuice.Newt.Account.curve25519_key(untrusted_device.olm_account)
        ],
        sender_claimed_ed25519_key:
          Polyjuice.Newt.Account.ed25519_key(sending_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(sending_device.olm_account),
        extra_fields: [{"org.matrix.msc3879.trusted", true}]
      )
      |> Util.Encryption.forward_room_key_to(
        uu_session,
        test_device,
        forwarding_curve25519_key_chain: [
          Polyjuice.Newt.Account.curve25519_key(untrusted_device.olm_account)
        ],
        sender_claimed_ed25519_key:
          Polyjuice.Newt.Account.ed25519_key(sending_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(sending_device.olm_account)
      )

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Test.TrustedKeyForwards.Fluent,
      "decryptable-messages",
      [],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})

    # request the keys and make sure that they are shared with the right trust flag
    {:ok, request_event_id} =
      BaseTestServer.send_progress(
        server,
        Polyjuice.ClientTest.Test.TrustedKeyForwards.Fluent,
        "requesting-keys"
      )

    Enum.each(
      [
        {"tt", tt_session.id},
        {"tu", tu_session.id},
        {"ut", ut_session.id},
        {"uu", uu_session.id}
      ],
      fn {name, session_id} ->
        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          user.id,
          requesting_device.id,
          name,
          "m.room_key_request",
          %{
            user.id => %{
              test_device.id => %{
                "action" => "request",
                "body" => %{
                  "algorithm" => Util.Encryption.megolm_v1_algorithm(),
                  "room_id" => room_id,
                  "sender_key" =>
                    Polyjuice.Newt.Account.curve25519_key(sending_device.olm_account),
                  "session_id" => session_id
                },
                "request_id" => name,
                "requesting_device_id" => requesting_device.id
              }
            }
          }
        )

        Util.Encryption.MegolmSession.wait_for_session(requesting_device, room_id, session_id)
      end
    )

    BaseTestServer.send_done(server, request_event_id)

    assert(
      Enum.reduce(
        [
          {tt_session.id, true},
          {tu_session.id, false},
          {ut_session.id, false},
          {uu_session.id, false}
        ],
        true,
        fn {session_id, trusted}, acc ->
          event =
            Util.Encryption.get_megolm_room_key_event(
              server,
              user.id,
              requesting_device.id,
              room_id,
              session_id
            )

          if Map.get(event["content"], "org.matrix.msc3879.trusted", false) != trusted do
            BaseTestServer.send_error(
              server,
              Polyjuice.ClientTest.Test.TrustedKeyForwards.Fluent,
              if(trusted, do: "trusted-marked-as-untrusted", else: "untrusted-marked-as-trusted"),
              session_id: session_id
            )

            false
          else
            acc
          end
        end
      )
    )
  end
end
