# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.SymmetricBackup do
  @moduledoc """
  Symmetric backup.

  Tests that a client can read and write to a symmetric key backup.

  #e2ee #keybackup #msc3270 #megolm
  """

  use Polyjuice.ClientTest.TestHelper, fluent: true

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  def test_script(server) do
    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()
      |> Util.KeyBackup.create("org.matrix.msc3270.v1.aes-hmac-sha2")
      |> Util.KeyBackup.store_in_ssss()

    device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    # create room and send two encrypted messages -- first one with a trusted key
    # second with an untrusted key
    {:ok, room_id} =
      Util.Room.create(server, user.id,
        name: "Symmetric backup test",
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    session1 =
      Util.Encryption.MegolmSession.create(device, room_id)
      |> Util.Encryption.MegolmSession.store_in_backup()

    Util.Encryption.MegolmSession.encrypt(session1, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Hello world!"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    session2 =
      Util.Encryption.MegolmSession.create(device, room_id, untrusted: true)
      |> Util.Encryption.MegolmSession.store_in_backup()

    Util.Encryption.MegolmSession.encrypt(session2, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Bonjour, tout le monde!"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    # start the test
    Util.Device.wait_for_login(user)

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
      key: Util.SSSS.make_key_repr(user, :default)
    )

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Test.SymmetricBackup.Fluent,
      "decryptable-messages",
      [],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})
  end
end
