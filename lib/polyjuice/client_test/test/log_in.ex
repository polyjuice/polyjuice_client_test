# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.Test.LogIn do
  @moduledoc """
  Log in test.

  Test that a client can log in.
  """

  use Polyjuice.ClientTest.TestHelper

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.ClientTest.Util

  def test_script(test_server) do
    user = Util.User.create(test_server, "test", "ThisIsATest")
    # {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(
        test_server,
        Polyjuice.ClientTest.Fluent,
        "log-in-with-password",
        user: user.id,
        password: user.password
      )

    logged_in =
      BaseTestServer.wait_until_event(
        test_server,
        fn
          {:login, _, _, _} ->
            true

          {:failed_login, {:password, user_id, _password}} ->
            if not String.starts_with?(user_id, "@test:") do
              :wrong_user
            else
              :wrong_password
            end

          {:failed_login, _} ->
            :wrong_method

          _ ->
            false
        end
      )

    BaseTestServer.send_done(test_server, prompt_event_id)

    case logged_in do
      true ->
        :ok

      :wrong_user ->
        raise Polyjuice.ClientTest.TestError,
          module: Polyjuice.ClientTest.Fluent,
          identifier: "login-failed-wrong-user"

      :wrong_password ->
        raise Polyjuice.ClientTest.TestError,
          module: Polyjuice.ClientTest.Fluent,
          identifier: "login-failed-wrong-password"

      :wrong_method ->
        raise Polyjuice.ClientTest.TestError,
          module: Polyjuice.ClientTest.Fluent,
          identifier: "login-failed-wrong-method"
    end
  end
end
