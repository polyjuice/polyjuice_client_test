# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.UnsignedBackupNotTrusted do
  @moduledoc """
  Unsigned backup is not trusted

  Tests that an unsigned backup is not trusted, even if the client obtains the
  private key.

  You should also check that the backup is trusted when the backup is
  signed, for example by using the "Unlock key backup after emoji
  self-verification" test and checking that the client is backup up keys.

  #e2ee #ssss #key-backup #rhul5 #rhul-sign-on-decrypt
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [&__MODULE__.backup_watcher/3],
    fluent: true

  def test_script(server) do
    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()
      |> Util.KeyBackup.create("m.megolm_backup.v1.curve25519-aes-sha2", no_cross_signing: true)
      |> Util.KeyBackup.store_in_ssss()

    room_name = "Backup trust test"

    {:ok, room_id} =
      Util.Room.create(server, user.id,
        name: room_name,
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    # start the test
    _device = Util.Device.wait_for_login(user)

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
      key: Util.SSSS.make_key_repr(user, :default)
    )

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "send-any-message-in-room",
        room: room_name,
        room_id: room_id
      )

    user_id = user.id

    BaseTestServer.wait_until_event(
      server,
      fn
        {:send_event, ^user_id, ^room_id,
         %{
           "type" => "m.room.encrypted",
           "content" => %{
             "algorithm" => "m.megolm.v1.aes-sha2",
             "ciphertext" => _,
             "session_id" => _
           }
         } = event} ->
          event

        _ ->
          false
      end
    )

    BaseTestServer.send_done(server, prompt_event_id)

    # see if the client backs up the key
    {:ok, progress_event_id} =
      BaseTestServer.send_progress(
        server,
        Polyjuice.ClientTest.Test.UnsignedBackupNotTrusted.Fluent,
        "waiting-for-backup"
      )

    # do the wait in a sub-process so that a timeout doesn't kill the test
    {:ok, supervisor} = Task.Supervisor.start_link()

    %{ref: ref} =
      Task.Supervisor.async_nolink(supervisor, fn ->
        BaseTestServer.wait_until_state(
          server,
          &Map.get(&1, :key_backed_up, false),
          30_000
        )
      end)

    got_backup =
      receive do
        {^ref, _res} ->
          true

        {:DOWN, ^ref, :process, _, _} ->
          false
      after
        60_000 ->
          false
      end

    BaseTestServer.send_done(server, progress_event_id)

    assert(
      got_backup == false,
      module: Polyjuice.ClientTest.Test.UnsignedBackupNotTrusted.Fluent,
      identifier: "key-backed-up"
    )
  end

  def backup_watcher(event, state, _server) do
    case event do
      {:key_backup_put_keys, _, _} ->
        Map.put(state, :key_backed_up, true)

      _ ->
        state
    end
  end
end
