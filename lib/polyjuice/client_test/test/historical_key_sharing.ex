# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.HistoricalKeySharing do
  @moduledoc """
  Historical key sharing

  Tests that a client marks keys as shareable depending on the room history
  settings, and will share (shareable) historical keys when inviting a user to
  a room.

  #e2ee #msc3061 #megolm
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Towel

  use Polyjuice.ClientTest.TestHelper,
    fluent: true,
    event_handlers: [&Util.Encryption.olm_event_handler/3]

  def test_script(server) do
    shared_history_name = "org.matrix.msc3061.shared_history"

    alice = Util.User.create(server, :aliceandbob, nil)

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()

    carol = Util.User.create(server, :aliceandbob, nil)

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()
      |> Util.Encryption.upload_olm_otks(1)

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "Historical key sharing test",
        invite: [bob.id],
        encryption: Util.Encryption.megolm_v1_algorithm(),
        power_levels: %{
          "invite" => 0,
          "users" => %{
            alice.id => 100
          }
        }
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    # start the test
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])
    # test sending a message when the history visibility is "shared"
    # - the session should be marked as shareable
    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "send-any-message-in-room",
        room: "Historical key sharing test"
      )

    bob_id = bob.id

    event1 =
      BaseTestServer.wait_until_event(
        server,
        fn
          {:send_event, ^bob_id, ^room_id,
           %{
             "type" => "m.room.encrypted",
             "content" => %{
               "algorithm" => "m.megolm.v1.aes-sha2",
               "ciphertext" => _,
               "session_id" => _
             }
           } = event} ->
            event

          _ ->
            false
        end
      )

    BaseTestServer.send_done(server, prompt_event_id)

    inbound_session1 =
      Util.Encryption.MegolmSession.wait_for_session(
        alice_device,
        room_id,
        event1["content"]["session_id"]
      )

    room_key_event1 =
      Util.Encryption.get_megolm_room_key_event(
        server,
        alice.id,
        alice_device.id,
        room_id,
        event1["content"]["session_id"]
      )

    assert match?(%{"content" => %{^shared_history_name => true}}, room_key_event1),
      module: Polyjuice.ClientTest.Test.HistoricalKeySharing.Fluent,
      identifier: "session-marked-as-not-shareable"

    {:ok, text1} =
      Util.Encryption.MegolmSession.decrypt_event(inbound_session1, event1)
      |> fmap(&Util.Room.get_event_text/1)

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "message-received",
      message: text1
    )

    outbound_session1 = Util.Encryption.MegolmSession.create(alice_device, room_id)

    alice_device =
      Util.Encryption.send_room_key_to(alice_device, outbound_session1, bob_device,
        extra_fields: [{shared_history_name, true}]
      )

    Util.Encryption.MegolmSession.encrypt(outbound_session1, "m.room.message", %{
      "body" => "You sent:\n#{text1}",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    # test sending a message when the history visibility is "joined"
    # - a new megolm session should be used
    # - the session should not be marked as shareable
    {:ok, _event_id} =
      Protocols.Room.send_event(
        server,
        alice.id,
        "",
        room_id,
        "m.room.history_visibility",
        "",
        nil,
        %{
          "history_visibility" => "joined"
        }
      )

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "send-any-message-in-room",
        room: "Historical key sharing test"
      )

    event2 =
      BaseTestServer.wait_until_event(
        server,
        fn
          {:send_event, ^bob_id, ^room_id,
           %{
             "type" => "m.room.encrypted",
             "content" => %{
               "algorithm" => "m.megolm.v1.aes-sha2",
               "ciphertext" => _,
               "session_id" => _
             }
           } = event} ->
            event

          _ ->
            false
        end
      )

    BaseTestServer.send_done(server, prompt_event_id)

    inbound_session2 =
      Util.Encryption.MegolmSession.wait_for_session(
        alice_device,
        room_id,
        event2["content"]["session_id"]
      )

    assert event1["content"]["session_id"] != event2["content"]["session_id"],
      module: Polyjuice.ClientTest.Test.HistoricalKeySharing.Fluent,
      identifier: "same-session-used"

    room_key_event2 =
      Util.Encryption.get_megolm_room_key_event(
        server,
        alice.id,
        alice_device.id,
        room_id,
        event2["content"]["session_id"]
      )

    assert not match?(%{"content" => %{^shared_history_name => true}}, room_key_event2),
      module: Polyjuice.ClientTest.Test.HistoricalKeySharing.Fluent,
      identifier: "session-marked-as-shareable"

    {:ok, text2} =
      Util.Encryption.MegolmSession.decrypt_event(inbound_session2, event2)
      |> fmap(&Util.Room.get_event_text/1)

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "message-received",
      message: text2
    )

    outbound_session2 = Util.Encryption.MegolmSession.create(alice_device, room_id)

    alice_device = Util.Encryption.send_room_key_to(alice_device, outbound_session2, bob_device)

    Util.Encryption.MegolmSession.encrypt(outbound_session2, "m.room.message", %{
      "body" => "You sent:\n#{text2}",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    # Now the user should invite Carol, and share the keys with her
    {:ok, _event_id} =
      Protocols.Room.send_event(
        server,
        alice.id,
        "",
        room_id,
        "m.room.history_visibility",
        "",
        nil,
        %{
          "history_visibility" => "shared"
        }
      )

    # create Carol's device
    carol_device =
      Util.Device.log_in(carol)
      |> Util.Encryption.initialize_olm()
      |> Util.Encryption.upload_olm_otks(1)

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "invite-user",
        user: carol.id
      )

    carol_id = carol.id

    BaseTestServer.wait_until_event(
      server,
      fn
        {:send_event, ^bob_id, ^room_id,
         %{
           "type" => "m.room.member",
           "state_key" => ^carol_id
         }} ->
          true

        _ ->
          false
      end
    )

    BaseTestServer.send_done(server, prompt_event_id)

    Util.Encryption.MegolmSession.wait_for_session(
      carol_device,
      room_id,
      event1["content"]["session_id"]
    )

    Util.Encryption.MegolmSession.wait_for_session(
      carol_device,
      room_id,
      outbound_session1.id
    )

    # give it a bit of time to share any other keys
    Process.sleep(500)

    assert Util.Encryption.get_megolm_room_key_event(
             server,
             carol.id,
             carol_device.id,
             room_id,
             event2["content"]["session_id"]
           ) == nil and
             Util.Encryption.get_megolm_room_key_event(
               server,
               carol.id,
               carol_device.id,
               room_id,
               outbound_session2.id
             ) == nil,
           module: Polyjuice.ClientTest.Test.HistoricalKeySharing.Fluent,
           identifier: "shared-unshareable-session"

    # FIXME: also check that the keys stored in backup has the shared_history
    # flag set
  end
end
