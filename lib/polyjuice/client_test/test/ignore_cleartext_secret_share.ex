# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.IgnoresCleartextSecretShare do
  @moduledoc """
  Ignores unencrypted secret share

  Tests that an unencrypted secret share is not trusted.

  You should also check that the backup is trusted when the backup is
  signed, for example by using the "Unlock key backup after emoji
  self-verification" test and checking that the client is backup up keys.

  #e2ee #ssss #key-backup #rhul5
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &Util.Encryption.Verification.to_device_event_handler/3,
      &Util.SSSS.secret_request_event_handler/3
    ]

  def test_script(server) do
    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()
      |> Util.KeyBackup.create("m.megolm_backup.v1.curve25519-aes-sha2")

    our_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    # create room and send an encrypted message
    room_name = "Key backup test"

    {:ok, room_id} =
      Util.Room.create(server, user.id,
        name: room_name,
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    session =
      Util.Encryption.MegolmSession.create(our_device, room_id)
      |> Util.Encryption.MegolmSession.store_in_backup()

    Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Hello world!"
    })
    |> (&Protocols.Room.send_event(server, user.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    # start the test
    their_device = Util.Device.wait_for_login(user)

    # verify the user
    {:ok, prompt_event_id} =
      BaseTestServer.send_message(
        server,
        Polyjuice.ClientTest.Fluent,
        "accept-verification-request"
      )

    verification =
      Util.Encryption.Verification.request_to_device(
        our_device,
        their_device,
        ["m.sas.v1"]
      )

    keys = %{
      ("ed25519:" <> user.cross_signing.msk.b64) => user.cross_signing.msk.b64,
      ("ed25519:" <> our_device.id) => Polyjuice.Newt.Account.ed25519_key(our_device.olm_account)
    }

    verification = Util.Encryption.Verification.SAS.verify(verification, keys, sas: [:emoji])

    if match?(%{state: {:error, _}}, verification) do
      # FIXME: make error more specific
      IO.puts(inspect(verification.state))

      raise Polyjuice.ClientTest.TestError,
        module: Polyjuice.ClientTest.Fluent,
        identifier: "verification-error"
    end

    {:verified, key_ids} = verification.state

    assert Enum.member?(key_ids, "ed25519:" <> their_device.id),
      # FIXME: make error more specific
      module: Polyjuice.ClientTest.Fluent,
      identifier: "verification-error"

    # send the backup key, and see if it can decrypt
    backup_key = Base.encode64(our_device.user.key_backup.key, padding: false)

    our_device = share_secret(our_device, their_device, "m.megolm_backup.v1", backup_key, room_id)

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: room_name],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :no})
  end

  # this is a copy of Polyjuice.ClientTest.Util.SSSS.share_secret except that it
  # shares the secret unencrypted
  def share_secret(
        %Util.Device{} = from,
        %Util.Device{} = to,
        name,
        data,
        room_id,
        timeout \\ 60_000
      ) do
    from_user = from.user.id
    from_device = from.id
    from_pair = {from_user, from_device}
    to_user = to.user.id
    to_device = to.id
    to_pair = {to_user, to_device}
    server = from.user.server

    # wait until we get a request
    requests =
      Polyjuice.ClientTest.BaseTestServer.wait_until_state(
        server,
        fn
          %{
            polyjuice: %{
              encryption: %{ssss_requests: %{^from_pair => %{^to_pair => %{} = requests}}}
            }
          } ->
            requests =
              Enum.flat_map(
                requests,
                fn
                  {req_id, ^name} -> [req_id]
                  _ -> []
                end
              )

            if requests != [], do: requests, else: false

          state ->
            false
        end,
        timeout
      )

    # remove the request from the queue
    Polyjuice.ClientTest.BaseTestServer.update_test_state(
      server,
      &update_in(&1, [:polyjuice, :encryption, :ssss_requests, from_pair, to_pair], fn req ->
        Map.drop(req, requests)
      end)
    )

    # and send the secret
    Enum.reduce(requests, from, fn req_id, device ->
      Polyjuice.ClientTest.Test.ToDeviceMegolm.send_to_device_with_room_id(
        device.user.server,
        device.user.id,
        device.id,
        "m.secret.send",
        room_id,
        %{
          to.user.id => %{
            to.id => %{
              "request_id" => req_id,
              "secret" => data
            }
          }
        }
      )

      device
    end)
  end
end
