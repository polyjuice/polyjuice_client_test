# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.IgnoresSecretShareFromAnotherUserMasqueradingAsYou do
  @moduledoc """
  Ignores secret share from another user masquerading as you

  Tests that an unencrypted secret share is not trusted.

  You should also check that the backup is trusted when the backup is
  signed, for example by using the "Unlock key backup after emoji
  self-verification" test and checking that the client is backup up keys.

  #e2ee #ssss #key-backup #rhul5
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &Util.Encryption.Verification.to_device_event_handler/3,
      &Util.SSSS.secret_request_event_handler/3
    ]

  def test_script(server) do
    alice =
      Util.User.create(server, :aliceandbob, nil)
      |> Util.CrossSigning.generate_keys()

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()
      |> Util.CrossSigning.cross_sign(alice)
      |> Util.KeyBackup.create("m.megolm_backup.v1.curve25519-aes-sha2")

    our_device =
      Util.Device.log_in(bob)
      |> Util.Encryption.initialize_olm()

    alice_device =
      Util.Device.log_in(alice, device_id: our_device.id)
      |> Util.Encryption.initialize_olm()

    # create room and send an encrypted message
    room_name = "Key backup test"

    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: room_name,
        invite: [bob.id],
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    session =
      Util.Encryption.MegolmSession.create(our_device, room_id)
      |> Util.Encryption.MegolmSession.store_in_backup()

    Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
      "msgtype" => "m.text",
      "body" => "Hello world!"
    })
    |> (&Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.encrypted", nil, nil, &1)).()

    # start the test
    their_device = Util.Device.wait_for_login(bob)

    # verify the user
    {:ok, prompt_event_id} =
      BaseTestServer.send_message(
        server,
        Polyjuice.ClientTest.Fluent,
        "accept-verification-request"
      )

    verification =
      Util.Encryption.Verification.request_to_device(
        our_device,
        their_device,
        ["m.sas.v1"]
      )

    keys = %{
      ("ed25519:" <> bob.cross_signing.msk.b64) => bob.cross_signing.msk.b64,
      ("ed25519:" <> our_device.id) => Polyjuice.Newt.Account.ed25519_key(our_device.olm_account)
    }

    verification = Util.Encryption.Verification.SAS.verify(verification, keys, sas: [:emoji])

    if match?(%{state: {:error, _}}, verification) do
      # FIXME: make error more specific
      IO.puts(inspect(verification.state))

      raise Polyjuice.ClientTest.TestError,
        module: Polyjuice.ClientTest.Fluent,
        identifier: "verification-error"
    end

    {:verified, key_ids} = verification.state

    assert Enum.member?(key_ids, "ed25519:" <> their_device.id),
      # FIXME: make error more specific
      module: Polyjuice.ClientTest.Fluent,
      identifier: "verification-error"

    # send the backup key, and see if it can decrypt
    backup_key = Base.encode64(our_device.user.key_backup.key, padding: false)

    our_device =
      share_secret(
        alice_device,
        our_device,
        their_device,
        "m.megolm_backup.v1",
        backup_key,
        room_id
      )

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: room_name],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :no})
  end

  # this is a copy of Polyjuice.ClientTest.Util.SSSS.share_secret except that it
  # shares the secret unencrypted
  def share_secret(
        %Util.Device{} = from,
        %Util.Device{} = requestee,
        %Util.Device{} = to,
        name,
        data,
        room_id,
        timeout \\ 60_000
      ) do
    from_user = from.user.id
    from_device = from.id
    from_pair = {from_user, from_device}
    requestee_user = requestee.user.id
    requestee_device = requestee.id
    requestee_pair = {requestee_user, requestee_device}
    to_user = to.user.id
    to_device = to.id
    to_pair = {to_user, to_device}
    server = from.user.server

    # wait until we get a request
    requests =
      Polyjuice.ClientTest.BaseTestServer.wait_until_state(
        server,
        fn
          %{
            polyjuice: %{
              encryption: %{ssss_requests: %{^requestee_pair => %{^to_pair => %{} = requests}}}
            }
          } ->
            requests =
              Enum.flat_map(
                requests,
                fn
                  {req_id, ^name} -> [req_id]
                  _ -> []
                end
              )

            if requests != [], do: requests, else: false

          state ->
            false
        end,
        timeout
      )

    # remove the request from the queue
    Polyjuice.ClientTest.BaseTestServer.update_test_state(
      server,
      &update_in(&1, [:polyjuice, :encryption, :ssss_requests, requestee_pair, to_pair], fn req ->
        Map.drop(req, requests)
      end)
    )

    # and send the secret
    txnid =
      Enum.reduce(requests, from.txnid, fn req_id, txnid ->
        {:ok, encrypted_secret} =
          Polyjuice.ClientTest.Util.Encryption.encrypt_olm(
            server,
            "m.secret.send",
            %{"request_id" => req_id, "secret" => data},
            from: requestee_pair,
            to: to_pair,
            olm_account: from.olm_account
          )

        Polyjuice.Server.Protocols.SendToDevice.send_to_device(
          server,
          requestee_user,
          requestee_device,
          "polyjuice_#{txnid}",
          "m.room.encrypted",
          %{to_user => %{to_device => encrypted_secret}}
        )

        txnid + 1
      end)

    %{from | txnid: txnid}
  end
end
