# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.SasDeviceConfusion do
  @moduledoc """
  Device confusion with emoji verification

  Tests that a client doesn't confuse device IDs and cross-signing keys when
  verifying.  A client should either refuse to verify a user that has a device
  ID equal to one of their cross-signing keys, or should not confuse them when
  performing verification.
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &__MODULE__.signature_accumulator/3
    ],
    fluent: true

  def test_script(server) do
    alice =
      Util.User.create(server, :aliceandbob, nil)
      |> Util.AccountData.disable_analytics()
      |> Util.CrossSigning.generate_keys()

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()

    # create a device for Alice that has the same ID as her MSK
    BaseTestServer.get_and_update_server_state(server, fn state ->
      state = put_in(state.users[alice.id].devices[alice.cross_signing.msk.b64], %{})
      {nil, state}
    end)

    alice_device =
      %Util.Device{
        user: alice,
        id: alice.cross_signing.msk.b64
      }
      |> Util.Encryption.initialize_olm()

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "Verification device confusion test",
        invite: [bob.id]
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    bob = Util.AccountData.set_dms(bob, %{alice.id => [room_id]})

    # wait for user to log in and cross-sign their device
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
      key: Util.SSSS.make_key_repr(bob, :default)
    )

    Util.CrossSigning.wait_until_cross_signed(bob_device)

    # request verification
    BaseTestServer.send_message(
      server,
      Polyjuice.ClientTest.Fluent,
      "accept-verification-request"
    )

    verification =
      Util.Encryption.Verification.request_in_room(
        alice_device,
        bob,
        room_id,
        ["m.sas.v1"]
      )

    # wait for signatures.  We do this in a separate process because the client
    # could upload the signatures before the verification is completed, so we
    # need to start waiting for the signatures before we start the verification
    bob_id = bob.id

    signature_waiter =
      Task.async(fn ->
        BaseTestServer.wait_until_event(
          server,
          fn
            {:add_signatures, ^bob_id, _} -> true
            _ -> false
          end,
          300_000
        )
      end)

    keys = %{
      ("ed25519:" <> alice_device.id) =>
        Polyjuice.Newt.Account.ed25519_key(alice_device.olm_account)
    }

    # finish the verification
    verification = Util.Encryption.Verification.SAS.verify(verification, keys, sas: [:emoji])

    # TODO: also pass if verification failed
    if not match?({:error, _}, verification.state) do
      Task.await(signature_waiter)

      # give it a bit of time to upload any more signatures
      Process.sleep(500)

      # check uploaded signatures
      signatures = BaseTestServer.get_test_state(server, &Map.get(&1, :signatures))

      alice_id = alice.id
      alice_device_id = alice_device.id

      signed_msk =
        Enum.any?(signatures, fn
          %{^alice_id => %{^alice_device_id => %{"keys" => keys}}} ->
            Map.get(keys, "ed25519:#{alice_device.id}") == alice_device.id

          _ ->
            false
        end)

      assert signed_msk == false,
        module: __MODULE__.Fluent,
        identifier: "master-key-signed"
    end
  end

  def signature_accumulator(event, state, _server) do
    case event do
      {:add_signatures, <<"@bob:", _::binary>>, sig} ->
        Map.get(state, :signatures, [])
        |> (&[sig | &1]).()
        |> (&Map.put(state, :signatures, &1)).()

      _ ->
        state
    end
  end
end
