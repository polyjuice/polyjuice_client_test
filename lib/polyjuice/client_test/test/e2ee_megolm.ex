# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.E2EEMegolm do
  @moduledoc """
  End-to-end encryption using olm/megolm

  Tests that a client can send and receive olm/megolm messages.

  #e2ee #megolm
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper, event_handlers: [&Util.Encryption.olm_event_handler/3]

  def test_script(server) do
    alice = Util.User.create(server, :aliceandbob, nil)

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()
      |> Util.Encryption.upload_olm_otks(1)

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "Encryption test",
        invite: [bob.id],
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    # start the test
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "send-any-message-in-room",
        room: "Encryption test"
      )

    bob_id = bob.id

    event =
      BaseTestServer.wait_until_event(
        server,
        fn
          {:send_event, ^bob_id, ^room_id,
           %{
             "type" => "m.room.encrypted",
             "content" => %{
               "algorithm" => "m.megolm.v1.aes-sha2",
               "ciphertext" => _,
               "session_id" => _
             }
           } = event} ->
            event

          _ ->
            false
        end
      )

    BaseTestServer.send_done(server, prompt_event_id)

    event_content = event["content"]

    inbound_session =
      Util.Encryption.MegolmSession.wait_for_session(
        alice_device,
        room_id,
        event_content["session_id"]
      )

    with {:ok, decrypted_event} <-
           Util.Encryption.MegolmSession.decrypt_event(inbound_session, event) do
      text = Util.Room.get_event_text(decrypted_event)

      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "message-received",
        message: text
      )

      session = Util.Encryption.MegolmSession.create(alice_device, room_id)

      alice_device = Util.Encryption.send_room_key_to(alice_device, session, bob_device)

      Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
        "body" => "You sent:\n#{text}",
        "msgtype" => "m.notice"
      })
      |> (&Protocols.Room.send_event(
            server,
            alice.id,
            "",
            room_id,
            "m.room.encrypted",
            nil,
            nil,
            &1
          )).()

      BaseTestServer.prompt(
        server,
        Polyjuice.ClientTest.Fluent,
        "is-message-decryptable",
        [room: "Encryption test"],
        {:choice,
         [
           yes: {Polyjuice.ClientTest.Fluent, "yes"},
           no: {Polyjuice.ClientTest.Fluent, "no"}
         ]},
        timeout: 300_000
      )
      |> assert_eq({:ok, :yes})
    else
      _ ->
        raise Polyjuice.ClientTest.TestError,
          module: Polyjuice.ClientTest.Fluent,
          identifier: "decryption-error"
    end
  end
end
