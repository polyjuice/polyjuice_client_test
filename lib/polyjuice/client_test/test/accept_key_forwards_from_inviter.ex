# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.AcceptKeyForwardsFromInviter do
  @moduledoc """
  Accepts key forwards from inviter

  Tests that a client accepts key forwards from the user that invited them to a room.

  Bob sends a message in a room.  Alice invites Carol.  Carol logs in.  Alice
  forwards the key to Carol.  Carol should accept Alice's key forward since
  Alice invited him.

  This test is only valid if the client also passes the "Unsolicited key
  forwards" test.
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper

  def test_script(server) do
    alice = Util.User.create(server, :aliceandbob, nil)

    bob = Util.User.create(server, :aliceandbob, nil)

    carol =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()

    bob_device =
      Util.Device.log_in(bob)
      |> Util.Encryption.initialize_olm()

    # prepare the room
    room_name = "Key forward on invite"

    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: room_name,
        invite: [bob.id, carol.id],
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    session = Util.Encryption.MegolmSession.create(bob_device, room_id)

    {:ok, _event_id} =
      Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
        "body" => "This message should be decryptable, but marked as untrusted",
        "msgtype" => "m.notice"
      })
      |> (&Protocols.Room.send_event(
            server,
            bob.id,
            "",
            room_id,
            "m.room.encrypted",
            nil,
            nil,
            &1
          )).()

    {:ok, _event_id} =
      Protocols.Room.send_event(server, carol.id, "", room_id, "m.room.member", carol.id, nil, %{
        "membership" => "join"
      })

    # start the test
    carol_device = Util.Device.wait_for_login(carol, wait_for: [:olm_otks, :device_keys])

    alice_device =
      Util.Encryption.forward_room_key_to(
        alice_device,
        session,
        carol_device,
        forwarding_curve25519_key_chain: [],
        sender_claimed_ed25519_key: Polyjuice.Newt.Account.ed25519_key(bob_device.olm_account),
        sender_key: Polyjuice.Newt.Account.curve25519_key(bob_device.olm_account),
        extra_fields: [{"org.matrix.msc3061.shared_history", true}],
        immediate: true
      )

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: room_name],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})
  end
end
