# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.CrossSigningSSSS do
  @moduledoc """
  Cross-signing with SSSS.

  Tests that a client can unlock SSSS to cross-sign their device.

  #e2ee #cross-signing #ssss
  """

  use Polyjuice.ClientTest.TestHelper

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  def test_script(server) do
    # FIXME: allow selecting padded/unpadded base64

    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()

    device = Util.Device.wait_for_login(user)

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
        key: Util.SSSS.make_key_repr(user, :default)
      )

    Util.CrossSigning.wait_until_cross_signed(device)

    BaseTestServer.send_done(server, prompt_event_id)
  end
end
