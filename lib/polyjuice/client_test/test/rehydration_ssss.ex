# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.RehydrationSSSS do
  @moduledoc """
  Rehydrate a device with SSSS

  Tests that a client can rehydrate a device by entering an SSSS key, and fetch
  the megolm keys sent to it.

  #e2ee #msc3814 #megolm
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    server_opts: [custom_plug: &Polyjuice.ClientTest.Test.RehydrationSSSS.custom_plug/2],
    fluent: true

  def test_script(server) do
    dehydration_key = :crypto.strong_rand_bytes(32)
    dehydration_key_b64 = Base.encode64(dehydration_key, padding: false)

    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.SSSS.store("org.matrix.msc3814", dehydration_key_b64)

    # create the dehydrated device
    dehydrated_device_data = %{
      identity: :crypto.generate_key(:ecdh, :x25519, :crypto.strong_rand_bytes(32)),
      fingerprint: :crypto.generate_key(:eddsa, :ed25519, :crypto.strong_rand_bytes(32)),
      otks:
        Enum.map(1..50, fn ->
          :crypto.generate_key(:ecdh, :x25519, :crypto.strong_rand_bytes(32))
        end),
      fallback: :crypto.generate_key(:ecdh, :x25519, :crypto.strong_rand_bytes(32))
    }

    dehydrated_device = %Util.Device{
      user: user,
      # no existing devices yet, so don't need to worry about collisions
      id: Polyjuice.Util.Randomizer.randomize(10)
    }

    BaseTestServer.get_and_update_server_state(server, fn state ->
      state =
        state
        |> put_in([:custom, :dehydrated], %{
          user.id => %{
            device_id: dehydrated_device.id,
            device_data: %{
              "algorithm" => Util.Encryption.megolm_v1_algorithm(),
              "account" => "foo"
            }
          }
        })
        |> put_in([:users, user.id, :devices, dehydrated_device.id], %{})

      # FIXME: set device keys and otks
      {nil, state}
    end)

    # create a room and send encrypted messages to it
    {:ok, room_id} =
      Util.Room.create(server, user.id,
        name: "Dehydration test",
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    temp_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    session = Util.Encryption.MegolmSession.create(temp_device, room_id)
    temp_device = Util.Encryption.send_room_key_to(temp_device, session, dehydrated_device)

    Util.Encryption.MegolmSession.encrypt(session, "m.room.message", %{
      "body" => "Hello world!",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          user.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    temp_device = Util.Device.log_out(temp_device)

    # prompt the user to log in and unlock SSSS
    device = Util.Device.wait_for_login(user)

    BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
      key: Util.SSSS.make_key_repr(user, :default)
    )

    # FIXME: wait for rehydrated event

    # ask if the user can decrypt
    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: "Dehydration test"],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})

    # FIXME: wait for dehydrated device
  end

  def custom_plug(
        %{path_info: ["_matrix", "client", "unstable", "org.matrix.msc3814.v1" | path_rest]} =
          conn,
        opts
      ) do
    Plug.forward(conn, path_rest, Polyjuice.ClientTest.Test.RehydrationSSSS.Router, opts)
  end

  def custom_plug(conn, _opts) do
    conn
  end

  defmodule Router do
    use Plug.Router

    plug(:match)
    plug(:dispatch, builder_opts())

    alias Polyjuice.ClientTest.Test.RehydrationSSSS.Router

    put "/dehydrated_device" do
      Plug.forward(conn, [], Router.PutDehydratedDevice, opts)
      |> Plug.Conn.halt()
    end

    get "/dehydrated_device" do
      Plug.forward(conn, [], Router.GetDehydratedDevice, opts)
      |> Plug.Conn.halt()
    end

    post "/dehydrated_device/:device_id/events" do
      Plug.forward(conn, [], Router.PostDehydratedDeviceEvents, opts)
      |> Plug.Conn.halt()
    end
  end

  defmodule Router.PutDehydratedDevice do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:put_dehydrated_device, builder_opts())

    def put_dehydrated_device(conn, opts) do
      user_id = conn.assigns[:user].user_id
      state = conn.assigns[:polyjuice_client_test_server_state]

      device_data =
        Map.get_lazy(conn.body_params, "device_data", fn ->
          raise Polyjuice.Server.Plug.MatrixError,
            plug_status: 400,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MBadJson.new("missing device_data")
        end)

      # FIXME: handle initial_device_display_name

      state =
        case Map.get(state.custom, :dehydrated) do
          nil ->
            state

          # if we had a dehydrated device before, remove it
          %{^user_id => %{device_id: device_id}} ->
            {:reply, :ok, state} =
              Polyjuice.ClientTest.BaseTestServer.User.log_out({user_id, device_id}, nil, state)

            state
        end

      {:ok, user_info} = Map.fetch(state.users, user_id)

      device_id =
        Polyjuice.ClientTest.Util.make_random_key(user_info.devices, fn ->
          Polyjuice.Util.Randomizer.randomize(10, :upcase)
        end)

      state =
        state
        |> update_in(
          [:custom, :dehydrated],
          fn
            nil ->
              %{user_id => %{device_id: device_id, device_data: device_data}}

            dehydrated ->
              Map.put(dehydrated, user_id, %{device_id: device_id, device_data: device_data})
          end
        )
        |> put_in([:users, user_id, :devices, device_id], %{})

      state =
        Polyjuice.ClientTest.BaseTestServer.emit(
          {:dehydrated, user_id, device_id, device_data},
          state
        )

      conn = put_in(conn.assigns[:polyjuice_client_test_server_state], state)

      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Jason.encode!(%{"device_id" => device_id}))
    end
  end

  defmodule Router.GetDehydratedDevice do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:get_dehydrated_device, builder_opts())

    def get_dehydrated_device(conn, opts) do
      user_id = conn.assigns[:user].user_id
      state = conn.assigns[:polyjuice_client_test_server_state]

      with %{device_id: device_id, device_data: device_data} <-
             get_in(state, [:custom, :dehydrated, user_id]) do
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(
          200,
          Jason.encode!(%{"device_id" => device_id, "device_data" => device_data})
        )
      else
        _ ->
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(404, ~s({"errcode":"M_NOT_FOUND,"error":"No dehydrated device"""}))
      end
    end
  end

  defmodule Router.PostDehydratedDeviceEvents do
    use Plug.Builder

    import Polyjuice.Server.Plug.ClientAuthentication,
      only: [client_authentication: 2]

    plug(:client_authentication, builder_opts())
    plug(Plug.Parsers, parsers: [{:json, json_decoder: Jason}])
    plug(:get_events, builder_opts())

    def get_events(conn, opts) do
      user_id = conn.assigns[:user].user_id
      state = conn.assigns[:polyjuice_client_test_server_state]
      device_id = conn.path_params["device_id"]

      if match?(%{device_id: ^device_id}, get_in(state, [:custom, :dehydrated, user_id])) do
        next_batch =
          try do
            Map.fetch!(conn.body_params, "next_batch")
            |> String.to_integer()
          rescue
            _ -> 0
          end

        user_to_device_stream =
          Map.get(state, :to_device_stream, %{})
          |> Map.get({user_id, device_id}, [])

        to_device_msgs =
          if match?([{_, :empty}], user_to_device_stream) do
            []
          else
            Enum.take_while(user_to_device_stream, fn {pos, _event} -> pos > next_batch end)
            |> Enum.map(fn {_, event} -> event end)
          end

        to_device_end_pos =
          user_to_device_stream
          |> Enum.at(0, {0})
          |> elem(0)

        # FIXME: limit number of messages we send

        state =
          if next_batch == 0 do
            Polyjuice.ClientTest.BaseTestServer.emit(
              {:rehydrated, user_id, device_id},
              state
            )
          else
            state
          end

        conn = put_in(conn.assigns[:polyjuice_client_test_server_state], state)

        if to_device_msgs != [] do
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(
            200,
            Jason.encode!(%{
              "events" => Enum.reverse(to_device_msgs),
              "next_batch" => to_string(to_device_end_pos)
            })
          )
        else
          conn
          |> put_resp_content_type("application/json")
          |> send_resp(200, Jason.encode!(%{"events" => []}))
        end
      else
        conn
        |> put_resp_content_type("application/json")
        |> send_resp(404, ~s({"errcode":"M_NOT_FOUND,"error":"No dehydrated device"""}))
      end
    end
  end
end
