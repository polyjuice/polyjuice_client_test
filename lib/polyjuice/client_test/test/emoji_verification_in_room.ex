# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.EmojiVerificationInRoom do
  @moduledoc """
  Verify another user in a room using emoji.

  Tests that a client can verify another user using emoji when it logs in.

  #e2ee #verification
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.ClientTest.Util
  alias Polyjuice.Server.Protocols

  use Polyjuice.ClientTest.TestHelper

  def test_script(server) do
    alice =
      Util.User.create(server, :aliceandbob, nil)
      |> Util.AccountData.disable_analytics()
      |> Util.CrossSigning.generate_keys()

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()
      |> Util.CrossSigning.store_in_ssss()

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "Emoji verification test",
        invite: [bob.id]
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    bob = Util.AccountData.set_dms(bob, %{alice.id => [room_id]})

    # wait for user to log in and cross-sign their device
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(server, Polyjuice.ClientTest.Fluent, "unlock-ssss-with-key",
        key: Util.SSSS.make_key_repr(bob, :default)
      )

    Util.CrossSigning.wait_until_cross_signed(bob_device)

    BaseTestServer.send_done(server, prompt_event_id)

    # request verification
    {:ok, prompt_event_id} =
      BaseTestServer.send_message(
        server,
        Polyjuice.ClientTest.Fluent,
        "accept-verification-request"
      )

    verification =
      Util.Encryption.Verification.request_in_room(
        alice_device,
        bob,
        room_id,
        ["m.sas.v1"]
      )

    BaseTestServer.send_done(server, prompt_event_id)

    keys = %{
      ("ed25519:" <> alice.cross_signing.msk.b64) => alice.cross_signing.msk.b64,
      ("ed25519:" <> alice_device.id) =>
        Polyjuice.Newt.Account.ed25519_key(alice_device.olm_account)
    }

    verification = Util.Encryption.Verification.SAS.verify(verification, keys, sas: [:emoji])

    if match?(%{state: {:error, _}}, verification) do
      # FIXME: make error more specific
      IO.puts(inspect(verification.state))

      raise Polyjuice.ClientTest.TestError,
        module: Polyjuice.ClientTest.Fluent,
        identifier: "verification-error"
    end

    {:verified, key_ids} = verification.state

    assert Enum.member?(key_ids, "ed25519:" <> bob_device.id),
      # FIXME: make error more specific
      module: Polyjuice.ClientTest.Fluent,
      identifier: "verification-error"

    # FIXME: also check if the cross-signing key was verified (needs change to
    # verification util function)
  end
end
