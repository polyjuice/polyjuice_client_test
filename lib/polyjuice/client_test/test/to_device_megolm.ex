# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.ToDeviceMegolm do
  @moduledoc """
  Ignores megolm-encrypted to_device messages

  Tests that a client ignores to_devices messages encrypted with megolm.

  #rhul2 #rhul5
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper

  def test_script(server) do
    alice = Util.User.create(server, :aliceandbob, nil)

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()
      |> Util.Encryption.upload_olm_otks(1)

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "To-device megolm test",
        invite: [bob.id],
        encryption: "m.megolm.v1.aes-sha2"
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    # start the test
    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    # we send megolm session1 (via olm), which we use to encrypt megolm
    # session2, which we use to encrypt a room message.  If the client can
    # decrypt the room message, then it used session2, which it shouldn't have.
    session1 = Util.Encryption.MegolmSession.create(alice_device, room_id)
    alice_device = Util.Encryption.send_room_key_to(alice_device, session1, bob_device)

    # delay between sending the keys, to try to make sure that the client
    # processes the first message before the second one, we depend on the right
    # order
    Process.sleep(500)

    session2 = Util.Encryption.MegolmSession.create(alice_device, room_id)
    session2_key = Polyjuice.Newt.GroupSession.get_key(session2.outbound)

    event =
      Util.Encryption.MegolmSession.encrypt(session1, "m.room_key", %{
        "algorithm" => "m.megolm.v1.aes-sha2",
        "room_id" => room_id,
        "session_id" => session2.id,
        "session_key" => session2_key
      })

    send_to_device_with_room_id(
      server,
      alice.id,
      alice_device.id,
      "m.room.encrypted",
      room_id,
      %{bob.id => %{bob_device.id => event}}
    )

    Process.sleep(500)

    # send a message to the room encrypted with the second session
    Util.Encryption.MegolmSession.encrypt(session2, "m.room.message", %{
      "body" => "This message should not be decryptable",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Fluent,
      "is-message-decryptable",
      [room: "To-device megolm test"],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> (&assert(&1 == {:ok, :no})).()
  end

  # send a to-device message, but include a room ID, since the client will
  # check that the event claims to be sent to a room
  def send_to_device_with_room_id(server, user_id, _device_id, event_type, room_id, messages) do
    BaseTestServer.get_and_update_server_state(server, fn state ->
      # copied from Polyjuice.ClientTest.BaseTestServer.SendToDevice.send_to_device
      state =
        Enum.reduce(messages, state, fn {target_user_id, device_events}, state ->
          Enum.reduce(device_events, state, fn {target_device_id, content}, state ->
            full_event = %{
              "sender" => user_id,
              "type" => event_type,
              "content" => content,
              "room_id" => room_id
            }

            # queue the event to be sent to a specific device
            send_to = fn target_device_id, state ->
              [{stream_pos, _} | _] =
                device_stream =
                case Map.get(state.to_device_stream, {target_user_id, target_device_id}, []) do
                  [] ->
                    [{1, full_event}]

                  [{pos, :empty}] ->
                    [{pos + 1, full_event}]

                  [{pos, _} | _] = stream ->
                    [{pos + 1, full_event} | stream]
                end

              if Map.has_key?(state, :subscribers) do
                Polyjuice.ClientTest.BaseTestServer.PubSub.publish(
                  {{:to_device, stream_pos, full_event}, [{target_user_id, target_device_id}]},
                  nil,
                  state
                )
              end

              state =
                Polyjuice.ClientTest.BaseTestServer.emit(
                  {:send_to_device, target_user_id, target_device_id, full_event},
                  state
                )

              %{
                state
                | to_device_stream:
                    Map.put(
                      state.to_device_stream,
                      {target_user_id, target_device_id},
                      device_stream
                    )
              }
            end

            if target_device_id == "*" do
              {:reply, devices, _} =
                Polyjuice.ClientTest.BaseTestServer.User.get_user_devices(
                  {target_user_id},
                  nil,
                  state
                )

              Enum.reduce(devices, state, send_to)
            else
              send_to.(target_device_id, state)
            end
          end)
        end)

      {:ok, state}
    end)
  end
end
