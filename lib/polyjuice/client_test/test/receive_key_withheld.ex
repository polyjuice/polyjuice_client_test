# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.ReceiveKeyWithheld do
  @moduledoc """
  Receive key withheld codes

  Tests that a client can recognize key withheld codes and indicate the reason
  messages are withheld.

  #e2ee #msc2399 #megolm #utd
  """

  use Polyjuice.ClientTest.TestHelper, fluent: true

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.Server.Protocols
  alias Polyjuice.ClientTest.Util

  def test_script(server) do
    withheld_name =
      BaseTestServer.prompt(
        server,
        Polyjuice.ClientTest.Fluent,
        "stable-or-unstable",
        [],
        {:choice,
         [
           stable: {Polyjuice.ClientTest.Fluent, "stable"},
           unstable: {Polyjuice.ClientTest.Fluent, "unstable"}
         ]},
        timeout: 60_000
      )
      |> case do
        {:ok, :stable} -> "m.room_key.withheld"
        {:ok, :unstable} -> "org.matrix.room_key.withheld"
      end

    alice = Util.User.create(server, :aliceandbob, nil)

    bob =
      Util.User.create(server, :aliceandbob, "ThisIsATest")
      |> Util.AccountData.disable_analytics()

    alice_device =
      Util.Device.log_in(alice)
      |> Util.Encryption.initialize_olm()

    # prepare the room
    {:ok, room_id} =
      Util.Room.create(server, alice.id,
        name: "Key withheld test",
        invite: [bob.id],
        encryption: Util.Encryption.megolm_v1_algorithm()
      )

    {:ok, _event_id} =
      Protocols.Room.send_event(server, bob.id, "", room_id, "m.room.member", bob.id, nil, %{
        "membership" => "join"
      })

    bob_device = Util.Device.wait_for_login(bob, wait_for: [:olm_otks, :device_keys])

    # bob is blacklisted
    session1 = Util.Encryption.MegolmSession.create(alice_device, room_id)

    Util.Encryption.MegolmSession.encrypt(session1, "m.room.message", %{
      "body" => "This should be unreadable",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    Polyjuice.Server.Protocols.SendToDevice.send_to_device(
      server,
      alice.id,
      alice_device.id,
      "1",
      withheld_name,
      %{
        bob.id => %{
          bob_device.id => %{
            "room_id" => room_id,
            "algorithm" => "m.megolm.v1.aes-sha2",
            "session_id" => session1.id,
            "sender_key" => Polyjuice.Newt.Account.curve25519_key(alice_device.olm_account),
            "code" => "m.blacklisted"
          }
        }
      }
    )

    alice_device = Util.Encryption.send_room_key_to(alice_device, session1, bob_device)

    Util.Encryption.MegolmSession.encrypt(session1, "m.room.message", %{
      "body" => "This should be readable",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Test.ReceiveKeyWithheld.Fluent,
      "check-blocked",
      [],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})

    # bob is unverified
    session2 = Util.Encryption.MegolmSession.create(alice_device, room_id)

    Util.Encryption.MegolmSession.encrypt(session2, "m.room.message", %{
      "body" => "This should be unreadable",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    Polyjuice.Server.Protocols.SendToDevice.send_to_device(
      server,
      alice.id,
      alice_device.id,
      "2",
      withheld_name,
      %{
        bob.id => %{
          bob_device.id => %{
            "room_id" => room_id,
            "algorithm" => "m.megolm.v1.aes-sha2",
            "session_id" => session2.id,
            "sender_key" => Polyjuice.Newt.Account.curve25519_key(alice_device.olm_account),
            "code" => "m.unverified"
          }
        }
      }
    )

    alice_device = Util.Encryption.send_room_key_to(alice_device, session2, bob_device)

    Util.Encryption.MegolmSession.encrypt(session1, "m.room.message", %{
      "body" => "This should be readable",
      "msgtype" => "m.notice"
    })
    |> (&Protocols.Room.send_event(
          server,
          alice.id,
          "",
          room_id,
          "m.room.encrypted",
          nil,
          nil,
          &1
        )).()

    BaseTestServer.prompt(
      server,
      Polyjuice.ClientTest.Test.ReceiveKeyWithheld.Fluent,
      "check-unverified",
      [],
      {:choice,
       [
         yes: {Polyjuice.ClientTest.Fluent, "yes"},
         no: {Polyjuice.ClientTest.Fluent, "no"}
       ]},
      timeout: 300_000
    )
    |> assert_eq({:ok, :yes})

    # FIXME: also check no_olm, and see if the client creates a new olm session
    # and makes a key request
  end
end
