# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Test.EmojiSelfVerifyOnLogin do
  @moduledoc """
  Self-verify using emoji on login.

  Tests that a client can verify another device using emoji when it logs in.

  #e2ee #cross-signing #verification
  """

  alias Polyjuice.ClientTest.BaseTestServer
  alias Polyjuice.ClientTest.Util

  use Polyjuice.ClientTest.TestHelper,
    event_handlers: [
      &Util.Encryption.Verification.to_device_event_handler/3
    ]

  def test_script(server) do
    user =
      Util.User.create(server, "test", "ThisIsATest")
      |> Util.SSSS.create_default()
      |> Util.CrossSigning.generate_keys()

    our_device =
      Util.Device.log_in(user)
      |> Util.Encryption.initialize_olm()

    their_device = Util.Device.wait_for_login(user, wait_for: [:device_keys])

    {:ok, prompt_event_id} =
      BaseTestServer.send_message(
        server,
        Polyjuice.ClientTest.Fluent,
        "accept-verification-request"
      )

    verification =
      Util.Encryption.Verification.request_to_device(
        our_device,
        their_device,
        ["m.sas.v1"]
      )

    BaseTestServer.send_done(server, prompt_event_id)

    keys = %{
      ("ed25519:" <> user.cross_signing.msk.b64) => user.cross_signing.msk.b64,
      ("ed25519:" <> our_device.id) => Polyjuice.Newt.Account.ed25519_key(our_device.olm_account)
    }

    verification = Util.Encryption.Verification.SAS.verify(verification, keys, sas: [:emoji])

    if match?(%{state: {:error, _}}, verification) do
      # FIXME: make error more specific
      IO.puts(inspect(verification.state))

      raise Polyjuice.ClientTest.TestError,
        module: Polyjuice.ClientTest.Fluent,
        identifier: "verification-error"
    end

    {:verified, key_ids} = verification.state

    assert Enum.member?(key_ids, "ed25519:" <> their_device.id),
      # FIXME: make error more specific
      module: Polyjuice.ClientTest.Fluent,
      identifier: "verification-error"
  end
end
