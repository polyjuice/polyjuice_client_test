# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.DashboardServer.Room do
  @moduledoc false
  @behaviour Polyjuice.Server.Protocols.Helper.Room

  @impl Polyjuice.Server.Protocols.Helper.Room
  def create({_}, _from, state) do
    {:reply,
     {:error,
      %Polyjuice.Server.Plug.MatrixError{
        message: "Not allowed",
        plug_status: 403,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
      }}, state}
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def join({user_id, room_id, _opts}, _from, state) do
    servername = state.server.servername
    clienttest_user = "@test:#{servername}"

    with {:ok, parsed_test_room_id} <-
           Polyjuice.Util.Identifiers.V1.RoomIdentifier.new(room_id),
         {:ok, parsed_user_id} <- Polyjuice.Util.Identifiers.V1.UserIdentifier.new(user_id),
         {:ok, %{sync_token: sync_token, rooms: user_tests}} <-
           Map.fetch(state.users, parsed_user_id),
         {:ok, test_data} <- Map.fetch(state.server.test_data, parsed_test_room_id.opaque_id),
         test_mod = String.to_existing_atom(parsed_test_room_id.opaque_id) do
      room_id =
        Polyjuice.ClientTest.Util.make_random_key(state.test_servers, fn ->
          Polyjuice.Util.Identifiers.V1.RoomIdentifier.generate(servername)
        end)

      events =
        Enum.concat([
          [
            %{
              "sender" => clienttest_user,
              "type" => "m.room.create",
              "state_key" => "",
              "content" => %{
                "creator" => clienttest_user,
                "room_version" => "5",
                "ca.uhoreg.polyjuice.client_test.test_id" => to_string(parsed_test_room_id)
              }
            },
            %{
              "sender" => clienttest_user,
              "type" => "m.room.member",
              "state_key" => clienttest_user,
              "content" => %{
                "membership" => "join"
              }
            },
            %{
              "sender" => clienttest_user,
              "type" => "m.room.power_levels",
              "state_key" => "",
              "content" => %{
                "users" => %{
                  clienttest_user => 100
                }
              }
            },
            %{
              "sender" => clienttest_user,
              "type" => "m.room.join_rules",
              "state_key" => "",
              "content" => %{
                "join_rule" => "invite"
              }
            },
            %{
              "sender" => clienttest_user,
              "type" => "m.room.history_visibility",
              "state_key" => "",
              "content" => %{
                "history_visibility" => "shared"
              }
            },
            %{
              "sender" => clienttest_user,
              "type" => "m.room.name",
              "state_key" => "",
              "content" => %{
                "name" => test_data.name
              }
            }
          ],
          case test_data do
            %{topic: topic} ->
              [
                %{
                  "sender" => clienttest_user,
                  "type" => "m.room.topic",
                  "state_key" => "",
                  "content" => %{
                    "topic" => topic
                  }
                }
              ]

            _ ->
              []
          end,
          [
            %{
              "sender" => clienttest_user,
              "type" => "m.room.member",
              "state_key" => user_id,
              "content" => %{
                "membership" => "invite"
              }
            },
            %{
              "sender" => user_id,
              "type" => "m.room.member",
              "state_key" => user_id,
              "content" => %{
                "membership" => "join"
              }
            }
          ]
        ])

      {events, last_state} =
        Polyjuice.ClientTest.Util.make_room_events_v4(events, room_id, %{}, nil, nil)

      :ets.insert(state.ets, {{:room, room_id.opaque_id, sync_token + 1}, events})
      :ets.insert(state.ets, {{:room_last_state, room_id.opaque_id}, last_state})
      test_host = "#{room_id.opaque_id}.#{state.server.test_domain}"
      test = apply(test_mod, :new, [state.server, user_id, room_id.opaque_id, test_host])
      new_test_servers = Map.put(state.test_servers, room_id.opaque_id, {test, parsed_user_id})
      ref = Polyjuice.ClientTest.TestServer.monitor(test)
      new_monitors = Map.put(state.monitors, ref, room_id.opaque_id)

      new_users =
        Map.put(state.users, parsed_user_id, %{
          sync_token: sync_token + 1,
          rooms: MapSet.put(user_tests, room_id)
        })

      Polyjuice.ClientTest.DashboardServer.PubSub.publish(
        {{:room_events, sync_token + 1, to_string(room_id), events},
         [user_id, room_id.opaque_id]},
        nil,
        state
      )

      {:reply, :ok,
       %{state | test_servers: new_test_servers, users: new_users, monitors: new_monitors}}
    else
      _ ->
        {:reply, {:error, :unknown_room}, state}
    end
  end

  @impl Polyjuice.Server.Protocols.Helper.Room
  def send_event(
        {user_id, _device_id, room_id, event_type, state_key, _txn_id, content},
        _from,
        state
      ) do
    # FIXME: handle txn_id.  We ignore it for now because the front end doesn't
    # do retries anyways

    event =
      if state_key == nil do
        %{"type" => event_type, "sender" => user_id, "content" => content, "room_id" => room_id}
      else
        %{
          "type" => event_type,
          "state_key" => state_key,
          "sender" => user_id,
          "content" => content,
          "room_id" => room_id
        }
      end

    with {:ok, parsed_room_id} <-
           Polyjuice.Util.Identifiers.V1.RoomIdentifier.new(room_id),
         [{_, {room_state, _, _}}] <-
           :ets.lookup(state.ets, {:room_last_state, parsed_room_id.opaque_id}) do
      if Polyjuice.Util.RoomVersion.authorized?("5", event, room_state) do
        {[event_id], state} =
          Polyjuice.ClientTest.DashboardServer._send_events(state, parsed_room_id, [event])

        {:reply, {:ok, event_id}, state}
      else
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Forbidden",
            plug_status: 403,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
          }}, state}
      end
    else
      _ ->
        {:reply,
         {:error,
          %Polyjuice.Server.Plug.MatrixError{
            message: "Not found",
            plug_status: 404,
            matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
          }}, state}
    end
  end
end
