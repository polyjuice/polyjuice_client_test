# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.BaseTestServer do
  @moduledoc """
  Basic test server functionality.

  This module and its submodules provide the basic functionality for a test
  server.  Since tests are generally small and short-lived, the main design
  goals for this server are extensibility and simplicity, rather than
  performance.  So this server will likely perform badly with large amounts of
  data.

  ## Writing tests

  The main part of each test is the test script, which describes how the test
  proceeds.  The test script can control the server through:

  - calling helper functions defined in this module such as `create_user`
  - calling helper functions defined in the `Polyjuice.ClientTest.Util.*`
    modules
  - calling `Polyjuice.Server.Protocols.*` functions (with the caveat that these
    functions usually rely on the parameters having already been validated, so
    passing them invalid data may crash the server)
  - querying and/or manipulating the server state directly by using the
    `get_and_update_server_state` function (with the caveat that doing so may
    break the server if it is not done with care)

  The script can also interact with the dashboard by using the
  `send_dashboard_event` or `send_message` functions to send messages.  The
  `send_dashboard_event` function allows you to send a custom event, while
  `send_message` is intended for sending normal message events, and uses
  [Fluent](https://projectfluent.org/) to translate strings.

  When a test script executes to the end, it is considered to have passed.  When
  a test fails, it should `raise` an error, preferably a
  `Polyjuice.ClientTest.TestError` so that the error message can be translated.

  The server will emit events in response to calls to certain endpoints; the
  test script can wait for these events using the `wait_until_event` function.

  As an example, the following script will create a user, inform the tester of
  the user ID and password, and wait until the user is logged in:

  ```
  def test_script(test_server) do
    {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

    BaseTestServer.send_message(test_server, Polyjuice.ClientTest.Fluent, "log-in-with-password",
      user: to_string(user),
      password: password
    )

    BaseTestServer.wait_until_event(test_server, &(match?({:login, _, _, _}, &1)))
  end
  ```

  For more complicated situations, the test can also have an event handler that
  is called on every event, and that can manipulate the test state (which by
  default is initially `%{}`).  The test script can then use `wait_until_state`
  to wait until the state satisfies the given condition.

  As an example, the following script will create a user and wait until the
  user is logged in twice:

  ```
  def test_script(test_server) do
    {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

    BaseTestServer.send_message(test_server, Polyjuice.ClientTest.Fluent, "log-in-with-password",
      user: to_string(user),
      password: password
    )

    BaseTestServer.wait_until_state(test_server, &(Map.get(&1, :login_count, 0) >= 2))
  end

  def event_handler({:login, _, _, _}, state, _server) do
    # count the number of login events that we see
    Map.update(state, :login_count, 1, &(&1 + 1))
  end

  def event_handler(_, state, _) do
    state
  end
  ```

  The server struct will be passed to the event handler as the third argument.
  This can be used to send dashboard messages or emit other events.  Not all
  server functions are safe to call within the event handler.

  ## Events

  FIXME: list events that can be emitted

  ## Customizing

  This server should be sufficient for most tests, but can be used as a basis
  for a more complicated server.  Most of the implementation is done in
  submodules, using `Polyjuice.Server.Protocols.Helper`, so a custom server
  could implement the functionality that it needs to re-implement, and re-use
  the implementation from the submodules for the other parts.  The functions in
  this module and in sub-modules assume certain things about the `GenServer`
  state.  They require the state to be a map with certain fields present, and
  the fields to have specific types; these are defined as the `required_state`
  type in this module and its submodules.  Usually if the field is a container
  type, initializing it to an empty container will be sufficient.  Otherwise,
  the type will have documentation describing any special conditions.  They may
  also use other fields, if they are present, for optional functionality.
  These are defined as the `optional_state_*` types in the submodules.  If
  these fields are present, they *must* be of the given type, and operate in
  the way that the implementation expects, otherwise things may not work
  properly.

  """

  use Polyjuice.Server.Protocols.Helper,
    delegates: [
      {Polyjuice.Server.Protocols.BaseClientServer,
       Polyjuice.ClientTest.BaseTestServer.BaseClientServer},
      {Polyjuice.Server.Protocols.AccountData, Polyjuice.ClientTest.BaseTestServer.AccountData},
      {Polyjuice.Server.Protocols.DeviceKey, Polyjuice.ClientTest.BaseTestServer.DeviceKey},
      {Polyjuice.Server.Protocols.Discovery, Polyjuice.ClientTest.BaseTestServer.Discovery},
      {Polyjuice.Server.Protocols.KeyBackup, Polyjuice.ClientTest.BaseTestServer.KeyBackup},
      {Polyjuice.Server.Protocols.PubSub, Polyjuice.ClientTest.BaseTestServer.PubSub},
      {Polyjuice.Server.Protocols.PushRule, Polyjuice.ClientTest.BaseTestServer.PushRule},
      {Polyjuice.Server.Protocols.Room, Polyjuice.ClientTest.BaseTestServer.Room},
      {Polyjuice.Server.Protocols.SendToDevice, Polyjuice.ClientTest.BaseTestServer.SendToDevice},
      {Polyjuice.Server.Protocols.User, Polyjuice.ClientTest.BaseTestServer.User}
    ]

  @type event_handler() :: (any, any, any -> any)

  @type plug() :: (Plug.Conn.t(), Plug.opts() -> Plug.Conn.t())

  @type wait_func() :: (any -> any)

  @typedoc """
  State properties required for the functions defined in this module.
  """
  @type required_state() :: %{
          server: t(),
          script_pid: pid,
          script_ref: reference,
          event_handlers: list(event_handler()),
          test_state: any,
          state_waiters: list({GenServer.from(), wait_func()}),
          event_waiters: list({GenServer.from(), wait_func()})
        }

  @typedoc false
  @type state() :: %{
          server: t(),
          script_pid: pid,
          script_ref: reference,
          event_handlers: list(event_handler()),
          test_state: any,
          state_waiters: list({GenServer.from(), wait_func()}),
          event_waiters: list({GenServer.from(), wait_func()}),
          custom_plug: plug(),
          custom: map(),
          # account data
          account_data: Polyjuice.ClientTest.BaseTestServer.AccountData.account_data_map(),
          account_data_stream:
            Polyjuice.ClientTest.BaseTestServer.AccountData.account_data_stream(),
          # device key
          device_keys: Polyjuice.ClientTest.BaseTestServer.DeviceKey.device_keys_map(),
          one_time_keys: Polyjuice.ClientTest.BaseTestServer.DeviceKey.one_time_keys_map(),
          # FIXME: add fallback keys
          cross_signing_keys:
            Polyjuice.ClientTest.BaseTestServer.DeviceKey.cross_signing_keys_map(),
          changed_keys_stream:
            Polyjuice.ClientTest.BaseTestServer.DeviceKey.changed_keys_stream(),
          # key backup
          key_backup: %{
            optional(String.t()) => %{
              latest: String.t(),
              versions: %{
                String.t() => %{
                  algorithm: String.t(),
                  auth_data: String.t(),
                  backup: %{
                    optional(String.t()) => %{
                      String.t() => Polyjuice.Server.KeyBackup.backup_data()
                    }
                  }
                }
              }
            }
          },
          # pub sub
          subscribers: %{optional(any) => Polyjuice.Server.Protocols.Subscriber.t()},
          sync_helper: pid,
          filters: %{optional(String.t()) => %{String.t() => map()}},
          # rooms
          room_event_stream: Polyjuice.ClientTest.BaseTestServer.Room.room_event_stream(),
          room_prev_event: Polyjuice.ClientTest.BaseTestServer.Room.room_prev_event_map(),
          room_state: Polyjuice.ClientTest.BaseTestServer.Room.room_state_map(),
          room_transactions: Polyjuice.ClientTest.BaseTestServer.Room.transactions_map(),
          rooms_by_user: Polyjuice.ClientTest.BaseTestServer.Room.rooms_by_user_map(),
          # send to device
          to_device_stream: Polyjuice.ClientTest.BaseTestServer.SendToDevice.to_device_stream(),
          to_device_transactions:
            Polyjuice.ClientTest.BaseTestServer.SendToDevice.transactions_set(),
          # user
          users: Polyjuice.ClientTest.BaseTestServer.User.user_map(),
          access_tokens: Polyjuice.ClientTest.BaseTestServer.User.access_token_map()
        }

  use TypedStruct

  typedstruct do
    field :dashboard_server, Polyjuice.ClientTest.Dash.t(), enforce: true
    field :user_id, String.t(), enforce: true
    field :test_id, String.t(), enforce: true
    field :servername, String.t(), enforce: true
    field :address, String.t(), enforce: true
    field :has_custom_plug, boolean, default: false
    field :pid, pid()
  end

  @doc false
  @spec new(
          dashboard_server :: Polyjuice.ClientTest.DashboardServer.t(),
          user_id :: String.t(),
          test_id :: String.t(),
          servername :: String.t(),
          script :: Polyjuice.ClientTest.TestServer.test_script(),
          opts :: Keyword.t()
        ) :: t()
  def new(
        %Polyjuice.ClientTest.DashboardServer{} = dashboard_server,
        user_id,
        test_id,
        servername,
        script,
        opts \\ []
      ) do
    server = %__MODULE__{
      dashboard_server: dashboard_server,
      user_id: user_id,
      test_id: test_id,
      servername: servername,
      address: URI.to_string(%URI{scheme: "https", host: servername}),
      has_custom_plug: Keyword.get(opts, :custom_plug) != nil
    }

    {:ok, pid} = GenServer.start(__MODULE__, [server, script | opts])
    %{server | pid: pid}
  end

  @impl GenServer
  @doc false
  def init([server, script | opts]) do
    server = %{server | pid: self()}

    if server.dashboard_server.pid do
      Task.start(fn ->
        Polyjuice.Server.Protocols.PubSub.subscribe(server.dashboard_server, server.pid, [
          server.test_id
        ])
      end)
    end

    event_handlers = Keyword.get(opts, :event_handlers, [])
    initial_state = Keyword.get(opts, :initial_state, %{})

    {:ok, sync_helper} = GenServer.start_link(Polyjuice.Server.SyncHelper, [])

    {:ok, script_pid} =
      Task.start(fn ->
        try do
          script.(server)
          # pause before exiting, so that the test server can handle events
          # that are being sent to it before it goes down
          Process.sleep(200)
        rescue
          x ->
            Process.sleep(200)
            Process.exit(self(), {:error, x})
        end
      end)

    script_ref = Process.monitor(script_pid)

    {:ok,
     %{
       server: server,
       script_pid: script_pid,
       script_ref: script_ref,
       event_handlers: event_handlers,
       test_state: initial_state,
       state_waiters: [],
       event_waiters: [],
       custom_plug: Keyword.get(opts, :custom_plug),
       custom: %{},
       # account data
       account_data: %{},
       account_data_stream: [],
       # device key
       device_keys: %{},
       one_time_keys: %{},
       cross_signing_keys: %{},
       changed_keys_stream: [],
       # key backup
       key_backup: %{},
       # pub sub
       sync_helper: sync_helper,
       subscribers: %{},
       filters: %{},
       # rooms
       room_event_stream: [],
       room_prev_event: %{},
       room_state: %{},
       room_transactions: %{},
       rooms_by_user: %{},
       # send to device
       to_device_stream: %{},
       to_device_transactions: %{},
       # user
       users: %{},
       access_tokens: %{}
     }}
  end

  @doc """
  Send an event to the dashboard.

  Usually, you should call a higher-level function such as `send_message/4` or
  `prompt/6` instead, but this can be used to send an event when those
  functions are not sufficient.
  """
  @spec send_dashboard_event(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          event :: Polyjuice.Util.event()
        ) :: {:ok, String.t()}
  def send_dashboard_event(server, event) do
    Polyjuice.ClientTest.DashboardServer.send_dashboard_event(
      server.dashboard_server,
      server.test_id,
      event
    )
  end

  @doc """
  Send an internationalizable message to be displayed in the dashboard.

  `module` indicates which module to load the message from, `identifier`
  indicates which message to use, and `fills` indicates how to fill the
  placeholders in the message.
  """
  @spec send_message(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_message(server, module, identifier, fills \\ []) do
    try do
      message =
        apply(module, :ftl, [identifier, fills])
        # bluntly drop BiDi isolate characters, because Fluent adds these and
        # they interfere with copy/paste
        |> String.replace(["\u2066", "\u2067", "\u2068", "\u2069"], "")

      event = %{
        "type" => "m.room.message",
        "content" => %{
          "body" => message,
          "msgtype" => "m.notice",
          "ca.uhoreg.polyjuice.clienttest.message" => %{
            "module" => to_string(module),
            "identifier" => identifier,
            "fills" => Map.new(fills)
          }
        }
      }

      send_dashboard_event(server, event)
    rescue
      _ ->
        fills_str =
          Enum.map(fills, fn {name, value} -> "#{name}: #{inspect(value)}" end) |> Enum.join(", ")

        event = %{
          "type" => "m.room.message",
          "content" => %{
            "body" => "Failed to create string: #{module}:#{identifier}, [#{fills_str}]",
            "msgtype" => "m.notice",
            "ca.uhoreg.polyjuice.clienttest.level" => "error"
          }
        }

        send_dashboard_event(server, event)
    end
  end

  @doc """
  Signal that a previously-sent instruction message has been performed.
  """
  @spec send_done(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          event_id :: String.t()
        ) :: {:ok, String.t()}
  def send_done(server, event_id) when is_binary(event_id) do
    event = %{
      "type" => "m.reaction",
      "content" => %{
        "m.relates_to" => %{
          "rel_type" => "m.annotation",
          "event_id" => event_id,
          "key" => "✅"
        }
      }
    }

    send_dashboard_event(server, event)
  end

  @spec send_log(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          level :: String.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  defp send_log(server, level, module, identifier, fills) do
    try do
      message =
        apply(module, :ftl, [identifier, fills])
        # bluntly drop BiDi isolate characters, because Fluent adds these and
        # they interfere with copy/paste
        |> String.replace(["\u2066", "\u2067", "\u2068", "\u2069"], "")

      event = %{
        "type" => "m.room.message",
        "content" => %{
          "body" => message,
          "msgtype" => "m.notice",
          "ca.uhoreg.polyjuice.clienttest.message" => %{
            "module" => to_string(module),
            "identifier" => identifier,
            "fills" => Map.new(fills)
          },
          "ca.uhoreg.polyjuice.clienttest.level" => level
        }
      }

      send_dashboard_event(server, event)
    rescue
      _ ->
        fills_str =
          Enum.map(fills, fn {name, value} -> "#{name}: #{inspect(value)}" end) |> Enum.join(", ")

        event = %{
          "type" => "m.room.message",
          "content" => %{
            "body" => "Failed to create string: #{module}:#{identifier}, [#{fills_str}]",
            "msgtype" => "m.notice",
            "ca.uhoreg.polyjuice.clienttest.level" => "error"
          }
        }

        send_dashboard_event(server, event)
    end
  end

  @doc """
  Send an internationalizable debug message to be displayed in the dashboard.

  See `send_message/4` for a description of the parameters
  """
  @spec send_debug(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_debug(server, module, identifier, fills \\ []) do
    send_log(server, "debug", module, identifier, fills)
  end

  @doc """
  Send an internationalizable info message to be displayed in the dashboard.

  See `send_message/4` for a description of the parameters
  """
  @spec send_info(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_info(server, module, identifier, fills \\ []) do
    send_log(server, "info", module, identifier, fills)
  end

  @doc """
  Send an internationalizable warning message to be displayed in the dashboard.

  See `send_message/4` for a description of the parameters
  """
  @spec send_warning(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_warning(server, module, identifier, fills \\ []) do
    send_log(server, "warning", module, identifier, fills)
  end

  @doc """
  Send an internationalizable error message to be displayed in the dashboard.

  See `send_message/4` for a description of the parameters
  """
  @spec send_error(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_error(server, module, identifier, fills \\ []) do
    send_log(server, "error", module, identifier, fills)
  end

  @doc """
  Send an internationalizable progress message to be displayed in the dashboard.

  See `send_message/4` for a description of the parameters
  """
  @spec send_progress(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t()
        ) :: {:ok, String.t()}
  def send_progress(server, module, identifier, fills \\ []) do
    send_log(server, "progress", module, identifier, fills)
  end

  @doc """
  Prompt the dashboard user.

  `module`, `identifier`, and `fills` are as in `send_message/4`.  `response`
  indicates the type of response to expect.  Options are:

  - `:integer` an integer (not yet implemented in the frontend)
  - `:string` any string (not yet implemented in the frontend)
  - `{:choice, choices}` allow the user to select from a list of choices, where
    `choices` is of the form `{module, identifier, fills}` or `{module, identifier}`
    with `module`, `identifier`, and `fills` having the same meaning as above.
    (The second form can be used when the message has no fills.)
  """
  @spec prompt(
          server :: Polyjuice.ClientTest.BaseTestServer.t(),
          module :: atom,
          identifier :: String.t(),
          fills :: Keyword.t(),
          response_type ::
            :integer
            | :string
            | {:choice, list({atom, {atom, String.t(), Keyword.t()} | {atom, String.t()}})},
          opts :: Keyword.t()
        ) :: {:ok, any} | {:error, any}
  def prompt(%{pid: pid}, module, identifier, fills, response_type, opts \\ [])
      when is_atom(module) and is_binary(identifier) and is_list(fills) and is_list(opts) do
    timeout = Keyword.get(opts, :timeout, 120_000)
    GenServer.call(pid, {:prompt, module, identifier, fills, response_type, opts}, timeout)
  end

  @doc """
  Create a user.

  If the username is a string, then it is treated as a local part for the user
  ID.  If it is `:aliceandbob`, then the username will be the next available
  name from a list of "standard" names (currently, 6 names are available).  If
  it is `:random`, then a the username will be a randomly generated string.

  If the password is a string, then the user will use that password.  If it is
  `nil`, then a randomly generated password will be created.  If it is
  `:no_login`, then the user cannot log in with a password.
  """
  @spec create_user(
          server :: map(),
          String.t() | :aliceandbob | :random,
          String.t() | :no_login | nil
        ) :: {String.t(), String.t() | :no_login}
  def create_user(%{pid: pid}, username \\ :aliceandbob, password \\ nil) do
    GenServer.call(pid, {:create_user, username, password})
  end

  @doc """
  Wait until the condition function returns a truthy value when called on the test state.

  Returns the value returned by the condition function.
  """
  @spec wait_until_state(server :: map(), (any -> any), non_neg_integer | :infinity) :: any
  def wait_until_state(%{pid: pid}, condition, timeout \\ 120_000) do
    GenServer.call(pid, {:wait_until_state, condition}, timeout)
  end

  @doc """
  Wait until an event is emitted that makes the condition function return a truthy value.

  Returns the value returned by the condition function.
  """
  @spec wait_until_event(server :: map(), (any -> any), non_neg_integer | :infinity) :: any
  def wait_until_event(%{pid: pid}, condition, timeout \\ 120_000) do
    GenServer.call(pid, {:wait_until_event, condition}, timeout)
  end

  @doc """
  Get and/or update the server state.

  The function will be called with the server's current state, and should
  return a tuple where the first element will be returned to the caller of this
  function, and the second element will be the server's new state.

  Use caution when calling this function, as you can break the server.  This
  should only be used when you cannot do what you want using other functions.
  It is purposeful that there are no functions for only getting or updating the
  server state, in order to make these operations less convenient since
  manipulating the server state directly should rarely be done.

  """
  @spec get_and_update_server_state(server :: map, (any -> {any, any})) :: any
  def get_and_update_server_state(%{pid: pid}, func) do
    GenServer.call(pid, {:get_and_update_server_state, func})
  end

  @doc """
  Get the test state.

  The function will be called on the test state, and the return value will be
  returned.  If no function is provided, the identity function will be used by
  default.  In other words, the entire state will be returned.
  """
  @spec get_test_state(server :: map(), (any -> any)) :: any
  def get_test_state(%{pid: pid}, func \\ & &1) do
    GenServer.call(pid, {:get_test_state, func})
  end

  @doc """
  Update the test state.

  The function will be called on the test state, and the return value will be
  use used as the new test state.
  """
  @spec update_test_state(server :: map(), (any -> any)) :: :ok
  def update_test_state(%{pid: pid}, func) do
    GenServer.call(pid, {:update_test_state, func})
  end

  @doc """
  Get and update the test state.

  The function will be called with the current test state, and should return a
  tuple where the first element will be returned to the caller of this
  function, and the second element will be the new test state.
  """
  @spec get_and_update_test_state(server :: map(), (any -> {any, any})) :: :ok
  def get_and_update_test_state(%{pid: pid}, func) do
    GenServer.call(pid, {:get_and_update_test_state, func})
  end

  @doc """
  Emit an event.
  """
  @spec emit_event(server :: map(), any) :: :ok
  def emit_event(%{pid: pid}, event) do
    GenServer.cast(pid, {:emit_event, event})
  end

  @doc false
  # emits an event
  def emit(event, state) do
    new_test_state =
      Enum.reduce(
        state.event_handlers,
        state.test_state,
        &apply(&1, [event, &2, state.server])
      )

    new_state_waiters =
      Enum.filter(state.state_waiters, fn {from, f} ->
        res = f.(new_test_state)

        if res do
          GenServer.reply(from, res)
          false
        else
          true
        end
      end)

    new_event_waiters =
      Enum.filter(state.event_waiters, fn {from, f} ->
        res = f.(event)

        if res do
          GenServer.reply(from, res)
          false
        else
          true
        end
      end)

    %{
      state
      | test_state: new_test_state,
        state_waiters: new_state_waiters,
        event_waiters: new_event_waiters
    }
  end

  @doc false
  def handle_call_other({:create_user, username, password}, _from, state) do
    servername = state.server.servername

    ok_user_id =
      case username do
        _ when is_binary(username) ->
          Polyjuice.Util.Identifiers.V1.UserIdentifier.new(username, servername)

        :aliceandbob ->
          case Enum.find_value(
                 ["alice", "bob", "carol", "dan", "erin", "frank"],
                 fn u ->
                   {:ok, user_id} =
                     Polyjuice.Util.Identifiers.V1.UserIdentifier.new(u, servername)

                   if not Map.has_key?(state.users, to_string(user_id)), do: user_id, else: nil
                 end
               ) do
            nil -> :error
            x -> {:ok, x}
          end

        :random ->
          Polyjuice.ClientTest.DashboardServer.make_user_id(state.users, servername)
      end

    with {:ok, user_id} <- ok_user_id do
      user_id = to_string(user_id)

      password =
        cond do
          is_binary(password) -> password
          password == :no_login -> password
          true -> Polyjuice.Util.Randomizer.randomize(10)
        end

      newstate = %{
        state
        | users: Map.put(state.users, user_id, %{devices: %{}, password: password})
      }

      {:reply, {user_id, password}, newstate}
    else
      _ -> {:reply, :error, state}
    end
  end

  def handle_call_other({:wait_until_state, func}, from, state) do
    res = func.(state.test_state)

    if res do
      {:reply, res, state}
    else
      {:noreply, %{state | state_waiters: [{from, func} | state.state_waiters]}}
    end
  end

  def handle_call_other({:wait_until_event, func}, from, state) do
    {:noreply, %{state | event_waiters: [{from, func} | state.event_waiters]}}
  end

  def handle_call_other({:get_and_update_server_state, func}, _from, state) do
    {result, state} = func.(state)
    {:reply, result, state}
  end

  def handle_call_other({:get_test_state, func}, _from, state) do
    {:reply, func.(state.test_state), state}
  end

  def handle_call_other({:update_test_state, func}, _from, state) do
    new_test_state = func.(state.test_state)
    {:reply, :ok, %{state | test_state: new_test_state}}
  end

  def handle_call_other({:get_and_update_test_state, func}, _from, state) do
    {result, new_test_state} = func.(state.test_state)
    {:reply, result, %{state | test_state: new_test_state}}
  end

  def handle_call_other({:prompt, module, identifier, fills, response_type, opts}, from, state) do
    try do
      message = apply(module, :ftl, [identifier, fills])

      response =
        case response_type do
          :integer ->
            %{"type" => "integer"}

          :string ->
            %{"type" => "string"}

          {:choice, choices} ->
            %{
              "type" => "choice",
              "choices" =>
                Enum.map(choices, fn {name, text} ->
                  text =
                    case text do
                      {module, identifier, fills} ->
                        apply(module, :ftl, [identifier, fills])

                      {module, identifier} ->
                        apply(module, :ftl, [identifier, []])
                    end

                  %{"response" => to_string(name), "text" => text}
                end)
            }
        end

      event = %{
        "type" => "m.room.message",
        "content" => %{
          "body" => message,
          "msgtype" => "m.notice",
          "ca.uhoreg.polyjuice.clienttest.message" => %{
            "module" => to_string(module),
            "identifier" => identifier,
            "fills" => Map.new(fills)
          },
          "ca.uhoreg.polyjuice.clienttest.response" => response
        }
      }

      {:ok, event_id} = send_dashboard_event(state.server, event)

      func = fn
        {:user_event, "ca.uhoreg.polyjuice.clienttest.response", nil,
         %{"body" => response, "m.relates_to" => %{"event_id" => ^event_id}}} ->
          send_done(state.server, event_id)

          case response_type do
            :integer ->
              case Integer.parse(response) do
                {int, ""} -> {:ok, int}
                _ -> {:error, :invalid_number}
              end

            :string ->
              {:ok, response}

            {:choice, choices} ->
              try do
                response = String.to_existing_atom(response)

                Enum.find_value(choices, {:error, :invalid_choice}, fn {name, _} ->
                  if name == response, do: {:ok, name}
                end)
              rescue
                _ -> {:error, :invalid_choice}
              end
          end

        _ ->
          false
      end

      {:noreply, %{state | event_waiters: [{from, func} | state.state_waiters]}}
    rescue
      _ ->
        fills_str =
          Enum.map(fills, fn {name, value} -> "#{name}: #{inspect(value)}" end) |> Enum.join(", ")

        event = %{
          "type" => "m.room.message",
          "content" => %{
            "body" => "Failed to create string: #{module}:#{identifier}, [#{fills_str}]",
            "msgtype" => "m.notice",
            "ca.uhoreg.polyjuice.clienttest.level" => "error"
          }
        }

        send_dashboard_event(state.server, event)

        {:reply, {:error, :create_string}, state}
    end
  end

  def handle_call_other({:call_plug, conn, opts}, _from, state) do
    conn = put_in(conn.assigns[:polyjuice_client_test_server_state], state)

    conn =
      try do
        state.custom_plug.(conn, opts)
      rescue
        x ->
          case x do
            %{matrix_error: err, plug_status: status} ->
              conn
              |> Plug.Conn.put_resp_content_type("application/json")
              |> Plug.Conn.send_resp(status, Jason.encode!(err))

            _ ->
              IO.puts("Internal error: #{inspect(x)}")

              conn
              |> Plug.Conn.put_resp_content_type("application/json")
              |> Plug.Conn.send_resp(500, ~s({"errcode":"M_UNKNOWN","error":"Unknown error"}))
          end
          |> Plug.Conn.halt()
      end

    state = conn.assigns[:polyjuice_client_test_server_state]
    conn = update_in(conn.assigns, &Map.delete(&1, :polyjuice_client_test_server_state))
    {:reply, conn, state}
  end

  @impl GenServer
  @doc false
  def handle_cast({:exit, reason}, state) do
    Process.exit(state.script_pid, reason)
    {:stop, reason, state}
  end

  def handle_cast({:emit_event, event}, state) do
    state = emit(event, state)
    {:noreply, state}
  end

  @impl GenServer
  @doc false
  def handle_info({:DOWN, ref, _, _, reason}, state) do
    if ref == state.script_ref do
      {:stop, reason, state}
    else
      {:noreply, state}
    end
  end

  def handle_info({:notify, queue, data}, state) do
    server = state.server

    if queue == server.test_id do
      room_id = "!#{server.test_id}:#{server.dashboard_server.servername}"
      test_user = server.user_id

      state =
        case data do
          {:room_events, _, ^room_id, events} ->
            Enum.reduce(events, state, fn
              %{"sender" => ^test_user, "type" => type, "content" => content}, state ->
                emit({:user_event, type, Map.get(state, "state_key", nil), content}, state)

              _, state ->
                state
            end)

          _ ->
            state
        end

      {:noreply, state}
    else
      {:noreply, state}
    end
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defimpl Polyjuice.ClientTest.TestServer do
    def monitor(%{pid: pid}) do
      Process.monitor(pid)
    end

    def exit(%{pid: pid}, reason) do
      GenServer.cast(pid, {:exit, reason})
    end
  end

  defimpl Polyjuice.ClientTest.OverrideEndPoints do
    def call(%{pid: pid, has_custom_plug: override}, conn, opts) do
      conn =
        if override do
          GenServer.call(pid, {:call_plug, conn, opts})
        else
          conn
        end

      conn =
        if conn.state == :unset and conn.path_info == ["_matrix", "client", "versions"] do
          # pretend we support some things
          conn
          |> Polyjuice.Server.Plug.Client.cors([])
          |> Plug.Conn.put_resp_content_type("application/json")
          |> Plug.Conn.put_resp_header("cache-control", "max-age=3600, public")
          |> Plug.Conn.send_resp(
            200,
            Jason.encode!(%{
              "versions" => [
                "r0.0.1",
                "r0.1.0",
                "r0.2.0",
                "r0.3.0",
                "r0.4.0",
                "r0.5.0",
                "r0.6.0",
                "r0.6.1",
                "v1.1",
                "v1.2",
                "v1.3"
              ]
            })
          )
          |> Plug.Conn.halt()
        else
          conn
        end

      conn
    end
  end
end
