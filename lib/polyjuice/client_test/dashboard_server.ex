# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.DashboardServer do
  @moduledoc false

  use Polyjuice.Server.Protocols.Helper,
    delegates: [
      {Polyjuice.Server.Protocols.Room, Polyjuice.ClientTest.DashboardServer.Room},
      {Polyjuice.Server.Protocols.PubSub, Polyjuice.ClientTest.DashboardServer.PubSub}
    ]

  use TypedStruct

  typedstruct do
    field :servername, String.t(), enforce: true
    field :address, String.t(), enforce: true
    field :test_domain, String.t(), enforce: true
    field :pid, pid()
    field :test_names, list(String.t())
    field :test_data, %{optional(String.t()) => map()}
  end

  @impl GenServer
  def init([servername, tests | opts]) do
    all_atoms =
      for i <- 0..(:erlang.system_info(:atom_count) - 1),
          do: :erlang.binary_to_term(<<131, 75, i::24>>)

    tests =
      Enum.map(tests, fn module ->
        cond do
          is_atom(module) ->
            [module]

          is_binary(module) and String.ends_with?(module, ".*") ->
            base = "Elixir." <> String.slice(module, 0, String.length(module) - 2)

            Enum.filter(all_atoms, fn m ->
              if Atom.to_string(m) |> String.starts_with?(base) do
                Code.ensure_loaded(m)
                function_exported?(m, :test_script, 1)
              else
                false
              end
            end)
        end
      end)
      |> Enum.concat()

    test_data =
      Enum.map(tests, fn mod ->
        name = Atom.to_string(mod)

        data = %{
          module: mod
        }

        data =
          case Code.fetch_docs(mod) do
            {:docs_v1, _, _, _, %{"en" => module_doc}, _, _} ->
              case String.split(module_doc, "\n\n", parts: 2) do
                [name, topic] ->
                  topic =
                    String.split(topic, "\n\n")
                    |> Enum.map(&String.replace(&1, "\n", " "))
                    |> Enum.join("\n")

                  Map.merge(data, %{name: name, topic: topic})

                [name] ->
                  Map.put(data, :name, name)
              end

            _ ->
              name =
                Atom.to_string(mod)
                |> String.split(".")
                |> List.last()
                |> String.split(~r/\p{Lu}/, include_captures: true)
                |> Enum.drop(1)
                |> Enum.chunk_every(2)
                |> Enum.map(&Enum.join/1)
                |> Enum.join(" ")
                |> String.capitalize()

              Map.put(data, :name, name)
          end

        {name, data}
      end)
      |> Map.new()

    test_names = Map.keys(test_data)

    {:ok, sync_helper} = GenServer.start_link(Polyjuice.Server.SyncHelper, [])

    {:ok,
     %{
       server: %__MODULE__{
         servername: servername,
         address:
           Keyword.get_lazy(opts, :address, fn ->
             URI.to_string(%URI{scheme: "https", host: servername})
           end),
         test_domain: Keyword.get(opts, :test_domain, servername),
         pid: self(),
         test_names: test_names,
         test_data: test_data
       },
       ets: :ets.new(:polyjuice_client_test_dashboard, [:private]),
       users: %{},
       test_servers: %{},
       sync_helper: sync_helper,
       subscribers: %{},
       monitors: %{}
     }}
  end

  def make_user_id(users, servername) do
    Polyjuice.ClientTest.Util.make_random_key(users, fn ->
      Polyjuice.Util.Identifiers.V1.UserIdentifier.generate(servername)
    end)
  end

  def get_server_for_host(pid, host) when is_binary(host) do
    GenServer.call(pid, {:get_server_for_host, host})
  end

  def send_dashboard_event(%__MODULE__{pid: pid}, test_id, event)
      when is_binary(test_id) and is_map(event) do
    GenServer.call(pid, {:send_dashboard_event, test_id, event})
  end

  def end_test(%__MODULE__{pid: pid}, test_id, result)
      when is_binary(test_id) and is_atom(result) do
    GenServer.cast(pid, {:end_test, test_id, result})
  end

  @doc false
  def _make_room_events(events, parsed_room_id, state, prev_event, prev_event_hash) do
    room_id = to_string(parsed_room_id)
    time = :erlang.system_time(:millisecond)

    Enum.map_reduce(
      events,
      {state, prev_event, prev_event_hash},
      fn e, {state, prev_event, prev_event_hash} ->
        e = Map.put(e, "room_id", room_id)
        e = Map.put(e, "origin_server_ts", time)

        e =
          case prev_event do
            nil -> e
            _ -> Map.put(e, "prev_events", [prev_event, %{"sha256" => prev_event_hash}])
          end

        user_id = Map.fetch!(e, "sender")
        member_state_key = {"m.room.member", user_id}

        auth_events =
          [
            case state do
              %{{"m.room.create", ""} => {event_id, hash}} -> [event_id, %{"sha256" => hash}]
              _ -> []
            end,
            case state do
              %{{"m.room.join_rules", ""} => {event_id, hash}} ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end,
            case state do
              %{{"m.room.power_levels", ""} => {event_id, hash}} ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end,
            case state do
              %{^member_state_key => {event_id, hash}} -> [event_id, %{"sha256" => hash}]
              _ -> []
            end
          ]
          |> Enum.concat()

        e = Map.put(e, "auth_events", auth_events)

        {:ok, content_hash} = Polyjuice.Util.RoomVersion.compute_content_hash("5", e)
        e = Map.put(e, "hashes", %{"sha256" => Base.encode64(content_hash, padding: false)})
        {:ok, reference_hash} = Polyjuice.Util.RoomVersion.compute_reference_hash("5", e)

        reference_hash = Base.encode64(reference_hash, padding: false)

        event_id =
          "$" <>
            String.replace(
              reference_hash,
              ["+", "/"],
              fn
                "+" -> "-"
                "/" -> "_"
              end
            )

        e = Map.put(e, "event_id", event_id)

        state =
          case e do
            %{"type" => type, "state_key" => state_key} ->
              Map.put(state, {type, state_key}, {event_id, reference_hash})

            _ ->
              state
          end

        {e, {state, event_id, reference_hash}}
      end
    )
  end

  @doc false
  # Internal function for sending events to a room.  This function assumes that
  # permissions have already been checked and that the events are well-formed.
  def _send_events(state, parsed_room_id, events) do
    with [{_, {room_state, prev_event, prev_event_hash}}] <-
           :ets.lookup(state.ets, {:room_last_state, parsed_room_id.opaque_id}),
         {:ok, {_test, parsed_user_id}} <-
           Map.fetch(state.test_servers, parsed_room_id.opaque_id),
         {:ok, %{sync_token: sync_token} = user_info} <-
           Map.fetch(state.users, parsed_user_id) do
      {events, last_state} =
        Polyjuice.ClientTest.Util.make_room_events_v4(
          events,
          parsed_room_id,
          room_state,
          prev_event,
          prev_event_hash
        )

      :ets.insert(state.ets, {{:room, parsed_room_id.opaque_id, sync_token + 1}, events})
      :ets.insert(state.ets, {{:room_last_state, parsed_room_id.opaque_id}, last_state})

      Polyjuice.ClientTest.DashboardServer.PubSub.publish(
        {{:room_events, sync_token + 1, to_string(parsed_room_id), events},
         [to_string(parsed_user_id), parsed_room_id.opaque_id]},
        nil,
        state
      )

      {Enum.map(events, &Map.get(&1, "event_id")),
       %{
         state
         | users: Map.put(state.users, parsed_user_id, %{user_info | sync_token: sync_token + 1})
       }}
    else
      _ -> :error
    end
  end

  def handle_call_other({:get_server_for_host, host}, _from, state) do
    if host == state.server.servername or host == "" or
         String.split(state.server.servername, ":", parts: 2) |> Enum.at(0) == host do
      {:reply, state.server, state}
    else
      [test_id | _] = String.split(host, ".", parts: 2)

      case Map.get(state.test_servers, test_id) do
        {server, _} ->
          {:reply, server, state}

        nil ->
          {:reply, nil, state}
      end
    end
  end

  def handle_call_other({:get_user_from_access_token, access_token}, _from, state) do
    with {:ok, user_id} <-
           Polyjuice.Util.Identifiers.V1.UserIdentifier.new(
             access_token,
             state.server.servername
           ),
         true <- Map.has_key?(state.users, user_id) do
      {:reply,
       {:user, Polyjuice.Util.Identifiers.V1.UserIdentifier.fqid(user_id), access_token, false},
       state}
    else
      _ -> {:reply, nil, state}
    end
  end

  def handle_call_other(:create_user, _from, state) do
    user_id = make_user_id(state.users, state.server.servername)

    new_users =
      Map.put(state.users, user_id, %{
        sync_token: 0,
        rooms: MapSet.new()
      })

    {:reply, user_id, %{state | users: new_users}}
  end

  def handle_call_other({:log_out, user_id}, _from, state) do
    with {:ok, parsed} <- Polyjuice.Util.Identifiers.V1.UserIdentifier.new(user_id) do
      # FIXME: also delete test servers
      new_users = Map.delete(state.users, parsed)
      {:reply, :ok, %{state | users: new_users}}
    else
      _ -> {:reply, :ok, state}
    end
  end

  def handle_call_other({:send_dashboard_event, test_id, event}, _from, state) do
    servername = state.server.servername
    {:ok, parsed_room_id} = Polyjuice.Util.Identifiers.V1.RoomIdentifier.new(test_id, servername)
    room_id = Polyjuice.Util.Identifiers.V1.RoomIdentifier.fqid(parsed_room_id)
    event = Map.merge(event, %{"sender" => "@test:#{servername}", "room_id" => room_id})

    case Polyjuice.ClientTest.DashboardServer._send_events(state, parsed_room_id, [event]) do
      {[event_id], state} -> {:reply, {:ok, event_id}, state}
      :error -> {:reply, :error, state}
    end
  end

  @impl GenServer
  @doc false
  def handle_cast({:end_test, test_id, event}, state) do
    # FIXME:
    {:noreply, state}
  end

  @impl GenServer
  @doc false
  def handle_info({:DOWN, ref, _, _, reason}, state) do
    case Map.fetch(state.monitors, ref) do
      :error ->
        {:noreply, state}

      {:ok, test_id} ->
        {_, parsed_user_id} = Map.get(state.test_servers, test_id)
        servername = state.server.servername
        test_user = "@test:" <> servername

        {:ok, parsed_room_id} =
          Polyjuice.Util.Identifiers.V1.RoomIdentifier.new(test_id, servername)

        room_id = Polyjuice.Util.Identifiers.V1.RoomIdentifier.fqid(parsed_room_id)

        member_event = %{
          "type" => "m.room.member",
          "state_key" => to_string(parsed_user_id),
          "sender" => test_user,
          "room_id" => room_id
        }

        events =
          case reason do
            :normal ->
              [Map.put(member_event, "content", %{"membership" => "leave", "reason" => "passed"})]

            :cancelled ->
              [
                Map.put(member_event, "content", %{
                  "membership" => "leave",
                  "reason" => "cancelled"
                })
              ]

            {:error, %Polyjuice.ClientTest.TestError{module: nil}} ->
              [
                Map.put(member_event, "content", %{
                  "membership" => "leave",
                  "reason" => "failed"
                })
              ]

            {:error, %Polyjuice.ClientTest.TestError{} = err} ->
              msg = apply(err.module, :ftl, [err.identifier, err.fills])

              [
                %{
                  "type" => "m.room.message",
                  "sender" => test_user,
                  "content" => %{
                    "body" => msg,
                    "msgtype" => "m.notice",
                    "ca.uhoreg.polyjuice.clienttest.level" => "error",
                    "ca.uhoreg.polyjuice.clienttest.message" => %{
                      "module" => to_string(err.module),
                      "identifier" => err.identifier,
                      "fills" => Map.new(err.fills)
                    }
                  },
                  "room_id" => room_id
                },
                Map.put(member_event, "content", %{"membership" => "leave", "reason" => "failed"})
              ]

            {:error, msg} when is_binary(msg) ->
              [
                %{
                  "type" => "m.room.message",
                  "sender" => test_user,
                  "content" => %{
                    "body" => msg,
                    "msgtype" => "m.notice",
                    "ca.uhoreg.polyjuice.clienttest.level" => "error"
                  },
                  "room_id" => room_id
                },
                Map.put(member_event, "content", %{"membership" => "leave", "reason" => "failed"})
              ]

            {:error, %{message: msg}} when is_binary(msg) ->
              [
                %{
                  "type" => "m.room.message",
                  "sender" => test_user,
                  "content" => %{
                    "body" => msg,
                    "msgtype" => "m.notice",
                    "ca.uhoreg.polyjuice.clienttest.level" => "error"
                  },
                  "room_id" => room_id
                },
                Map.put(member_event, "content", %{"membership" => "leave", "reason" => "failed"})
              ]

            {:timeout, _} ->
              [
                %{
                  "type" => "m.room.message",
                  "sender" => test_user,
                  "content" => %{
                    "body" => "Timed out",
                    "msgtype" => "m.notice",
                    "ca.uhoreg.polyjuice.clienttest.level" => "error"
                  },
                  "room_id" => room_id
                },
                Map.put(member_event, "content", %{"membership" => "leave", "reason" => "failed"})
              ]

            x ->
              [
                %{
                  "type" => "m.room.message",
                  "sender" => test_user,
                  "content" => %{
                    "body" => "Internal server error",
                    "msgtype" => "m.notice",
                    "ca.uhoreg.polyjuice.clienttest.level" => "error",
                    "ca.uhoreg.polyjuice.clienttest.details" => inspect(x)
                  },
                  "room_id" => room_id
                },
                Map.put(member_event, "content", %{"membership" => "leave", "reason" => "failed"})
              ]
          end

        {_, state} =
          Polyjuice.ClientTest.DashboardServer._send_events(state, parsed_room_id, events)

        %{rooms: user_tests} = user_info = Map.get(state.users, parsed_user_id)

        new_users =
          Map.put(state.users, parsed_user_id, %{
            user_info
            | rooms: MapSet.delete(user_tests, room_id)
          })

        new_test_servers = Map.delete(state.test_servers, test_id)
        new_monitors = Map.delete(state.monitors, ref)

        {:noreply,
         %{state | users: new_users, test_servers: new_test_servers, monitors: new_monitors}}
    end
  end

  def handle_info(_, state) do
    {:noreply, state}
  end

  defimpl Polyjuice.Server.Protocols.BaseClientServer do
    def get_server_name(%{servername: servername}) do
      servername
    end

    def user_from_access_token(%{servername: servername, pid: pid}, access_token) do
      GenServer.call(pid, {:get_user_from_access_token, access_token})
    end

    def user_in_appservice_namespace?(_, _, _), do: false
  end

  defimpl Polyjuice.Server.Protocols.Discovery do
    def address(%{address: address}) do
      address
    end

    def identity_server(_), do: nil
    def extra_fields(_), do: nil
    def delegated_hostname(_), do: nil
  end

  defimpl Polyjuice.Server.Protocols.RoomDirectory do
    def public_rooms(
          %{servername: servername, test_data: test_data},
          _source,
          third_party_instance_id,
          filter,
          since,
          limit
        )
        when third_party_instance_id == nil or third_party_instance_id == :all do
      filter_fn =
        case filter do
          %{"generic_search_term" => term} when is_binary(term) ->
            term = String.trim(term)

            if term == "" do
              &Function.identity/1
            else
              terms = String.split(term)

              fn test_data ->
                Enum.filter(test_data, fn {_test_name, data} ->
                  case data do
                    %{name: name, topic: topic} ->
                      Enum.all?(
                        terms,
                        &(String.contains?(name, &1) or String.contains?(topic, &1))
                      )

                    %{name: name} ->
                      Enum.all?(terms, &String.contains?(name, &1))
                  end
                end)
              end
            end

          _ ->
            &Function.identity/1
        end

      rooms =
        test_data
        |> filter_fn.()
        |> Enum.map(fn {test_name, data} ->
          base = %{
            "guest_can_join" => false,
            "num_joined_members" => 0,
            "room_id" => "!#{test_name}:#{servername}",
            "word_readable" => false
          }

          case data do
            %{name: name, topic: topic} ->
              Map.merge(base, %{"name" => name, "topic" => topic})

            %{name: name} ->
              Map.put(base, "name", name)
          end
        end)
        |> Enum.sort_by(fn elem -> elem["name"] end)

      %{"chunk" => rooms, "total_room_count_estimate" => Enum.count(rooms)}
    end

    def public_rooms(_, _, _, _, _, _) do
      %{"chunk" => [], "total_room_count_estimate" => 0}
    end
  end

  defimpl Polyjuice.Server.Protocols.User do
    def login_flows(_) do
      [%{"type" => "m.login.password"}]
    end

    def log_in(%{pid: pid, servername: servername}, credentials, _) do
      case credentials do
        {:password, "@test:" <> ^servername, "test"} ->
          user_id = GenServer.call(pid, :create_user)
          localpart = user_id.localpart

          {:ok,
           %{
             access_token: localpart,
             device_id: localpart,
             username: localpart
           }}
      end
    end

    def log_out(%{pid: pid}, user_id, _) do
      GenServer.call(pid, {:log_out, user_id})
      :ok
    end

    def get_user_devices(%{pid: pid}, user_id) do
      with {:ok, parsed} <- Polyjuice.Util.Identifiers.V1.UserIdentifier.new(user_id),
           {:user, _, _, _} <-
             GenServer.call(pid, {:get_user_from_access_token, parsed.localpart}) do
        [parsed.localpart]
      else
        _ -> []
      end
    end

    def register(_, _, _, _, _, _) do
      raise Polyjuice.Server.Plug.MatrixError,
        message: "Forbidden",
        plug_status: 403,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
    end

    def change_password(_, _, _) do
      raise Polyjuice.Server.Plug.MatrixError,
        message: "Forbidden",
        plug_status: 403,
        matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
    end

    def deactivate(%{pid: pid}, user_id) do
      GenServer.call(pid, {:log_out, user_id})
      :ok
    end

    def user_interactive_auth(_, _, _, _, _, _) do
      {:ok,
       %Polyjuice.Server.Plug.MatrixError{
         message: "Forbidden",
         plug_status: 403,
         matrix_error: Polyjuice.Util.ClientAPIErrors.MForbidden.new()
       }}
    end
  end
end
