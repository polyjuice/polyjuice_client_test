# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.Util do
  @moduledoc """
  Utility functions, mostly aimed at writing test servers.
  """

  @doc """
  Make a random key that is not already a key in the given map.

  `gen_random` is a function that will return a random key, and `tries` is the
  maximum number of times to try to generate a key before giving up.
  """
  @spec make_random_key(map(), (() -> term), non_neg_integer) :: map()
  def make_random_key(
        map,
        gen_random \\ fn -> Polyjuice.Util.Randomizer.randomize(10) end,
        tries \\ 10
      ) do
    if tries <= 0 do
      raise "Generating random key failed"
    else
      key = gen_random.()

      if Map.has_key?(map, key) do
        make_random_key(map, gen_random, tries - 1)
      else
        key
      end
    end
  end

  @doc """
  Make room events for a v4 or higher room.

  This function adds the room ID and timestamp, and calculates the event hashes
  and ID.

  Returns `{events, state, prev_event, prev_event_hash}`, where `events` is the
  list of events, `state` is the resulting room state, and `prev_event` and
  `prev_event_hash` are the values to be passed to subsequent calls to this
  function for the same `room_id`.
  """
  @spec make_room_events_v4(
          list(Polyjuice.Util.event()),
          String.Chars.t(),
          %{optional({String.t(), String.t()}) => Polyjuice.Util.event()},
          String.t() | nil,
          String.t() | nil
        ) ::
          {list(Polyjuice.Util.event()),
           {%{optional({String.t(), String.t()}) => Polyjuice.Util.event()}, String.t(),
            String.t()}}
  def make_room_events_v4(events, room_id, state, prev_event, prev_event_hash) do
    room_id = to_string(room_id)
    time = :erlang.system_time(:millisecond)

    Enum.map_reduce(
      events,
      {state, prev_event, prev_event_hash},
      fn e, {state, prev_event, prev_event_hash} ->
        e = Map.put(e, "room_id", room_id)
        e = Map.put(e, "origin_server_ts", time)

        e =
          case prev_event do
            nil -> e
            _ -> Map.put(e, "prev_events", [prev_event, %{"sha256" => prev_event_hash}])
          end

        user_id = Map.fetch!(e, "sender")
        member_state_key = {"m.room.member", user_id}

        auth_events =
          [
            case state do
              %{
                {"m.room.create", ""} => %{
                  "event_id" => event_id,
                  "hashes" => %{"sha256" => hash}
                }
              } ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end,
            case state do
              %{
                {"m.room.join_rules", ""} => %{
                  "event_id" => event_id,
                  "hashes" => %{"sha256" => hash}
                }
              } ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end,
            case state do
              %{
                {"m.room.power_levels", ""} => %{
                  "event_id" => event_id,
                  "hashes" => %{"sha256" => hash}
                }
              } ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end,
            case state do
              %{^member_state_key => %{"event_id" => event_id, "hashes" => %{"sha256" => hash}}} ->
                [event_id, %{"sha256" => hash}]

              _ ->
                []
            end
          ]
          |> Enum.concat()

        e = Map.put(e, "auth_events", auth_events)

        {:ok, content_hash} = Polyjuice.Util.RoomVersion.compute_content_hash("5", e)
        e = Map.put(e, "hashes", %{"sha256" => Base.encode64(content_hash, padding: false)})
        {:ok, reference_hash} = Polyjuice.Util.RoomVersion.compute_reference_hash("5", e)

        reference_hash = Base.encode64(reference_hash, padding: false)

        event_id =
          "$" <>
            String.replace(
              reference_hash,
              ["+", "/"],
              fn
                "+" -> "-"
                "/" -> "_"
              end
            )

        e = Map.put(e, "event_id", event_id)

        {e, state} =
          case e do
            %{"type" => type, "state_key" => state_key} ->
              e =
                case Map.fetch(state, {type, state_key}) do
                  {:ok, %{"content" => prev_content, "sender" => prev_sender}} ->
                    Map.get(e, "unsigned", %{})
                    |> Map.put("prev_content", prev_content)
                    |> Map.put("prev_sender", prev_sender)
                    |> (&Map.put(e, "unsigned", &1)).()

                  _ ->
                    e
                end

              {e, Map.put(state, {type, state_key}, e)}

            _ ->
              {e, state}
          end

        {e, {state, event_id, reference_hash}}
      end
    )
  end
end
