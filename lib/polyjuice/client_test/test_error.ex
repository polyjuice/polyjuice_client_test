# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.ClientTest.TestError do
  @type t() :: %__MODULE__{
          module: atom,
          identifier: String.t(),
          fills: Keyword.t()
        }

  @enforce_keys [:module, :identifier]
  defexception [
    :module,
    :identifier,
    fills: []
  ]

  def message(%__MODULE__{module: module, identifier: identifier, fills: fills}) do
    apply(module, :ftl, [identifier, fills])
  end
end
