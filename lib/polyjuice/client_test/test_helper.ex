# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.TestHelper do
  @moduledoc """
  Reduce some of the boilerplate in writing tests

  This can be used by adding `use Polyjuice.ClientTest.TestHelper` to the test
  module.  It will create a test based on
  `Polyjuice.ClientTest.BaseTestServer`.  Then, define a `test_script/1`
  function, which contains the test script (see the
  `Polyjuice.ClientTest.BaseTestServer` documentation for information on how to
  write the `test_script`.).

  You can also pass arguments to the `use Polyjuice.ClientTest.TestHelper`
  macro.  Supported options are:
  - `event_handlers:` a list of event handlers. e.g.
    `[&Polyjuice.ClientTest.Util.Encryption.olm_event_handler/3]`
  - `fluent:` `true` if the test has its own strings that should be localized
    (for) sending dashboard messages.  To use these strings, create a
    `priv/fluent/Elixir.[TestModuleName]/en/messages.ftl` file with the
    string definitions using the [Fluent
    syntax](https://projectfluent.org/fluent/guide/).

  """

  defmacro __using__(opts) do
    server_function =
      Keyword.get(opts, :server_function, &Polyjuice.ClientTest.BaseTestServer.new/6)

    server_opts = Keyword.get(opts, :server_opts, [])

    event_handlers = Keyword.get(opts, :event_handlers, [])
    fluent = Keyword.get(opts, :fluent, false)

    quote do
      @behaviour Polyjuice.ClientTest.TestHelper

      import Polyjuice.ClientTest.Assertions

      @doc false
      def new(dashboard_server, user_id, test_id, host) do
        unquote(server_function).(
          dashboard_server,
          user_id,
          test_id,
          host,
          &test_script/1,
          [{:event_handlers, unquote(event_handlers)} | unquote(server_opts)]
        )
      end

      if unquote(fluent) do
        modname = Module.concat(__MODULE__, "Fluent")
        privname = "fluent/#{__MODULE__}"

        defmodule modname do
          @moduledoc false
          use Fluent.Assembly,
            otp_app:
              if(unquote(fluent) == true, do: :polyjuice_client_test, else: unquote(fluent)),
            priv: privname,
            default_locale: "en"
        end
      end
    end
  end

  @doc """
  The test script.
  """
  @callback test_script(server :: Polyjuice.ClientTest.BaseTestServer) :: any
end
