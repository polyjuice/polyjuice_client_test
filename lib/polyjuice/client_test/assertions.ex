# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.Assertions do
  @doc """
  Assert that a condition is true.

  If the condition is not true, a `Polyjuice.ClientTest.TestError` will be raised.

  Known options are `module`, `identifier`, and `fills`.
  """
  @spec assert(boolean, Keyword.t()) :: true
  def assert(assertion, opts \\ []) do
    unless assertion do
      case {Keyword.fetch(opts, :module), Keyword.fetch(opts, :identifier)} do
        {{:ok, module}, {:ok, identifier}} when is_atom(module) and is_binary(identifier) ->
          raise Polyjuice.ClientTest.TestError,
            module: module,
            identifier: identifier,
            fills: Keyword.get(opts, :fills, [])

        _ ->
          raise Polyjuice.ClientTest.TestError
      end
    end

    true
  end

  @doc """
  Assert that a value is equal to an expected value.

  If they are not equal, a `Polyjuice.ClientTest.TestError` will be raised.

  Known options are `module`, `identifier`, and `fills`.
  """
  @spec assert_eq(any, any, Keyword.t()) :: true
  def assert_eq(actual, expected, opts \\ []) do
    assert(actual == expected, opts)
  end
end
