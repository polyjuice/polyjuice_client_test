accept-verification-request = Accept the verification request.
decryption-error = Decryption error.
continue = Continue
invite-user = Invite {$user} to the room.
is-message-decryptable = Can you decrypt the message sent in the room "{$room}"?
log-in-with-password = Log in with the user ID "{ $user }" and password "{ $password }" (without quotes).
login-failed-wrong-method = Login failed: wrong login method.
login-failed-wrong-password = Login failed: wrong password.
login-failed-wrong-user = Login failed: wrong user.
message-received = You sent the message: {$message}
no = No
sas_verification_fixed_base64 = Using fixed base64 encoding.
sas_verification_fixed_base64_unstable = Using fixed base64 encoding (unstable prefix).
sas_verification_invalid_base64 = Using invalid base64 encoding.
send-any-message-in-room = Send a message in the room "{$room}"
stable = Stable
unlock-ssss-with-key = Unlock SSSS using the key "{ $key }".
unstable = Unstable
stable-or-unstable = Use stable or unstable prefix?
send-start-verification = send start message.
start-verification-emoji = Start verification in the client using emoji, or
verify-sas-emoji = Verify that the emoji match: {$emoji1} ({$emoji1_name}) {$emoji2} ({$emoji2_name}) {$emoji3} ({$emoji3_name}) {$emoji4} ({$emoji4_name}) {$emoji5} ({$emoji5_name}) {$emoji6} ({$emoji6_name}) {$emoji7} ({$emoji7_name})
verification-error = Verification error
yes = Yes
