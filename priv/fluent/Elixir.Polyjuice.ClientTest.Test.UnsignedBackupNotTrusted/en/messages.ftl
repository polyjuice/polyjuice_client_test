is-backup-active = Is the client backing up to the key backup?  You may need to go into the client's settings to check this.
waiting-for-backup = Waiting 30 seconds to see if key gets backed up.
key-backed-up = Key backed up.
