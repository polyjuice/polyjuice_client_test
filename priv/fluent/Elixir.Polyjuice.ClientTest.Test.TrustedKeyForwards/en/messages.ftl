decryptable-messages = If you view the "Trusted key forwards test" room, you should see four encrypted messages, and only the first message should be marked as trusted.  Is this what you see?
requesting-keys = Requesting keys from the client.
trusted-marked-as-untrusted = Trusted session {$session_id} marked as untrusted.
untrusted-marked-as-trusted = Untrusted session {$session_id} marked as trusted.
