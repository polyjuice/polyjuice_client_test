# Copyright 2021 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.TestScript do
  require ExUnit.Assertions

  def test_script(script, event_handler \\ fn _, state, _ -> state end, initial_state \\ %{}) do
    server =
      Polyjuice.ClientTest.BaseTestServer.new(
        %Polyjuice.ClientTest.DashboardServer{
          servername: "test.localhost",
          address: "https://test.localhost:8008",
          test_domain: "test.localhost"
        },
        "@fakeuser:test.localhost",
        "faketest",
        "faketest.test.localhost",
        script,
        event_handlers: [event_handler],
        initial_state: initial_state
      )

    ref = Polyjuice.ClientTest.TestServer.monitor(server)

    receive do
      {:DOWN, ^ref, _, _, reason} ->
        case reason do
          :normal ->
            :ok

          {:error, err} ->
            if Exception.exception?(err) do
              raise err
            else
              raise "Error: #{inspect(err)}"
            end
        end
    end
  end
end
