# Copyright 2022 The Matrix.org Foundation C.I.C.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.RoomTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  defp strip_events(events) do
    Enum.map(
      events,
      &Map.take(&1, [
        "content",
        "event_id",
        "origin_server_ts",
        "room_id",
        "sender",
        "state_key",
        "type"
      ])
    )
  end

  test "creating a room notifies creator" do
    test_script(
      fn server ->
        {user, password} = BaseTestServer.create_user(server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(server, {:password, user, password}, [])

        sync_task =
          Task.async(fn ->
            Polyjuice.Server.Protocols.PubSub.await_sync(
              server,
              user,
              device_id,
              nil,
              2000,
              %{}
            )
          end)

        # wait for the sync helper to start
        Process.sleep(100)

        {:ok, room_id} =
          Polyjuice.Server.Protocols.Room.create(server, [
            {user, "m.room.create", "",
             %{
               "creator" => user,
               "room_version" => "5"
             }},
            {user, "m.room.member", user,
             %{
               "membership" => "join"
             }},
            {user, "m.room.power_levels", "",
             %{
               "users" => %{user => 100}
             }},
            {user, "m.room.join_rules", "", %{"join_rule" => "invite"}},
            {user, "m.room.history_visibility", "", %{"history_visibility" => "shared"}}
          ])

        {sync_token1, sync_data1} = Task.await(sync_task)

        assert Enum.count(sync_data1["rooms"]["join"][room_id]["timeline"]["events"]) == 5
        assert sync_data1["rooms"]["join"][room_id]["state"]["events"] == []

        {sync_token1_2, sync_data1_2} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            user,
            device_id,
            nil,
            %{}
          )

        assert sync_token1_2 == sync_token1

        assert sync_data1_2["rooms"]["join"][room_id]["timeline"]["events"] |> strip_events() ==
                 sync_data1["rooms"]["join"][room_id]["timeline"]["events"] |> strip_events()

        sync_task =
          Task.async(fn ->
            Polyjuice.Server.Protocols.PubSub.await_sync(
              server,
              user,
              device_id,
              sync_token1,
              2000,
              %{}
            )
          end)

        Polyjuice.Server.Protocols.Room.send_event(
          server,
          user,
          "",
          room_id,
          "m.room.message",
          nil,
          nil,
          %{
            "body" => "Hello world!",
            "msgtype" => "m.text"
          }
        )

        {sync_token2, sync_data2} = Task.await(sync_task)

        assert Enum.count(sync_data2["rooms"]["join"][room_id]["timeline"]["events"]) == 1

        {sync_token2_2, sync_data2_2} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            user,
            device_id,
            sync_token1,
            %{}
          )

        assert sync_token2 == sync_token2_2

        assert sync_data2["rooms"]["join"][room_id]["timeline"]["events"] |> strip_events() ==
                 sync_data2_2["rooms"]["join"][room_id]["timeline"]["events"] |> strip_events()

        {sync_token2_3, sync_data2_3} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            user,
            device_id,
            nil,
            %{}
          )

        assert sync_token2 == sync_token2_3

        assert sync_data2_3["rooms"]["join"][room_id]["timeline"]["events"] |> strip_events() ==
                 Enum.concat(
                   sync_data1["rooms"]["join"][room_id]["timeline"]["events"],
                   sync_data2["rooms"]["join"][room_id]["timeline"]["events"]
                 )
                 |> strip_events()

        [
          {:send_event, ^user, ^room_id, event0},
          {:create_room, ^user, ^room_id, events1},
          {:login, user, device_id, access_token}
        ] = BaseTestServer.get_test_state(server, &Map.get(&1, :events, []))

        assert strip_events([event0]) ==
                 strip_events(sync_data2["rooms"]["join"][room_id]["timeline"]["events"])

        assert strip_events(events1) ==
                 strip_events(sync_data1["rooms"]["join"][room_id]["timeline"]["events"])
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  test "can invite a user on room creation" do
    test_script(
      fn server ->
        {alice, _alice_password} = BaseTestServer.create_user(server, :aliceandbob, "ThisIsATest")
        {bob, bob_password} = BaseTestServer.create_user(server, :aliceandbob, "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(server, {:password, bob, bob_password}, [])

        {:ok, room_id} =
          Polyjuice.Server.Protocols.Room.create(server, [
            {alice, "m.room.create", "",
             %{
               "creator" => alice,
               "room_version" => "5"
             }},
            {alice, "m.room.member", alice,
             %{
               "membership" => "join"
             }},
            {alice, "m.room.power_levels", "",
             %{
               "users" => %{alice => 100}
             }},
            {alice, "m.room.join_rules", "", %{"join_rule" => "invite"}},
            {alice, "m.room.history_visibility", "", %{"history_visibility" => "shared"}},
            {alice, "m.room.member", bob,
             %{
               "membership" => "invite"
             }}
          ])

        {_sync_token1, sync_data1} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            bob,
            device_id,
            nil,
            %{}
          )

        assert Map.has_key?(sync_data1["rooms"]["invite"], room_id)

        assert Map.delete(sync_data1["rooms"], "invite") == %{}

        {:ok, _event_id} =
          Polyjuice.Server.Protocols.Room.send_event(
            server,
            bob,
            "",
            room_id,
            "m.room.member",
            bob,
            nil,
            %{"membership" => "join"}
          )

        {_sync_token2, sync_data2_full} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            bob,
            device_id,
            nil,
            %{}
          )

        room_events = sync_data2_full["rooms"]["join"][room_id]["timeline"]["events"]

        assert Enum.count(room_events) == 7

        {initial_events, [join_event]} = Enum.split(room_events, 6)

        events = BaseTestServer.get_test_state(server, &Map.get(&1, :events, []))

        assert events == [
                 {:send_event, bob, room_id, join_event},
                 {:create_room, alice, room_id, initial_events},
                 {:login, bob, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  test "can invite a user" do
    test_script(
      fn server ->
        {alice, _alice_password} = BaseTestServer.create_user(server, :aliceandbob, "ThisIsATest")
        {bob, bob_password} = BaseTestServer.create_user(server, :aliceandbob, "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(server, {:password, bob, bob_password}, [])

        {:ok, room_id} =
          Polyjuice.Server.Protocols.Room.create(server, [
            {alice, "m.room.create", "",
             %{
               "creator" => alice,
               "room_version" => "5"
             }},
            {alice, "m.room.member", alice,
             %{
               "membership" => "join"
             }},
            {alice, "m.room.power_levels", "",
             %{
               "users" => %{alice => 100}
             }},
            {alice, "m.room.join_rules", "", %{"join_rule" => "invite"}},
            {alice, "m.room.history_visibility", "", %{"history_visibility" => "shared"}}
          ])

        :ok =
          Polyjuice.Server.Protocols.Room.invite(
            server,
            alice,
            room_id,
            bob,
            ""
          )

        {_sync_token1, sync_data1} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            bob,
            device_id,
            nil,
            %{}
          )

        assert Map.has_key?(sync_data1["rooms"]["invite"], room_id)

        assert Map.delete(sync_data1["rooms"], "invite") == %{}

        {:ok, _event_id} =
          Polyjuice.Server.Protocols.Room.send_event(
            server,
            bob,
            "",
            room_id,
            "m.room.member",
            bob,
            nil,
            %{"membership" => "join"}
          )

        {_sync_token2, sync_data2_full} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            server,
            bob,
            device_id,
            nil,
            %{}
          )

        room_events = sync_data2_full["rooms"]["join"][room_id]["timeline"]["events"]

        assert Enum.count(room_events) == 7

        {initial_events, [invite_event, join_event]} = Enum.split(room_events, 5)

        events = BaseTestServer.get_test_state(server, &Map.get(&1, :events, []))

        assert events == [
                 {:send_event, bob, room_id, join_event},
                 {:send_event, alice, room_id, invite_event},
                 {:create_room, alice, room_id, initial_events},
                 {:login, bob, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  # FIXME: test changing state includes old state in event
end
