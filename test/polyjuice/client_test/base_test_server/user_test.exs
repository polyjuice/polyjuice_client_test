# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.UserTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  test "log in, change password, log out, deactivate" do
    test_script(
      fn test_server ->
        {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])

        {users, access_tokens} =
          BaseTestServer.get_and_update_server_state(test_server, fn state ->
            {{state.users, state.access_tokens}, state}
          end)

        assert Map.fetch!(users, user) |> Map.fetch!(:devices) == %{
                 device_id => %{access_token: access_token}
               }

        assert access_tokens == %{access_token => %{user_id: user, device_id: device_id}}

        :ok = Polyjuice.Server.Protocols.User.change_password(test_server, user, "NewPassword")

        users =
          BaseTestServer.get_and_update_server_state(test_server, fn state ->
            {state.users, state}
          end)

        assert Map.fetch!(users, user) |> Map.fetch!(:password) == "NewPassword"

        Polyjuice.Server.Protocols.User.log_out(test_server, user, device_id)

        {users, access_tokens} =
          BaseTestServer.get_and_update_server_state(test_server, fn state ->
            {{state.users, state.access_tokens}, state}
          end)

        assert Map.fetch!(users, user) |> Map.fetch!(:devices) == %{}
        assert access_tokens == %{}

        Polyjuice.Server.Protocols.User.deactivate(test_server, user)

        users =
          BaseTestServer.get_and_update_server_state(test_server, fn state ->
            {state.users, state}
          end)

        assert users == %{}

        events = BaseTestServer.get_test_state(test_server, &Map.get(&1, :events, []))

        assert events == [
                 {:deactivate, user},
                 {:logout, user, device_id},
                 {:change_password, user, password, "NewPassword"},
                 {:login, user, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end

  test "get profile" do
    test_script(fn server ->
      {user, _password} = BaseTestServer.create_user(server, "test", "ThisIsATest")

      assert Polyjuice.Server.Protocols.User.get_profile(server, nil, user, nil) == {:ok, %{}}

      assert Polyjuice.Server.Protocols.User.get_profile(server, nil, "not_a_user", nil) ==
               {:error,
                %Polyjuice.Server.Plug.MatrixError{
                  message: "Unknown user",
                  plug_status: 404,
                  matrix_error: Polyjuice.Util.ClientAPIErrors.MNotFound.new()
                }}
    end)
  end
end
