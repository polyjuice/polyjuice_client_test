# Copyright 2022 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule Polyjuice.ClientTest.BaseTestServer.AccountDataTest do
  use ExUnit.Case
  import Polyjuice.ClientTest.TestScript

  alias Polyjuice.ClientTest.BaseTestServer

  def make_account_data(sync_data) do
    Map.get(sync_data, "account_data", %{})
    |> Map.get("events", [])
    |> Enum.map(fn %{"type" => type, "content" => content} -> {type, content} end)
    |> Map.new()
  end

  test "set and get, with sync" do
    test_script(
      fn test_server ->
        {user, password} = BaseTestServer.create_user(test_server, "test", "ThisIsATest")

        {:ok, %{device_id: device_id, access_token: access_token}} =
          Polyjuice.Server.Protocols.User.log_in(test_server, {:password, user, password}, [])

        Polyjuice.Server.Protocols.AccountData.set(test_server, user, nil, "foo", %{
          "foo" => "bar"
        })

        account_data =
          BaseTestServer.get_and_update_server_state(test_server, fn state ->
            {state.account_data, state}
          end)

        assert Map.get(account_data, user) == %{nil => %{"foo" => %{"foo" => "bar"}}}

        {sync_token, sync_data} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            test_server,
            user,
            device_id,
            nil,
            %{}
          )

        account_data = make_account_data(sync_data)

        assert account_data == %{"foo" => %{"foo" => "bar"}}

        assert Polyjuice.Server.Protocols.AccountData.get(test_server, user, nil, "foo") ==
                 {:ok, %{"foo" => "bar"}}

        # check that we get account data updates over sync
        sync_task =
          Task.async(fn ->
            Polyjuice.Server.Protocols.PubSub.await_sync(
              test_server,
              user,
              device_id,
              sync_token,
              30000,
              %{}
            )
          end)

        # wait for the sync helper to start
        Process.sleep(100)

        Polyjuice.Server.Protocols.AccountData.set(test_server, user, nil, "foo", %{
          "foo" => "baz"
        })

        assert Polyjuice.Server.Protocols.AccountData.get(test_server, user, nil, "foo") ==
                 {:ok, %{"foo" => "baz"}}

        {sync_token, sync_data} = Task.await(sync_task)

        account_data = make_account_data(sync_data)

        assert account_data == %{"foo" => %{"foo" => "baz"}}

        # make sure we get the same result when going back to the beginning of
        # the stream
        {^sync_token, sync_data} =
          Polyjuice.Server.Protocols.PubSub.get_sync_data_since(
            test_server,
            user,
            device_id,
            nil,
            %{}
          )

        account_data = make_account_data(sync_data)

        assert account_data == %{"foo" => %{"foo" => "baz"}}

        events = BaseTestServer.get_test_state(test_server, &Map.get(&1, :events, []))

        assert events == [
                 {:set_account_data, user, nil, "foo", %{"foo" => "baz"}},
                 {:set_account_data, user, nil, "foo", %{"foo" => "bar"}},
                 {:login, user, device_id, access_token}
               ]
      end,
      fn event, state, _ ->
        Map.put(state, :events, [event | Map.get(state, :events, [])])
      end
    )
  end
end
