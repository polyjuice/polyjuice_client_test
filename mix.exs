defmodule PolyjuiceClientTest.MixProject do
  use Mix.Project

  def project do
    [
      app: :polyjuice_client_test,
      version: "0.1.0",
      elixir: "~> 1.10",
      start_permanent: Mix.env() == :prod,
      compilers: [:esbuild | Mix.compilers()],
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),

      # Docs
      name: "Polyjuice Client Test",
      source_url: "https://gitlab.com/polyjuice/polyjuice_client_test",
      homepage_url: "https://www.uhoreg.ca/programming/matrix/polyjuice",
      docs: [
        # The main page in the docs
        main: "readme",
        logo: "priv/static/favicon.png",
        extras: ["README.md", "writing_tests.md"],
        groups_for_modules: [
          {"Test utility functions",
           [
             ~r/Polyjuice\.ClientTest\.Util\..*/
           ]},
          {"Base test server",
           [
             ~r/Polyjuice\.ClientTest\.BaseTestServer.*/
           ]},
          {"Tests",
           [
             ~r/Polyjuice\.ClientTest\.Test\..*/
           ]}
        ],
        deps: [
          polyjuice_newt: "https://polyjuice.gitlab.io/polyjuice_newt/",
          polyjuice_server: "https://polyjuice.gitlab.io/polyjuice_server/"
        ]
      ],
      package: [
        maintainers: ["Hubert Chathi"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/polyjuice/polyjuice_client_test"
        }
      ],

      # Tests
      test_coverage: [
        summary: [threshold: 0]
      ],
      consolidate_protocols: Mix.env() != :test
    ]
  end

  def application do
    [
      mod: {Polyjuice.ClientTest, []},
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},
      {:erl_base58, "~> 0.0.1"},
      {:ex_doc, "~> 0.21", only: :dev, runtime: false},
      {:libfluent, "~> 0.2.3"},
      {:plug_cowboy, "~> 2.0"},
      {:polyjuice_client, "~> 0.4.4"},
      {:polyjuice_newt, git: "https://gitlab.com/polyjuice/polyjuice_newt.git", ref: "ddd5f34b"},
      {:polyjuice_server,
       git: "https://gitlab.com/polyjuice/polyjuice_server.git", ref: "16d3ba8"},
      {:polyjuice_util,
       git: "https://gitlab.com/polyjuice/polyjuice_util.git", ref: "b2e6446e", override: true},
      {:towel, "~> 0.2.1"},
      {:typed_struct, "~> 0.3.0"}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end

defmodule Mix.Tasks.Compile.Esbuild do
  def run(_args) do
    {_, errcode} =
      System.cmd("yarn", ["install"],
        cd: "svelte",
        stderr_to_stdout: true,
        into: IO.stream(:stdio, :line)
      )

    if errcode != 0 do
      Mix.raise("Unable to install JavaScript dependencies for dashboard.")
    end

    {_, errcode} =
      System.cmd("node", ["build.js"],
        cd: "svelte",
        stderr_to_stdout: true,
        into: IO.stream(:stdio, :line)
      )

    if errcode != 0 do
      Mix.raise("Failed to compile dashboard.")
    end

    :ok
  end

  def clean do
    if File.exists?("priv/static/build") do
      File.rm_rf!("priv/static/build")
    end
  end
end
