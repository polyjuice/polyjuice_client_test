const sveltePlugin = require("esbuild-svelte");
const sveltePreprocess = require("svelte-preprocess");
const autoprefixer = require("autoprefixer");
const tailwindcss = require("tailwindcss");
const postCssPlugin = require("@deanc/esbuild-plugin-postcss");

require("esbuild").build({
  entryPoints: ["src/main.ts"],
  bundle: true,
  minify: true,
  sourcemap: true,
  outfile: "../priv/static/build/bundle.js",
  target: ['es2017'],
  //target: ['chrome58', 'firefox57', 'safari11', 'edge16'],
  external: ["/bubbles.png"],
  plugins: [
    sveltePlugin({
      preprocess: sveltePreprocess({
        postcss: {
          plugins: [tailwindcss, autoprefixer]
        }
      }),
    }),
    postCssPlugin({
      plugins: [tailwindcss, autoprefixer],
    }),
  ]
}).catch(() => process.exit(1))
