module.exports = {
  content: ["./src/**/*.{html,js,svelte}"],
  theme: {
    extend: {
      backgroundImage: {
        bubbles: "url('/bubbles.png')",
      },
    },
  },
  plugins: [],
}
