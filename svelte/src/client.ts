/* Copyright 2021-2022 The Matrix.org Foundation C.I.C.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

import { EventEmitter } from "events";

export class Client extends EventEmitter {
    private accessToken?: string;
    public userId?: string;
    public deviceId?: string;
    private since?: string;
    private txnId: number;
    constructor(readonly homeserverUrl: string) {
        super();
        this.txnId = 0;
    }

    private async post(path: string, data: any = {}): Promise<any> {
        const headers: Record<string, string> = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "POST",
                headers: headers,
                body: JSON.stringify(data),
            },
        );
        if (response.status > 400) {
            throw {
                status: response.status,
                body: await response.json(),
            };
        } else {
            return response.json();
        }
    }
    private async put(path: string, data: any = {}): Promise<any> {
        const headers: {[header: string]: string} = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "PUT",
                headers: headers,
                body: JSON.stringify(data),
            },
        );
        if (response.status > 400) {
            throw await response.json();
        } else {
            return response.json();
        }
    }
    private async get(path: string): Promise<any> {
        const headers: {[header: string]: string} = {
            "Content-Type": "application/json",
        };
        if (this.accessToken) {
            headers.Authorization = `Bearer ${this.accessToken}`;
        }
        const response = await fetch(
            `${this.homeserverUrl}/_matrix/client/${path}`,
            {
                method: "GET",
                headers: headers,
            },
        );
        if (response.status > 400) {
            throw await response.json();
        } else {
            return response.json();
        }
    }

    async logIn(): Promise<boolean> {
        const response = await this.post("v3/login", {
            type: "m.login.password",
            identifier: {
                type: "m.id.user",
                user: "test",
            },
            password: "test",
        });
        this.accessToken = response.access_token;
        this.deviceId = response.device_id;
        this.userId = response.user_id;

        this.doSync();
        return true;
    }

    async logOut(): Promise<void> {
        this.accessToken = undefined;
        this.deviceId = undefined;
        this.userId = undefined;
        await this.post("v3/logout");
    }

    async roomDirectory(search?: string | undefined): Promise<any> {
        // FIXME: add pagination
        if (search) {
            return this.post("v3/publicRooms", {
                filter: { generic_search_term: search },
            });
        } else {
            return this.get("v3/publicRooms");
        }
    }

    async join(room: string): Promise<string> {
        return (await this.post(`v3/join/${encodeURIComponent(room)}`)).room_id;
    }

    async doSync(): Promise<void> {
        while (this.accessToken) {
            const since = this.since ? `&since=${this.since}` : "";
            let result
            try {
                result = await this.get(
                    `v3/sync?timeout=30000${since}`,
                );
            } catch (e) {
                // assume that any error means we died and need to reconnect
                this.accessToken = undefined;
                this.deviceId = undefined;
                this.userId = undefined;
                this.emit("logged_out", e);
                return;
            }
            this.since = result.next_batch;

            this.emit("sync", result);
        }
    }

    async send(room: string, eventType: string, contents: Record<string, any>): Promise<void> {
        if (this.accessToken) {
            await this.put(
                `v3/rooms/${encodeURIComponent(room)}/send/${encodeURIComponent(eventType)}/${this.txnId++}`,
                contents
            )
        }
    }
}
