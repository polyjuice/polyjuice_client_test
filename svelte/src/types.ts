import type { Client } from "./client";

export interface IRequest {
    request: {
        method: string,
        path: string,
        query_string: string,
        headers: [string, string][],
        timestamp: number,
    },
    response?: {
        status: number,
        headers: [string, string][],
        timestamp: number,
        duration: number,
        content?: string,
    }
}

export interface IMessage {
    eventId: string,
    text: string,
    level: number,
    type?: string,
    response?: any,
    responded?: boolean,
    done?: boolean,
    retry?: string,
}

export interface ITest {
    client?: Client,
    roomId: string,
    name: string,
    topic?: string,
    status: number,
    messages: IMessage[],
    requests: Record<string, IRequest>,
    requestOrder: string[],
    testId?: string,
}

export interface IMxEvent {
    sender: string,
    room_id: string,
    type: string,
    event_id: string,
    state_key?: string,
    content: Record<string, any>,
}

interface IMxRoomData {
    timeline: {events: IMxEvent[] },
    state: { events: IMxEvent[] },
};

export interface IMxSyncData {
    rooms?: {
        join?: Record<string, IMxRoomData>,
        leave?: Record<string, IMxRoomData>,
    },
}
